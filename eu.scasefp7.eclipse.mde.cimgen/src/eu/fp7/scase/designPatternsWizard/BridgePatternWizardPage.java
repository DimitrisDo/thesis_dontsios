package eu.fp7.scase.designPatternsWizard;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ServiceCIM.RESTfulServiceCIM;
import ServiceCIM.Resource;
import ServiceCIM.ServiceCIMFactory;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import DesignPatternsLayerCIM.AnnCRUDActivity;
import DesignPatternsLayerCIM.AnnAlgoResource;
import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.MementoPattern;
import DesignPatternsLayerCIM.BridgePattern;
import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class BridgePatternWizardPage extends WizardPage {

	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	private Composite oMementoPatternGrid;
	private Label lblAvailableResources;
	private List oResourceList;
	private List oBridgableAlgoResourcesList;
	private Label lblResourceRepresentationInstances;
	
	private boolean bHasBridgeOnSearch;
	private boolean bHasBridgeOnExtComp;
	
	private Resource oResource;
	private int selectedBridgable;
	
	private Button btnMakeBridgable;
	private Button btnDeleteBridgable;
	
	private AnnotationModel oDesignPatternsCIM;
	private DesignPatternsLayerCIMFactory oDesignPatternsLayerCIMFactory;
	private DesignPatternsLayerCIM.AnnAlgoResource ooResource;
	private ServiceCIMFactory oServiceCIMFactory;
	
	private ArrayList<AnnAlgoResource> bridgablesList = new ArrayList<AnnAlgoResource>(); 
	
	
	protected BridgePatternWizardPage(String pageName, RESTfulServiceCIM oRESTfulServiceCIM, AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super("Bridge Pattern");
		setMessage("Choose which resources shall be mementable and define their memento's number.\r\n");
		setTitle("Bridge Pattern");
		this.oRESTfulServiceCIM = oRESTfulServiceCIM;
	    this.bReloadExistingModels = bReloadExistingModels;
	    this.oDesignPatternsCIM=oDesignPatternsCIM;
	    this.oDesignPatternsLayerCIMFactory = DesignPatternsLayerCIMFactory.eINSTANCE;
	    this.oServiceCIMFactory = ServiceCIMFactory.eINSTANCE;
	    oResource = oServiceCIMFactory.createResource();
	    bHasBridgeOnSearch = true;
		bHasBridgeOnExtComp = false;
	    ooResource = oDesignPatternsLayerCIMFactory.createAnnAlgoResource();
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
		this.setControl(this.oWizardPageGrid);
		
		initializeBuilderGrid();

	}
	
	private void initializeBuilderGrid(){
		this.oMementoPatternGrid = new Composite(oWizardPageGrid, SWT.NONE);
		this.oMementoPatternGrid.setBounds(0, 10, 572, 252);
		
		this.lblResourceRepresentationInstances = new Label(oMementoPatternGrid, SWT.NONE);
		lblResourceRepresentationInstances.setBounds(297, 0, 265, 20);
		lblResourceRepresentationInstances.setText("Build with Bridge Pattern");
		
		this.oResourceList = new List(oMementoPatternGrid, SWT.V_SCROLL);
		this.oResourceList.setBounds(10, 26, 265, 80);
		populateResourceList();
		addResourceListListener();
		
		this.oBridgableAlgoResourcesList = new List(oMementoPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oBridgableAlgoResourcesList.setBounds(297, 26, 265, 80);
		addMementosListListListener();
		
		this.lblAvailableResources = new Label(oMementoPatternGrid, SWT.NONE);
		lblAvailableResources.setBounds(10, 0, 265, 20);
		lblAvailableResources.setText("Available Algorithmic Resources");
		
		this.btnMakeBridgable = new Button(oMementoPatternGrid, SWT.NONE);
		btnMakeBridgable.setBounds(10, 112, 265, 30);
		btnMakeBridgable.setText("Build with Bridge Pattern");
		addbtnCreateMementableListener();
		
		this.btnDeleteBridgable = new Button(oMementoPatternGrid, SWT.NONE);
		btnDeleteBridgable.setBounds(297, 112, 265, 30);
		btnDeleteBridgable.setText("Revert");
		addbtnDeleteBridgableListener();
	}
	
	private void addbtnCreateMementableListener() {
		this.btnMakeBridgable.addListener(SWT.Selection, new Listener(){

			@Override
			public void handleEvent(Event event) {
				
				if(!bridgablesList.contains(ooResource)){
					bridgablesList.add(ooResource);
				}
				populateBridgableList();
				
			}
		});
	}
	
	private void addbtnDeleteBridgableListener() {
		
		btnDeleteBridgable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				bridgablesList.remove(selectedBridgable);
				populateBridgableList();
				
				//updateWidgetStatus();
				setPageComplete(isPageCompleted());
			}
		});
		
	}


	private void addMementosListListListener() {
		this.oBridgableAlgoResourcesList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				selectedBridgable = oBridgableAlgoResourcesList.getSelectionIndex();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
		
	}

	private void addResourceListListener() {
		// TODO Auto-generated method stub
		this.oResourceList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				oResource=oRESTfulServiceCIM.getHasResources().get(getResourceIndexByName(oResourceList.getSelection()[0]));
				ooResource = oDesignPatternsLayerCIMFactory.createAnnAlgoResource();
				ooResource.setAnnotatesAlgoResource(oResource);
				setPageComplete(isPageCompleted());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	private int getResourceIndexByName(String strResourceName) {
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(this.oRESTfulServiceCIM.getHasResources().get(n).getName().equalsIgnoreCase(strResourceName)){
				return n;
			}
		}
		return -1; //Throw exception in production code
	}
	
	private void populateResourceList(){
		this.oResourceList.removeAll();
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(this.oRESTfulServiceCIM.getHasResources().get(n).isIsAlgorithmic())
				this.oResourceList.add(this.oRESTfulServiceCIM.getHasResources().get(n).getName());
		}
	}
	
	private void populateBridgableList(){
		this.oBridgableAlgoResourcesList.removeAll();
		for(int i = 0; i < this.bridgablesList.size(); i++){
			this.oBridgableAlgoResourcesList.add(this.bridgablesList.get(i).getAnnotatesAlgoResource().getName());
		}
	}
	
	private void updateResourceWidgets() {
		if(this.oResourceList.getItemCount() > 0){
			this.oResourceList.setEnabled(true);
		}
		else{
			this.oResourceList.setEnabled(false);
		}
	}
	
	private void updateRepresentationWidgets() {
		if(this.oBridgableAlgoResourcesList.getItemCount() > 0){
			this.oBridgableAlgoResourcesList.setEnabled(true);
		}
		else{
			this.oBridgableAlgoResourcesList.setEnabled(false);
		}
	}
	
	private void updatePropertiesWidget() {

	}
	
	private void updateWidgetStatus(){
		updateResourceWidgets();
		updatePropertiesWidget();
		updateRepresentationWidgets();
		
	}
	
	private boolean isPageCompleted(){
		
		if(isValidCIMModel()){
			return true;
		}
		return false;
		
	}
	
	private boolean isValidCIMModel() {
		//validate resources
		boolean valid = true;
		return valid;
	}
	
	
	public void CreateDsignPatternsCIM(){
		
		this.oDesignPatternsCIM.getHasAnnotatedElement().clear();
		this.oDesignPatternsCIM.getHasAnnotation().clear();
		
		addDesignPatternsModelToCIM();
		System.out.println("Added Memento Command pattern");
		
	}
	
	public void addDesignPatternsModelToCIM(){
		
		DesignPatternModel oDesignPatternModel = this.oDesignPatternsLayerCIMFactory.createDesignPatternModel();
		BridgePattern oBridgePattern = this.oDesignPatternsLayerCIMFactory.createBridgePattern();
	
		for(AnnAlgoResource m : bridgablesList){
			oBridgePattern.getAssociatesAnnAlgoResource().add(m);
			this.oDesignPatternsCIM.getHasAnnotatedElement().add(m);
		}
		
		oBridgePattern.setBMakeBridgePatternForSearch(bHasBridgeOnSearch);
		System.out.println("Added bridge Pattern for search " + bHasBridgeOnSearch) ;
		oBridgePattern.setBMakeBridgePatternForExternalService(bHasBridgeOnExtComp);
		System.out.println("Added bridge Pattern for exte " + bHasBridgeOnExtComp) ;
		this.oDesignPatternsCIM.getHasAnnotation().add(oBridgePattern);
	}
	
	@Override
	public IWizardPage getNextPage(){
		return null;
	}
}
