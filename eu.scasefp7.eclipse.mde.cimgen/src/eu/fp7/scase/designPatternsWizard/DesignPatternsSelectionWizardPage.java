package eu.fp7.scase.designPatternsWizard;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ServiceCIM.RESTfulServiceCIM;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Text;

import DesignPatternsLayerCIM.AnnotationModel;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.ScrolledComposite;

public class DesignPatternsSelectionWizardPage extends WizardPage {
	
	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	private AnnotationModel oDesignPatternsCIM;
	private Text txtInfoapplyOnly;

	public DesignPatternsSelectionWizardPage(String strOutputFolder, RESTfulServiceCIM oRESTfulServiceCIM, AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super("Select Design Patterns");
		setMessage("Please select the desired design patterns that you would like to apply.");
		setTitle("Desing Pattern Selection");
		this.oDesignPatternsCIM = oDesignPatternsCIM;
	    this.oRESTfulServiceCIM = oRESTfulServiceCIM;
	    this.bReloadExistingModels = bReloadExistingModels;
	    DesignPatternsCIMWizard.gottenAnnResourcesList.clear();
	}


	@Override
	public void createControl(Composite parent) {
		//Creates and initializes the widgets
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
		  
		 // initializeWizardPagesGrids(parent);
		 // initializeWizardPageWidgets(parent);
		  
		this.setControl(this.oWizardPageGrid);
		
		Composite composite = new Composite(oWizardPageGrid, SWT.NONE);
		composite.setBounds(10, 10, 291, 193);
		
		Label lblNewLabel = new Label(composite, SWT.NONE);
		lblNewLabel.setBounds(10, 8, 271, 25);
		lblNewLabel.setText("Design Pattern:");
		
		Button btnBuilderRadioButton = new Button(composite, SWT.CHECK);
		btnBuilderRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btnBuilderPatternSelected = (Button) e.getSource();
				DesignPatternsCIMWizard.BuilderPatternBool=btnBuilderPatternSelected.getSelection();
				
				//for test purposeses
				if (btnBuilderPatternSelected.getSelection())
					System.out.println("Build with Builder Pattern");
				else
					System.out.println("Build without builder pattern");
				
				setPageComplete(isPageCompleted());
			}
		});
		btnBuilderRadioButton.setBounds(10, 39, 152, 25);
		btnBuilderRadioButton.setText("Builder Pattern");
		
		Button btnObserverRadioButton = new Button(composite, SWT.CHECK);
		btnObserverRadioButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btnObserverPatternSelected = (Button) e.getSource();
				DesignPatternsCIMWizard.ObserverPatternBool=btnObserverPatternSelected.getSelection();
				
				//for test purposeses
				if (btnObserverPatternSelected.getSelection())
					System.out.println("Build with Observer Pattern");
				else
					System.out.println("Build without Observer pattern");
				
				setPageComplete(isPageCompleted());
			}
		});
		btnObserverRadioButton.setBounds(10, 65, 168, 25);
		btnObserverRadioButton.setText("Observer Pattern");
		
		Button btnMementocommandPattern = new Button(composite, SWT.CHECK);
		btnMementocommandPattern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btnMementocommandPattern = (Button) e.getSource();
				DesignPatternsCIMWizard.MementoCommandPatternBool=btnMementocommandPattern.getSelection();
				
				//for test purposeses
				if (btnMementocommandPattern.getSelection())
					System.out.println("Build with Memento Pattern");
				else
					System.out.println("Build without Memento pattern");
				
				setPageComplete(isPageCompleted());
			}
		});
		btnMementocommandPattern.setBounds(10, 91, 267, 25);
		btnMementocommandPattern.setText("Memento/Command Pattern");
		
		Button btnBridgePattern = new Button(composite, SWT.CHECK);
		btnBridgePattern.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Button btnBridgePattern = (Button) e.getSource();
				DesignPatternsCIMWizard.BridgePatternBool=btnBridgePattern.getSelection();
				
				//for test purposeses
				if (btnBridgePattern.getSelection())
					System.out.println("Build with Bridge Pattern");
				else
					System.out.println("Build without Bridge pattern");
				
				setPageComplete(isPageCompleted());
			}
		});
		btnBridgePattern.setText("Bridge Pattern");
		btnBridgePattern.setBounds(10, 117, 147, 25);
		
		txtInfoapplyOnly = new Text(oWizardPageGrid, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL);
		txtInfoapplyOnly.setEditable(false);
		txtInfoapplyOnly.setText("Info: \r\n-- Build multiple representations\r\n    with the Builder Pattern. \r\n-- Observe HTTP actions with\r\n    Observer Pattern.\r\n-- Implement mementos and \r\n    rollback functionality with\r\n    Memento pattern.\r\n-- Achieve scalability, extensibility, \r\n    real time algorithm selection \r\n    and modifiability with \r\n    Bridge Pattern.\r\n-- Apply only one\r\n    pattern to each reasource.\r\n-- Bridge pattern will be applied \r\n    to Search and to External\r\n    Composition Algorithmic resources.\r\n-- Memento pattern can be \r\n    applied to CRUD resources.");
		txtInfoapplyOnly.setBounds(307, 10, 255, 242);
		if(!this.bReloadExistingModels){
		  this.setPageComplete(false);
    	}
		
		
	}
	
	//TODO analoga me ta patimena koumpia na eisagontai oi nees selides
	@Override
	public IWizardPage getNextPage(){
		if (DesignPatternsCIMWizard.BuilderPatternBool==true){
			return this.getWizard().getPage("Builder Pattern");
		} else if (DesignPatternsCIMWizard.MementoCommandPatternBool==true){
			return this.getWizard().getPage("Memento Command Pattern");
		} else if (DesignPatternsCIMWizard.ObserverPatternBool==true){
			return this.getWizard().getPage("Observer Pattern");
		} else
		if (DesignPatternsCIMWizard.BridgePatternBool==true){
			return this.getWizard().getPage("Bridge Pattern");
		} else {
//		if (DesignPatternsCIMWizard.ObserverPatternBool==false){
			return null;
		}
//		if (DesignPatternsCIMWizard.BuilderPatternBool==false){
//			return null;
//		} 
//		if (DesignPatternsCIMWizard.MementoCommandPatternBool==false){
//			return null;
//		} 
//		if (DesignPatternsCIMWizard.otherBool==true){
//			return this.getWizard().getPage("Other");
//		}
//		if (DesignPatternsCIMWizard.otherBool==false){
//			return null;
//		}
		
//		return null;
		
	} 
	
	private boolean isPageCompleted(){
		return true;
	}
}
