package eu.fp7.scase.designPatternsWizard;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ServiceCIM.RESTfulServiceCIM;
import ServiceCIM.Resource;
import ServiceCIM.ServiceCIMFactory;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.BuilderPattern;
import DesignPatternsLayerCIM.ConcreteBuilder;
import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory;
import DesignPatternsLayerCIM.Director;
import DesignPatternsLayerCIM.Representation;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public class BuilderPatternWizardPage extends WizardPage {

	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	private Composite oBuilderPatternGrid;
	private Label lblAvailableResources;
	private List oResourceList;
	private List oRepresentationInstancesList;
	private List oPropertiesList;
	private Label lblResourceRepresentationInstances;
	
	private Resource oResource;
	private int ResourceInstance;
	private int SelectedProperty;
	
	private Button btnCreateRepresentationInstance;
	private Button btnDeleteRepresentationInstance;
	private Button btnDeleteSelectedProperty;
	
	//this list contains the information that is needed 
	private ArrayList<Representation> ResourceRepresentationList;
	private AnnotationModel oDesignPatternsCIM;
	private DesignPatternsLayerCIMFactory oDesignPatternsLayerCIMFactory;
	private DesignPatternsLayerCIM.AnnResource ooResource;
	private ServiceCIMFactory oServiceCIMFactory;
	
	
	protected BuilderPatternWizardPage(String pageName, RESTfulServiceCIM oRESTfulServiceCIM, AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super("Builder Pattern");
		setMessage("Choose the resources that you would like to have multiple representations. Then create instances of the representations.");
		setTitle("Builder Pattern");
		this.oRESTfulServiceCIM = oRESTfulServiceCIM;
	    this.bReloadExistingModels = bReloadExistingModels;
	    this.oDesignPatternsCIM=oDesignPatternsCIM;
	    ResourceRepresentationList = new ArrayList<DesignPatternsLayerCIM.Representation>();
	    //make the boolean false to prevent the page from reapearing
	    //DesignPatternsCIMWizard.BuilderPatternBool = false;
	    this.oDesignPatternsLayerCIMFactory = DesignPatternsLayerCIMFactory.eINSTANCE;
	    ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
	    this.oServiceCIMFactory = ServiceCIMFactory.eINSTANCE;
	    oResource = oServiceCIMFactory.createResource();	    
	    
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
		this.setControl(this.oWizardPageGrid);
		
		initializeBuilderGrid();
		
		Label lblPropertiesIncludedIn = new Label(oBuilderPatternGrid, SWT.NONE);
		lblPropertiesIncludedIn.setBounds(10, 145, 552, 20);
		lblPropertiesIncludedIn.setText("Properties included in the selected representation instance");

	}
	
	private void initializeBuilderGrid(){
		this.oBuilderPatternGrid = new Composite(oWizardPageGrid, SWT.NONE);
		this.oBuilderPatternGrid.setBounds(0, 0, 617, 293);
		
		this.lblResourceRepresentationInstances = new Label(oBuilderPatternGrid, SWT.NONE);
		lblResourceRepresentationInstances.setBounds(307, 0, 288, 20);
		lblResourceRepresentationInstances.setText("Resource Representation Instances");
		
		this.oResourceList = new List(oBuilderPatternGrid, SWT.V_SCROLL);
		this.oResourceList.setBounds(10, 26, 274, 80);
		populateResourceList();
		addResourceListListener();
		
		this.oPropertiesList = new List(oBuilderPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oPropertiesList.setBounds(10, 171, 585, 80);
		addPropertiesListListener();
		
		this.oRepresentationInstancesList = new List(oBuilderPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oRepresentationInstancesList.setBounds(307, 26, 288, 80);
		addRepresentationInstancesListListListener();
		
		this.lblAvailableResources = new Label(oBuilderPatternGrid, SWT.NONE);
		lblAvailableResources.setBounds(10, 0, 265, 20);
		lblAvailableResources.setText("Available Resources");
		
		this.btnCreateRepresentationInstance = new Button(oBuilderPatternGrid, SWT.NONE);
		btnCreateRepresentationInstance.setBounds(10, 112, 274, 30);
		btnCreateRepresentationInstance.setText("Create Representation Instance");
		addbtnCreateRepresentationInstanceListener();
		
		this.btnDeleteRepresentationInstance = new Button(oBuilderPatternGrid, SWT.NONE);
		btnDeleteRepresentationInstance.setBounds(307, 112, 288, 30);
		btnDeleteRepresentationInstance.setText("Delete Selected Instance");
		addbtnDeleteRepresentationInstanceListener();
		
		btnDeleteSelectedProperty = new Button(oBuilderPatternGrid, SWT.NONE);
		btnDeleteSelectedProperty.setBounds(10, 257, 585, 30);
		btnDeleteSelectedProperty.setText("Delete Selected Property");
		addbtnDeleteSelectedPropertyListener();
	}
	private void addbtnCreateRepresentationInstanceListener() {
		this.btnCreateRepresentationInstance.addListener(SWT.Selection, new Listener(){

			@Override
			public void handleEvent(Event event) {
				
				Representation oRepresentation;
				oRepresentation = oDesignPatternsLayerCIMFactory.createRepresentation();
				AnnProperty oAnnProperty;
				
				
				oRepresentation.setName(ooResource.getAnnotatesResource().getName());
//				for(int i = 0; i < ooResource.getAnnotatesResource().getHasProperty().size(); i++){
//					oRepresentation.getPropertiesList().add(ooResource.getAnnotatesResource().getHasProperty().get(i));
//				}
				//oRepresentation.getPropertiesList().add(ooResource.getAnnotatesResource().getHasProperty());
				oRepresentation.setRefersTo(ooResource);
				DesignPatternsCIMWizard.gottenAnnResourcesList.add(ooResource);
				System.out.println("The list now contains" +DesignPatternsCIMWizard.gottenAnnResourcesList);

				
				System.out.printf("Selected resource %s \n",oRepresentation.getRefersTo().getAnnotatesResource().getName());
				System.out.printf("creating %d property relations for representation: %s \n",oRepresentation.getPropertiesList().size(),oRepresentation.getRefersTo().getAnnotatesResource().getName());
				for(int i=0; i<ooResource.getAnnotatesResource().getHasProperty().size() ;i++){
					oAnnProperty = oDesignPatternsLayerCIMFactory.createAnnProperty();
					oAnnProperty.setAnnotatesProperty(ooResource.getAnnotatesResource().getHasProperty().get(i));
					System.out.printf("Setting property %d: %s \n",i,oAnnProperty.getAnnotatesProperty().getName());
					oRepresentation.getHas().add(i,oAnnProperty);
					System.out.printf("Inserted %s \n",oRepresentation.getHas().get(i).getAnnotatesProperty().getName());
					//oRepresentation.getHas().get(i).setAnnotatesProperty(oResource.getHasProperty().get(getPropertyIndexByName(ooResource.getAnnotatesResource(),ooResource.getAnnotatesResource().getHasProperty().get(i).getName())));
				}	
				System.out.println();
				
				ResourceRepresentationList.add(oRepresentation);
				System.out.println("Now on ResourceRepresentationList");
				for(int i=0; i<ResourceRepresentationList.size();i++){
					System.out.printf("%s \n",ResourceRepresentationList.get(i).getRefersTo().getAnnotatesResource().getName());
				}
				populateRepresentationInstancesList();
				setPageComplete(isPageCompleted());
				
				//updateWidgetStatus();
			}
		});
	}
	
	
	private void addbtnDeleteSelectedPropertyListener() {
		// TODO Auto-generated method stub
		btnDeleteSelectedProperty.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				ResourceRepresentationList.get(ResourceInstance).getHas().remove(SelectedProperty);
				populatePropertiesList(ResourceInstance);
				setPageComplete(isPageCompleted());
			}
		});
	}
	
	private void addbtnDeleteRepresentationInstanceListener() {
		
		btnDeleteRepresentationInstance.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {

				AnnResource temp = ResourceRepresentationList.get(ResourceInstance).getRefersTo();
				ResourceRepresentationList.remove(ResourceInstance);
				populateRepresentationInstancesList();
				try{
					DesignPatternsCIMWizard.gottenAnnResourcesList.remove(temp);
				}catch(Exception ee){
					
				}
				//updateWidgetStatus();
				setPageComplete(isPageCompleted());
			}
		});
		
	}

	private void addPropertiesListListener() {
		this.oPropertiesList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				SelectedProperty = oPropertiesList.getSelectionIndex();
				setPageComplete(isPageCompleted());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
	}


	private void addRepresentationInstancesListListListener() {
		this.oRepresentationInstancesList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				ResourceInstance = oRepresentationInstancesList.getSelectionIndex();
				populatePropertiesList(ResourceInstance);
				
				System.out.printf("The properties of the selected representation %s are : \n",ResourceRepresentationList.get(ResourceInstance).getRefersTo().getAnnotatesResource().getName());
				for(int n = 0; n < ResourceRepresentationList.get(ResourceInstance).getHas().size(); n++){
					System.out.println(ResourceRepresentationList.get(ResourceInstance).getHas().get(n).getAnnotatesProperty().getName());
				}
				setPageComplete(isPageCompleted());
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
		
	}


	private void addResourceListListener() {
		// TODO Auto-generated method stub
		this.oResourceList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				oResource=oRESTfulServiceCIM.getHasResources().get(getResourceIndexByName(oResourceList.getSelection()[0]));
				ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
				ooResource.setAnnotatesResource(oResource);
				System.out.printf("The selected resource %s has the following properties: \n", ooResource.getAnnotatesResource().getName());
				for(int i=0; i< oResource.getHasProperty().size();i++){
					System.out.println(ooResource.getAnnotatesResource().getHasProperty().get(i).getName());
				}
				setPageComplete(isPageCompleted());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	private int getResourceIndexByName(String strResourceName) {
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(this.oRESTfulServiceCIM.getHasResources().get(n).getName().equalsIgnoreCase(strResourceName)){
				return n;
			}
		}
		return -1; //Throw exception in production code
	}

	private void populatePropertiesList(int ResourceInstance) {
		this.oPropertiesList.removeAll();
		
		for(int n = 0; n < ResourceRepresentationList.get(ResourceInstance).getHas().size(); n++){
			this.oPropertiesList.add(ResourceRepresentationList.get(ResourceInstance).getHas().get(n).getAnnotatesProperty().getName());
		}
		
	}
	
	private void populateResourceList(){
		this.oResourceList.removeAll();
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(!this.oRESTfulServiceCIM.getHasResources().get(n).isIsAlgorithmic()){
				if(!allreadyExists(this.oRESTfulServiceCIM.getHasResources().get(n))){
					this.oResourceList.add(this.oRESTfulServiceCIM.getHasResources().get(n).getName());}}
		}
	}
	
	private boolean allreadyExists(Resource oResource){
		for(AnnResource annRe : DesignPatternsCIMWizard.gottenAnnResourcesList){
			if(annRe.getAnnotatesResource().getName().equals(oResource.getName())){
				System.out.println("found /n");
				return true;
			}
		}
		return false;
	}
	
	private void populateRepresentationInstancesList(){
		this.oRepresentationInstancesList.removeAll();
		for(int n = 0; n < this.ResourceRepresentationList.size(); n++){
			this.oRepresentationInstancesList.add(this.ResourceRepresentationList.get(n).getRefersTo().getAnnotatesResource().getName());
		}
	}
	
	private void updateResourceWidgets() {
		if(this.oResourceList.getItemCount() > 0){
			this.oResourceList.setEnabled(true);
		}
		else{
			this.oResourceList.setEnabled(false);
		}
	}
	
	private void updateRepresentationWidgets() {
		if(this.oRepresentationInstancesList.getItemCount() > 0){
			this.oRepresentationInstancesList.setEnabled(true);
		}
		else{
			this.oRepresentationInstancesList.setEnabled(false);
		}
	}
	
	private void updatePropertiesWidget() {
		if(this.oPropertiesList.getItemCount() > 0){
			this.oPropertiesList.setEnabled(true);
		}
		else{
			this.oPropertiesList.setEnabled(false);
		}
	}
	
	private void updateWidgetStatus(){
		updateResourceWidgets();
		updatePropertiesWidget();
		updateRepresentationWidgets();
		
	}
	
	private boolean isPageCompleted(){
		if(isValidCIMModel()){
			return true;
		}
		return false;
	}
	
	private boolean isValidCIMModel() {
		//validate resources
		if(!ResourceInstancesAreUnique()){
			return false;
		}
		setErrorMessage(null);
		
		return true;
	}
	
	private boolean ResourceInstancesAreUnique() {
		
		for(int i = 0; i< this.ResourceRepresentationList.size(); i++){
			for(int j = 0; j<this.ResourceRepresentationList.size(); j++){
				if (i!=j){
					if(ResourceRepresentationList.get(i).getHas()==ResourceRepresentationList.get(j).getHas()){
						setErrorMessage("Every representation must be unique."+i+" and "+j+" representations are the same");
						System.out.println("Shouldn't perform finish");
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public void CreateDsignPatternsCIM(){
		
		this.oDesignPatternsCIM.getHasAnnotatedElement().clear();
		this.oDesignPatternsCIM.getHasAnnotation().clear();
		
		addDesignPatternsModelToCIM();
		System.out.println("Added builder pattern");
		
	}
	
	public void addDesignPatternsModelToCIM(){
		
		//create the Builder Pattern Model and add it to the DesignPattern CIM
		DesignPatternModel oDesignPatternModel = this.oDesignPatternsLayerCIMFactory.createDesignPatternModel();
		BuilderPattern oBuilderPattern = this.oDesignPatternsLayerCIMFactory.createBuilderPattern();
		Director oDirector = this.oDesignPatternsLayerCIMFactory.createDirector();
		ConcreteBuilder oConcreteBuilder;
		//debug
		//ArrayList<String> includedProperties = new ArrayList<String>();
		//System.out.println("Generating models. Adding Representations to the model:");
		//for (int i = 0; i < ResourceRepresentationList.size(); i++){
			//System.out.printf("%s \n",ResourceRepresentationList.get(i).getRefersTo().getAnnotatesResource().getName());
		//}
		//build a concrete builder for each representation
		for(int i = 0; i < ResourceRepresentationList.size(); i++){
			//add annotated elements to the CIM
			this.oDesignPatternsCIM.getHasAnnotatedElement().add(ResourceRepresentationList.get(i).getRefersTo());
			
			oConcreteBuilder=this.oDesignPatternsLayerCIMFactory.createConcreteBuilder();
			oConcreteBuilder.setBuildsRepresentation(ResourceRepresentationList.get(i));
			oConcreteBuilder.setAssociatesAnnResource(ResourceRepresentationList.get(i).getRefersTo());
			for(int j = 0; j < ResourceRepresentationList.get(i).getHas().size(); j++){
				//if(!includedProperties.contains(ResourceRepresentationList.get(i).getHas().get(j).getAnnotatesProperty().getName())){
					this.oDesignPatternsCIM.getHasAnnotatedElement().add(ResourceRepresentationList.get(i).getHas().get(j));
					//includedProperties.add(ResourceRepresentationList.get(i).getHas().get(j).getAnnotatesProperty().getName());
				//}
			}
			oBuilderPattern.getAssociatesAnnResource().add(ResourceRepresentationList.get(i).getRefersTo());
			oDirector.getHasBuilder().add(oConcreteBuilder);
			
		}
		
		//add annotations to the CIM

		oBuilderPattern.setHasDirector(oDirector);
		oDesignPatternModel.getHasDesignPattern().add(oBuilderPattern);
		this.oDesignPatternsCIM.getHasAnnotation().add(oBuilderPattern);
		this.oDesignPatternsCIM.getHasAnnotation().add(oDesignPatternModel);
		
		System.out.println(this.oDesignPatternsCIM.toString());
//		System.out.println("Press Any Key To Continue...");
//        new java.util.Scanner(System.in).nextLine();
		
	}
	
	@Override
	public IWizardPage getNextPage(){
		
		if (DesignPatternsCIMWizard.MementoCommandPatternBool==true){
			DesignPatternsCIMWizard.oMementoCommandPatternWizardPage.populateResourceList();
			return this.getWizard().getPage("Memento Command Pattern");
		} else if (DesignPatternsCIMWizard.ObserverPatternBool==true){
			DesignPatternsCIMWizard.oObserverPatternWizardPage.populateResourceList();
			return this.getWizard().getPage("Observer Pattern");
		} else if (DesignPatternsCIMWizard.BridgePatternBool==true){
			return this.getWizard().getPage("Bridge Pattern");
		} else {
//		if (DesignPatternsCIMWizard.ObserverPatternBool==false){
			return null;
		}
	}
	
	
}
