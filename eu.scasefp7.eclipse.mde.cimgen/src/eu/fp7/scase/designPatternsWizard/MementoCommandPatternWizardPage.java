package eu.fp7.scase.designPatternsWizard;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ServiceCIM.RESTfulServiceCIM;
import ServiceCIM.Resource;
import ServiceCIM.ServiceCIMFactory;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import DesignPatternsLayerCIM.AnnCRUDActivity;
import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.MementoPattern;
import DesignPatternsLayerCIM.Memento;
import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;

public class MementoCommandPatternWizardPage extends WizardPage {

	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	private Composite oMementoPatternGrid;
	private Label lblAvailableResources;
	private List oResourceList;
	private List oMementableResourcesList;
	private Label lblResourceRepresentationInstances;
	
	private Resource oResource;
	private int SelectedMemento;
	private int n;
	
	private Button btnMakeMementable;
	private Button btnDeleteMementable;
	private Spinner spnrMementoNum;
	
	private AnnotationModel oDesignPatternsCIM;
	private DesignPatternsLayerCIMFactory oDesignPatternsLayerCIMFactory;
	private DesignPatternsLayerCIM.AnnResource ooResource;
	private ServiceCIMFactory oServiceCIMFactory;
	
	private ArrayList<Memento> mementosList = new ArrayList<Memento>(); 
	
	
	protected MementoCommandPatternWizardPage(String pageName, RESTfulServiceCIM oRESTfulServiceCIM, AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super("Memento Command Pattern");
		setMessage("Choose which resources shall be mementable and define their memento's number.\r\n");
		setTitle("Memento Command Pattern");
		this.oRESTfulServiceCIM = oRESTfulServiceCIM;
	    this.bReloadExistingModels = bReloadExistingModels;
	    this.oDesignPatternsCIM=oDesignPatternsCIM;
	    this.oDesignPatternsLayerCIMFactory = DesignPatternsLayerCIMFactory.eINSTANCE;
	    this.oServiceCIMFactory = ServiceCIMFactory.eINSTANCE;
	    oResource = oServiceCIMFactory.createResource();
	    ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
		this.setControl(this.oWizardPageGrid);
		
		initializeBuilderGrid();

	}
	
	private void initializeBuilderGrid(){
		this.oMementoPatternGrid = new Composite(oWizardPageGrid, SWT.NONE);
		this.oMementoPatternGrid.setBounds(0, 0, 572, 262);
		
		this.lblResourceRepresentationInstances = new Label(oMementoPatternGrid, SWT.NONE);
		lblResourceRepresentationInstances.setBounds(297, 0, 265, 20);
		lblResourceRepresentationInstances.setText("Mementable Resources");
		
		this.oResourceList = new List(oMementoPatternGrid, SWT.V_SCROLL);
		this.oResourceList.setBounds(10, 26, 265, 80);
		populateResourceList();
		addResourceListListener();
		addCRUDListListener();
		
		this.oMementableResourcesList = new List(oMementoPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oMementableResourcesList.setBounds(297, 26, 265, 80);
		addMementosListListListener();
		
		this.lblAvailableResources = new Label(oMementoPatternGrid, SWT.NONE);
		lblAvailableResources.setBounds(10, 0, 265, 20);
		lblAvailableResources.setText("Available Resources");
		
		this.btnMakeMementable = new Button(oMementoPatternGrid, SWT.NONE);
		btnMakeMementable.setBounds(10, 112, 265, 30);
		btnMakeMementable.setText("Make Mementable");
		addbtnCreateMementableListener();
		
		this.btnDeleteMementable = new Button(oMementoPatternGrid, SWT.NONE);
		btnDeleteMementable.setBounds(297, 112, 265, 30);
		btnDeleteMementable.setText("Revert");
		
		Label lblNewLabel = new Label(oMementoPatternGrid, SWT.NONE);
		lblNewLabel.setBounds(297, 172, 185, 25);
		lblNewLabel.setText("Number of Mementos");
		
		this.spnrMementoNum = new Spinner(oMementoPatternGrid, SWT.BORDER);
		spnrMementoNum.setMaximum(1000);
		spnrMementoNum.setMinimum(1);
		spnrMementoNum.setBounds(495, 169, 67, 31);
		addSpnrMementoNumChangeLister();
		addbtnDeleteMementoListener();
	}
	
	private void addSpnrMementoNumChangeLister(){
		this.spnrMementoNum.addListener( SWT.Modify, new Listener(){
			@Override
			public void handleEvent(Event event) {
				mementosList.get(SelectedMemento).setMementoNum(Integer.parseInt(spnrMementoNum.getText()));
				System.out.println("Memento Num was set to "+Integer.parseInt(spnrMementoNum.getText()));
			}
		});
	}
	
	private void addbtnCreateMementableListener() {
		this.btnMakeMementable.addListener(SWT.Selection, new Listener(){

			@Override
			public void handleEvent(Event event) {
				
				Memento oMemento;
				oMemento = oDesignPatternsLayerCIMFactory.createMemento();		
				oMemento.setReferencesAnnResource(ooResource);
				DesignPatternsCIMWizard.gottenAnnResourcesList.add(ooResource);
				System.out.println("The list now contains" +DesignPatternsCIMWizard.gottenAnnResourcesList);

				oMemento.setMementoNum(1);
				//Does the memento Exist? 
				boolean existsFlag = false;
				for(Memento m : mementosList){
					if(m.getReferencesAnnResource().getAnnotatesResource().getName().equals(oMemento.getReferencesAnnResource().getAnnotatesResource().getName())){
						existsFlag = true;
					}
				}
				if(!existsFlag){
					mementosList.add(oMemento);
					System.out.println("Added memento on resource "+oMemento.getReferencesAnnResource().getAnnotatesResource().getName());
				}
				populateMementosList();
				
				setPageComplete(isPageCompleted());
				
			}
		});
	}
	
	private void addbtnDeleteMementoListener() {
		
		btnDeleteMementable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				
				AnnResource temp = mementosList.get(SelectedMemento).getReferencesAnnResource();
				try{
					DesignPatternsCIMWizard.gottenAnnResourcesList.remove(temp);
				}catch(Exception ee){
					
				}
				mementosList.remove(SelectedMemento);
				populateMementosList();
				
				//updateWidgetStatus();
				setPageComplete(isPageCompleted());
			}
		});
		
	}

	private void addCRUDListListener() {
	}


	private void addMementosListListListener() {
		this.oMementableResourcesList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				SelectedMemento = oMementableResourcesList.getSelectionIndex();
				loadMementoNum(SelectedMemento);
				
				spnrMementoNum.setSelection(mementosList.get(SelectedMemento).getMementoNum());
				
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
		
	}

	private void addResourceListListener() {
		// TODO Auto-generated method stub
		this.oResourceList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				oResource=oRESTfulServiceCIM.getHasResources().get(getResourceIndexByName(oResourceList.getSelection()[0]));
				ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
				ooResource.setAnnotatesResource(oResource);
				
//				System.out.printf("The selected resource %s has the following properties: \n", ooResource.getAnnotatesResource().getName());
//				for(int i=0; i< oResource.getHasProperty().size();i++){
//					System.out.println(ooResource.getAnnotatesResource().getHas().get(i).getName());
//				}
				setPageComplete(isPageCompleted());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	private int getResourceIndexByName(String strResourceName) {
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(this.oRESTfulServiceCIM.getHasResources().get(n).getName().equalsIgnoreCase(strResourceName)){
				return n;
			}
		}
		return -1; //Throw exception in production code
	}

	private void loadMementoNum(int selectedObserver) {
		
	}
	
	protected void populateResourceList(){
		this.oResourceList.removeAll();
		System.out.println("Populating resources list. \n");
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(!this.oRESTfulServiceCIM.getHasResources().get(n).isIsAlgorithmic()){
				if(!allreadyExists(this.oRESTfulServiceCIM.getHasResources().get(n))){
					this.oResourceList.add(this.oRESTfulServiceCIM.getHasResources().get(n).getName());
					}
				}
		}
	}
	
	private boolean allreadyExists(Resource oResource){
		System.out.println("Looking for resource "+oResource.getName()+"/n");
		for(AnnResource annRe : DesignPatternsCIMWizard.gottenAnnResourcesList){
			System.out.println("Found resource" + annRe.getAnnotatesResource().getName() +"/n");
			if(annRe.getAnnotatesResource().getName().equals(oResource.getName())){
				System.out.println("found /n");
				return true;
			}
		}
		System.out.println("not found /n");
		return false;
	}
	
	private void populateMementosList(){
		this.oMementableResourcesList.removeAll();
		for(int i = 0; i < this.mementosList.size(); i++){
			this.oMementableResourcesList.add(this.mementosList.get(i).getReferencesAnnResource().getAnnotatesResource().getName());
		}
	}
	
	private void updateResourceWidgets() {
		if(this.oResourceList.getItemCount() > 0){
			this.oResourceList.setEnabled(true);
		}
		else{
			this.oResourceList.setEnabled(false);
		}
	}
	
	private void updateRepresentationWidgets() {
		if(this.oMementableResourcesList.getItemCount() > 0){
			this.oMementableResourcesList.setEnabled(true);
		}
		else{
			this.oMementableResourcesList.setEnabled(false);
		}
	}
	
	private void updatePropertiesWidget() {

	}
	
	private void updateWidgetStatus(){
		updateResourceWidgets();
		updatePropertiesWidget();
		updateRepresentationWidgets();
		
	}
	
	private boolean isPageCompleted(){
		
		if(isValidCIMModel()){
			return true;
		}
		return false;
		
	}
	
	private boolean isValidCIMModel() {
		//validate resources
		boolean valid = true;
		for(Memento m : mementosList){
			try{
				System.out.println(m.getMementoNum());
			}catch(Exception e){
				valid = false;
				setMessage("Please set ".concat(m.getReferencesAnnResource().getAnnotatesResource().getName()).concat(" memento Number"));
			}
		}
		return valid;
	}
	
	
	public void CreateDsignPatternsCIM(){
		
		this.oDesignPatternsCIM.getHasAnnotatedElement().clear();
		this.oDesignPatternsCIM.getHasAnnotation().clear();
		
		addDesignPatternsModelToCIM();
		System.out.println("Added Memento Command pattern");
		
	}
	
	public void addDesignPatternsModelToCIM(){
		
		//create the Memento Command Design Pattern Model and add it to the DesignPattern CIM
		DesignPatternModel oDesignPatternModel = this.oDesignPatternsLayerCIMFactory.createDesignPatternModel();
		MementoPattern oMementoPattern = this.oDesignPatternsLayerCIMFactory.createMementoPattern();
		DesignPatternsLayerCIM.AnnResource annResource = oDesignPatternsLayerCIMFactory.createAnnResource();
	
		for(Memento m : mementosList){
			System.out.println("For resource "+m.getReferencesAnnResource().getAnnotatesResource().getName()+"'s memento");
			oMementoPattern.getHasMemento().add(m);
			this.oDesignPatternsCIM.getHasAnnotatedElement().add(m.getReferencesAnnResource());
		}
		
		this.oDesignPatternsCIM.getHasAnnotation().add(oMementoPattern);
		//oDesignPatternModel.getHasDesignPattern().add(oMementoPattern);
		//this.oDesignPatternsCIM.getHasAnnotation().add(oDesignPatternModel);
	}
	
	@Override
	public IWizardPage getNextPage(){
		 if (DesignPatternsCIMWizard.ObserverPatternBool==true){
			 DesignPatternsCIMWizard.oObserverPatternWizardPage.populateResourceList();
			return this.getWizard().getPage("Observer Pattern");
		} else if (DesignPatternsCIMWizard.BridgePatternBool==true){
			return this.getWizard().getPage("Bridge Pattern");
		} else {
			return null;
		}
	}
}
