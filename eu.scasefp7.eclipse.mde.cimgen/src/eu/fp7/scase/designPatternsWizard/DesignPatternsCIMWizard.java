package eu.fp7.scase.designPatternsWizard;

import java.util.ArrayList;

import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.AnnResource;
import ServiceCIM.RESTfulServiceCIM;
import eu.fp7.scase.cimGenerator.EcoreXMIDesignPatternsExtractor;

public class DesignPatternsCIMWizard extends Wizard {
	
	protected static DesignPatternsSelectionWizardPage oDesignPatternsSelectionWizardPage;
	protected static BuilderPatternWizardPage oBuilderPatternWizardPage;
	protected static ObserverPatternWizardPage oObserverPatternWizardPage;
	protected static MementoCommandPatternWizardPage oMementoCommandPatternWizardPage;
	protected static BridgePatternWizardPage oBridgePatternWizardPage;
	private String strOutputFolder;
	private AnnotationModel oDesignPatternsCIM;
	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	
	protected static boolean BuilderPatternBool;
	protected static boolean ObserverPatternBool;
	protected static boolean MementoCommandPatternBool;
	protected static boolean BridgePatternBool;
	protected static boolean otherBool;
	
	protected static ArrayList<AnnResource> gottenAnnResourcesList = new ArrayList<AnnResource>();
	
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	
	public DesignPatternsCIMWizard(String strOutputFolder, RESTfulServiceCIM oRESTfulServiceCIM, DesignPatternsLayerCIM.AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super();
		this.setWindowTitle("Design Patterns Wizard");
		this.strOutputFolder = strOutputFolder;
		this.oRESTfulServiceCIM = oRESTfulServiceCIM;
		this.oDesignPatternsCIM = oDesignPatternsCIM;
		this.bReloadExistingModels = bReloadExistingModels;
		DesignPatternsCIMWizard.BuilderPatternBool = false;
		DesignPatternsCIMWizard.ObserverPatternBool = false;
		DesignPatternsCIMWizard.MementoCommandPatternBool = false;
		DesignPatternsCIMWizard.BridgePatternBool = false;
		DesignPatternsCIMWizard.otherBool = false;
	}
	
	public void createControl(Composite parent) {
		//Creates and initializes the widgets
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
	}
	
	//Add a page for each Design Pattern that is going to be implemented
	@Override
	public void addPages() {
		
		this.oDesignPatternsSelectionWizardPage = new DesignPatternsSelectionWizardPage(strOutputFolder, oRESTfulServiceCIM, oDesignPatternsCIM, bReloadExistingModels);
		this.addPage(oDesignPatternsSelectionWizardPage);
		
		this.oBuilderPatternWizardPage = new BuilderPatternWizardPage(strOutputFolder, oRESTfulServiceCIM, oDesignPatternsCIM, bReloadExistingModels);
		this.addPage(oBuilderPatternWizardPage);
		
		this.oObserverPatternWizardPage = new ObserverPatternWizardPage(strOutputFolder, oRESTfulServiceCIM, oDesignPatternsCIM, bReloadExistingModels);
		this.addPage(oObserverPatternWizardPage);
		
		this.oMementoCommandPatternWizardPage = new MementoCommandPatternWizardPage(strOutputFolder, oRESTfulServiceCIM, oDesignPatternsCIM, bReloadExistingModels);
		this.addPage(oMementoCommandPatternWizardPage);
		
		this.oBridgePatternWizardPage = new BridgePatternWizardPage(strOutputFolder, oRESTfulServiceCIM, oDesignPatternsCIM, bReloadExistingModels);
		this.addPage(oBridgePatternWizardPage);
		
	}
	
	@Override
	public boolean performFinish() {
		if(this.oDesignPatternsSelectionWizardPage.isPageComplete() && this.oBuilderPatternWizardPage.isPageComplete() && this.oObserverPatternWizardPage.isPageComplete()){
			return true;
		}
		return false;
	}

	public void createDesignPatternsLayerCIM() {
		// gia kathe design pattern (diladi gia kathe page prepei na klithei to antistixo
		// createDesignPatternCim opws sto AuthenticationCIMWizard
		if(DesignPatternsCIMWizard.BuilderPatternBool){
			this.oBuilderPatternWizardPage.addDesignPatternsModelToCIM();
		}
		if(DesignPatternsCIMWizard.ObserverPatternBool){
			this.oObserverPatternWizardPage.addDesignPatternsModelToCIM();
		}
		if(DesignPatternsCIMWizard.MementoCommandPatternBool){
			this.oMementoCommandPatternWizardPage.addDesignPatternsModelToCIM();
		}
		if(DesignPatternsCIMWizard.BridgePatternBool){
			this.oBridgePatternWizardPage.addDesignPatternsModelToCIM();
		}
		exportDesignPatternsModel();
		//exportAuthenticationModelBackUp();
	}
	
	private void exportDesignPatternsModel(){
		EcoreXMIDesignPatternsExtractor oEcoreXMIDesignPatternsExtractor = new EcoreXMIDesignPatternsExtractor(this.strOutputFolder + "/CIMModels/" + this.oRESTfulServiceCIM.getName() + "DesignPatternsCIM.xmi", this.oRESTfulServiceCIM.getName());
		oEcoreXMIDesignPatternsExtractor.exportEcoreXMI(oDesignPatternsCIM);
	}
	
	public void exportAuthenticationModelBackUp(){
		//export also to a backup file to support 2nd run logic
		EcoreXMIDesignPatternsExtractor oEcoreXMIDesignPatternsExtractor = new EcoreXMIDesignPatternsExtractor(this.strOutputFolder + "/CIMModels/BackUp/" + this.oRESTfulServiceCIM.getName() + "DesignPatternsCIMBackUp.xmi", this.oRESTfulServiceCIM.getName());
		oEcoreXMIDesignPatternsExtractor.exportEcoreXMI(oDesignPatternsCIM);
	}
	
}
