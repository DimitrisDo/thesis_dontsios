package eu.fp7.scase.designPatternsWizard;

import java.util.ArrayList;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ServiceCIM.RESTfulServiceCIM;
import ServiceCIM.Resource;
import ServiceCIM.ServiceCIMFactory;

import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

import DesignPatternsLayerCIM.AnnCRUDActivity;
import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.ObserverPattern;
import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory;
import DesignPatternsLayerCIM.Observer;
import DesignPatternsLayerCIM.Notification;
import DesignPatternsLayerCIM.ObservableAnnCRUDActivity;

import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public class ObserverPatternWizardPage extends WizardPage {

	private RESTfulServiceCIM oRESTfulServiceCIM;
	private boolean bReloadExistingModels;
	private Composite oParentComposite;
	private Composite oWizardPageGrid;
	private Composite oBuilderPatternGrid;
	private Label lblAvailableResources;
	private List oResourceList;
	private List oObserversList;
	private List oCRUDList;
	private Label lblResourceRepresentationInstances;
	
	private Resource oResource;
	private int SelectedObserver;
	private int SelectedCRUD;
	private int n;
	
	private Button btnMakeObservable;
	private Button btnDeleteObservable;
	private Button btnSetSelectedCRUDVerb;
	
	//this list contains the information that is needed 
	private static ArrayList<Observer> ObserversList;
	//this also
	private static ArrayList<ObservableAnnCRUDActivity> ObservableAnnCRUDActivitiesList;

	private AnnotationModel oDesignPatternsCIM;
	private DesignPatternsLayerCIMFactory oDesignPatternsLayerCIMFactory;
	private DesignPatternsLayerCIM.AnnResource ooResource;
	private ServiceCIMFactory oServiceCIMFactory;
	
	
	protected ObserverPatternWizardPage(String pageName, RESTfulServiceCIM oRESTfulServiceCIM, AnnotationModel oDesignPatternsCIM, boolean bReloadExistingModels) {
		super("Observer Pattern");
		setMessage("Choose the r"
				+ "esources that you would like to be observed. Then set which CRUD Activities the observers can listen to.");
		setTitle("Observer Pattern");
		this.oRESTfulServiceCIM = oRESTfulServiceCIM;
	    this.bReloadExistingModels = bReloadExistingModels;
	    this.oDesignPatternsCIM=oDesignPatternsCIM;
	    ObserversList = new ArrayList<DesignPatternsLayerCIM.Observer>();
	    ObservableAnnCRUDActivitiesList = new ArrayList<DesignPatternsLayerCIM.ObservableAnnCRUDActivity>();
	    //make the boolean false to prevent the page from reapearing
	    //DesignPatternsCIMWizard.ObserverPatternPatternBool = false;
	    this.oDesignPatternsLayerCIMFactory = DesignPatternsLayerCIMFactory.eINSTANCE;
	    ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
	    this.oServiceCIMFactory = ServiceCIMFactory.eINSTANCE;
	    oResource = oServiceCIMFactory.createResource();
	    n=0;
	    
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		this.oParentComposite = parent;
		this.oWizardPageGrid = new Composite(oParentComposite, SWT.NONE);
		this.setControl(this.oWizardPageGrid);
		
		initializeBuilderGrid();
		
		Label lblCRUDVerbsIncludedIn = new Label(oBuilderPatternGrid, SWT.NONE);
		lblCRUDVerbsIncludedIn.setBounds(10, 145, 552, 20);
		lblCRUDVerbsIncludedIn.setText("Choose the CRUD Activity that will be able to be observed on this resource.");

	}
	
	private void initializeBuilderGrid(){
		this.oBuilderPatternGrid = new Composite(oWizardPageGrid, SWT.NONE);
		this.oBuilderPatternGrid.setBounds(0, 0, 572, 293);
		
		this.lblResourceRepresentationInstances = new Label(oBuilderPatternGrid, SWT.NONE);
		lblResourceRepresentationInstances.setBounds(297, 0, 246, 20);
		lblResourceRepresentationInstances.setText("Observable Resources");
		
		this.oResourceList = new List(oBuilderPatternGrid, SWT.V_SCROLL);
		this.oResourceList.setBounds(10, 26, 265, 80);
		populateResourceList();
		addResourceListListener();
		
		this.oCRUDList = new List(oBuilderPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oCRUDList.setBounds(10, 171, 552, 80);
		addCRUDListListener();
		
		this.oObserversList = new List(oBuilderPatternGrid, SWT.BORDER | SWT.V_SCROLL);
		oObserversList.setBounds(297, 26, 265, 80);
		addObserversListListListener();
		
		this.lblAvailableResources = new Label(oBuilderPatternGrid, SWT.NONE);
		lblAvailableResources.setBounds(10, 0, 246, 20);
		lblAvailableResources.setText("Available Resources");
		
		this.btnMakeObservable = new Button(oBuilderPatternGrid, SWT.NONE);
		btnMakeObservable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnMakeObservable.setBounds(10, 112, 265, 30);
		btnMakeObservable.setText("Make Observable");
		addbtnCreateObserverListener();
		
		this.btnDeleteObservable = new Button(oBuilderPatternGrid, SWT.NONE);
		btnDeleteObservable.setBounds(297, 112, 265, 30);
		btnDeleteObservable.setText("Delete Selected Resource");
		addbtnDeleteObserverListener();
		
		btnSetSelectedCRUDVerb = new Button(oBuilderPatternGrid, SWT.NONE);
		btnSetSelectedCRUDVerb.setBounds(10, 257, 552, 30);
		btnSetSelectedCRUDVerb.setText("Listen to this Verb");
		addbtnsetSelectedCRUDListener();
	}
	
	private void addbtnCreateObserverListener() {
		this.btnMakeObservable.addListener(SWT.Selection, new Listener(){

			@Override
			public void handleEvent(Event event) {
				
				Observer oObserver;
				oObserver = oDesignPatternsLayerCIMFactory.createObserver();				
				oObserver.setReferencesAnnResource(ooResource);
				
				//naming convention
				oObserver.setName("Observe_".concat(new Integer(n++).toString()).concat("::").concat(ooResource.getAnnotatesResource().getName()));
				
				ObserversList.add(oObserver);
				
				populateObserversList();
				setPageComplete(isPageCompleted());
				
				//updateWidgetStatus();
			}
		});
	}
	
	
	private void addbtnsetSelectedCRUDListener() {
		// TODO Auto-generated method stub
		btnSetSelectedCRUDVerb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				boolean CRUDActivityExistsForThis = false;
				boolean CRUDActivityVerbExistsForThis = false;
				int index = -1;
				int indexVerb = -1;
				ObservableAnnCRUDActivity oObservableAnnCRUDActivity = oDesignPatternsLayerCIMFactory.createObservableAnnCRUDActivity();;
				AnnCRUDActivity oAnnCRUDActivity = oDesignPatternsLayerCIMFactory.createAnnCRUDActivity();
				
				System.out.println(SelectedCRUD + " this is the selected CRUD");
				//Check if the list contains this Activity for the selected observer's resource
				for(int i = 0 ; i < ObservableAnnCRUDActivitiesList.size() ; i++){
					if(ObservableAnnCRUDActivitiesList.get(i).getReferencesAnnResource().getAnnotatesResource().getName() == ObserversList.get(SelectedObserver).getReferencesAnnResource().getAnnotatesResource().getName()){
						for(int j = 0 ; j < ObservableAnnCRUDActivitiesList.get(i).getIsObservedby().size(); j++){
							if(ObservableAnnCRUDActivitiesList.get(i).getIsObservedby().get(j).getName()==ObserversList.get(SelectedObserver).getName()){
								CRUDActivityExistsForThis = true;
								index = i;
							}
						}
						if(!CRUDActivityVerbExistsForThis && ObservableAnnCRUDActivitiesList.get(i).getExtendsAnnCRUDActivity().getAnnotatesCRUDActivity().getCRUDVerb().getName() == ObservableAnnCRUDActivitiesList.get(i).getReferencesAnnResource().getAnnotatesResource().getHasCRUDActivity().get(SelectedCRUD).getCRUDVerb().getName()){
							CRUDActivityVerbExistsForThis = true;
							indexVerb = i;
							System.out.println("Item found on "+i);
						}
//						for(int j = 0 ; j < ObservableAnnCRUDActivitiesList.size(); j++){
//						try{
//							if( i != j && ObservableAnnCRUDActivitiesList.get(i).getExtendsAnnCRUDActivity().getAnnotatesCRUDActivity().getCRUDVerb().getName() == ObservableAnnCRUDActivitiesList.get(j).getExtendsAnnCRUDActivity().getAnnotatesCRUDActivity().getCRUDVerb().getName() ){
//								CRUDActivityVerbExistsForThis = true;
//								indexVerb = j;
//								System.out.println("Item found on "+j);
//								break;
//							}
//						}
//						catch(Exception ex){
//							
//						}
//						}
					}
				}
				if(CRUDActivityExistsForThis && !CRUDActivityVerbExistsForThis){
					ObservableAnnCRUDActivitiesList.get(index).setReferencesAnnResource(ObserversList.get(SelectedObserver).getReferencesAnnResource());;
					//Set verb
					oAnnCRUDActivity.setAnnotatesCRUDActivity(ObserversList.get(SelectedObserver).getReferencesAnnResource().getAnnotatesResource().getHasCRUDActivity().get(SelectedCRUD));	
					ObservableAnnCRUDActivitiesList.get(index).setExtendsAnnCRUDActivity(oAnnCRUDActivity);
					//set Observes
					ObservableAnnCRUDActivitiesList.get(index).getIsObservedby().add(ObserversList.get(SelectedObserver));
					ObserversList.get(SelectedObserver).setObserves(ObservableAnnCRUDActivitiesList.get(index));
					System.out.println("Updated Old");
				}else if(!CRUDActivityExistsForThis && !CRUDActivityVerbExistsForThis){
					//selectedObserver
					oObservableAnnCRUDActivity.setReferencesAnnResource(ObserversList.get(SelectedObserver).getReferencesAnnResource());;
					//Set verb
					oAnnCRUDActivity.setAnnotatesCRUDActivity(ObserversList.get(SelectedObserver).getReferencesAnnResource().getAnnotatesResource().getHasCRUDActivity().get(SelectedCRUD));	
					oObservableAnnCRUDActivity.setExtendsAnnCRUDActivity(oAnnCRUDActivity);
					//set Observes
					oObservableAnnCRUDActivity.getIsObservedby().add(ObserversList.get(SelectedObserver));
					System.out.println(oObservableAnnCRUDActivity.toString()+" is observed by "+oObservableAnnCRUDActivity.getIsObservedby().get(oObservableAnnCRUDActivity.getIsObservedby().size()-1).getName());
					ObserversList.get(SelectedObserver).setObserves(oObservableAnnCRUDActivity);
					ObservableAnnCRUDActivitiesList.add(oObservableAnnCRUDActivity);
					System.out.println("Added new observer");
				}else if(CRUDActivityVerbExistsForThis){
					ObserversList.get(SelectedObserver).setObserves(ObservableAnnCRUDActivitiesList.get(indexVerb));
					ObservableAnnCRUDActivitiesList.get(indexVerb).getIsObservedby().add(ObserversList.get(SelectedObserver));
					System.out.println("Updated the ObservableAnnCRUDActivity that allready existed in the list");
				}
				
//				//Create ObservalbeAnnCRUDActivity and add it to the list
//				ObservableAnnCRUDActivity oObservableAnnCRUDActivity = oDesignPatternsLayerCIMFactory.createObservableAnnCRUDActivity();
//				
//				System.out.println(ObserversList.get(SelectedObserver).getObserves().getAnnotatesCRUDActivity().getCRUDVerb().getName());
//				
//				
//				ObservableAnnCRUDActivitiesList.add(oObservableAnnCRUDActivity);

//				System.out.println("The observer now contains: \n");
//				for(int j = 0 ; j < ObservableAnnCRUDActivitiesList.size(); j++){
//					System.out.println(ObservableAnnCRUDActivitiesList.get(j).getAnnotatesCRUDActivity().getCRUDVerb().getName());
//				}
				
//				ResourceRepresentationList.get(ResourceInstance).getHas().remove(SelectedProperty);
//				populatePropertiesList(ResourceInstance);
				
				
				setPageComplete(isPageCompleted());
			}
		});
	}
	
	private void addbtnDeleteObserverListener() {
		
		btnDeleteObservable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				
				for(int i = 0; i < ObservableAnnCRUDActivitiesList.size(); i++){
					if(ObservableAnnCRUDActivitiesList.get(i).getIsObservedby().remove(ObserversList.get(SelectedObserver)));
				}
				ObserversList.remove(SelectedObserver);
				populateObserversList();
				
				//TODO: also delete the curd verbs that have this observer
				//before the observer deletion
				
				//updateWidgetStatus();
				setPageComplete(isPageCompleted());
			}
		});
		
	}

	private void addCRUDListListener() {
		this.oCRUDList.addSelectionListener(new SelectionListener(){
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				SelectedCRUD = oCRUDList.getSelectionIndex();
				
				System.out.println(SelectedObserver);
				setPageComplete(isPageCompleted());
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
	}


	private void addObserversListListListener() {
		this.oObserversList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				SelectedObserver = oObserversList.getSelectionIndex();
				populateCRUDList(SelectedObserver);
//				
//				System.out.printf("The properties of the selected representation %s are : \n",ResourceRepresentationList.get(ResourceInstance).getRefersTo().getAnnotatesResource().getName());
//				for(int n = 0; n < ResourceRepresentationList.get(ResourceInstance).getHas().size(); n++){
//					System.out.println(ResourceRepresentationList.get(ResourceInstance).getHas().get(n).getAnnotatesProperty().getName());
//				}
//				setPageComplete(isPageCompleted());
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
			
		});
		
	}


	private void addResourceListListener() {
		// TODO Auto-generated method stub
		this.oResourceList.addSelectionListener(new SelectionListener(){

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				oResource=oRESTfulServiceCIM.getHasResources().get(getResourceIndexByName(oResourceList.getSelection()[0]));
				ooResource = oDesignPatternsLayerCIMFactory.createAnnResource();
				ooResource.setAnnotatesResource(oResource);
//				System.out.printf("The selected resource %s has the following properties: \n", ooResource.getAnnotatesResource().getName());
//				for(int i=0; i< oResource.getHasProperty().size();i++){
//					System.out.println(ooResource.getAnnotatesResource().getHas().get(i).getName());
//				}
				setPageComplete(isPageCompleted());

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	private int getResourceIndexByName(String strResourceName) {
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(this.oRESTfulServiceCIM.getHasResources().get(n).getName().equalsIgnoreCase(strResourceName)){
				return n;
			}
		}
		return -1; //Throw exception in production code
	}

	private void populateCRUDList(int selectedObserver) {
		this.oCRUDList.removeAll();
		int numOfVerbs = ObserversList.get(selectedObserver).getReferencesAnnResource().getAnnotatesResource().getHasCRUDActivity().size();
		for(int n = 0; n < numOfVerbs ; n++){
			this.oCRUDList.add(ObserversList.get(selectedObserver).getReferencesAnnResource().getAnnotatesResource().getHasCRUDActivity().get(n).getCRUDVerb().getName());
			//TODO: If selected highlight it
			for(int i = 0 ; i < ObservableAnnCRUDActivitiesList.size() ; i++){
				if(ObservableAnnCRUDActivitiesList.get(i).getReferencesAnnResource().getAnnotatesResource().getName() == ObserversList.get(SelectedObserver).getReferencesAnnResource().getAnnotatesResource().getName()){
					for(int j = 0 ; j < ObservableAnnCRUDActivitiesList.get(i).getIsObservedby().size(); j++){
						if(ObservableAnnCRUDActivitiesList.get(i).getIsObservedby().get(j).getName()==ObserversList.get(SelectedObserver).getName()){
							if(this.oCRUDList.getItem(n).equals(ObservableAnnCRUDActivitiesList.get(i).getExtendsAnnCRUDActivity().getAnnotatesCRUDActivity().getCRUDVerb().getName())){
								this.oCRUDList.select(n);
								System.out.println("Selected Verb: "+this.oCRUDList.getItem(n));
							}
						}
					}
					
				}
			}
//			if(ObserversList.get(selectedObserver).getObserves().getAnnotatesCRUDActivity().getCRUDVerb().getName() == this.oCRUDList.getItem(n)){
//				this.oCRUDList.select(n);
//			}
//				for(int i = 0 ; i < ObservableAnnCRUDActivitiesList.size() ; i++){
//					if(this.oCRUDList.getItem(n).equals(ObservableAnnCRUDActivitiesList.get(i).getAnnotatesCRUDActivity().getCRUDVerb().getName())){
//						this.oCRUDList.select(n);
//						System.out.println("Selected Verb: "+this.oCRUDList.getItem(n));
//					}
//					if(ObservableAnnCRUDActivitiesList.get(i).getAnnotatesCRUDActivity().getCRUDVerb().getName() == this.oCRUDList.getItem(n)){
//						this.oCRUDList.select(n);
//						System.out.println("Selected Verb: "+this.oCRUDList.getItem(n));
//					}
//				}
		}
	}
	
	protected void populateResourceList(){
		this.oResourceList.removeAll();
		for(int n = 0; n < this.oRESTfulServiceCIM.getHasResources().size(); n++){
			if(!this.oRESTfulServiceCIM.getHasResources().get(n).isIsAlgorithmic()){
				if(!allreadyExists(this.oRESTfulServiceCIM.getHasResources().get(n))){
					this.oResourceList.add(this.oRESTfulServiceCIM.getHasResources().get(n).getName());}}
		}
	}
	
	private boolean allreadyExists(Resource oResource){
		for(AnnResource annRe : DesignPatternsCIMWizard.gottenAnnResourcesList){
			if(annRe.getAnnotatesResource().getName().equals(oResource.getName())){
				System.out.println("found /n");
				return true;
			}
		}
		return false;
	}
	
	private void populateObserversList(){
		this.oObserversList.removeAll();
		for(int i = 0; i < this.ObserversList.size(); i++){
			this.oObserversList.add(this.ObserversList.get(i).getName());
		}
	}
	
	private void updateResourceWidgets() {
		if(this.oResourceList.getItemCount() > 0){
			this.oResourceList.setEnabled(true);
		}
		else{
			this.oResourceList.setEnabled(false);
		}
	}
	
	private void updateRepresentationWidgets() {
		if(this.oObserversList.getItemCount() > 0){
			this.oObserversList.setEnabled(true);
		}
		else{
			this.oObserversList.setEnabled(false);
		}
	}
	
	private void updatePropertiesWidget() {
		if(this.oCRUDList.getItemCount() > 0){
			this.oCRUDList.setEnabled(true);
		}
		else{
			this.oCRUDList.setEnabled(false);
		}
	}
	
	private void updateWidgetStatus(){
		updateResourceWidgets();
		updatePropertiesWidget();
		updateRepresentationWidgets();
		
	}
	
	private boolean isPageCompleted(){
		
		if(isValidCIMModel()){
			return true;
		}
		return false;
		
	}
	
	private boolean isValidCIMModel() {
		//validate resources
		boolean valid = true;
		for(int i = 0; i < ObserversList.size(); i ++){
			try{
				System.out.println(ObserversList.get(i).getObserves().getExtendsAnnCRUDActivity().getAnnotatesCRUDActivity().getCRUDVerb().getName());
				setMessage("Choose the resources that you would like to be observed. Then create observers that listen to seleced CRUD verb actions.");
			}catch(Exception e){
				valid = false;
				setMessage("Set ".concat(ObserversList.get(i).getName()).concat(" CRUD Verb"));
				return valid;
			}
			
		}
		return valid;
	}
	
	private boolean ResourceInstancesAreUnique() {
//		
//		for(int i = 0; i< this.ResourceRepresentationList.size(); i++){
//			for(int j = 0; j<this.ResourceRepresentationList.size(); j++){
//				if (i!=j){
//					if(ResourceRepresentationList.get(i).getHas()==ResourceRepresentationList.get(j).getHas()){
//						setErrorMessage("Every representation must be unique."+i+" and "+j+" representations are the same");
//						System.out.println("Shouldn't perform finish");
//						return false;
//					}
//				}
//			}
//		}
		return true;
	}
	
	public void CreateDsignPatternsCIM(){
		
		this.oDesignPatternsCIM.getHasAnnotatedElement().clear();
		this.oDesignPatternsCIM.getHasAnnotation().clear();
		
		addDesignPatternsModelToCIM();
		System.out.println("Added observer pattern");
		
	}
	
	public void addDesignPatternsModelToCIM(){
		
		//create the Observer Pattern Model and add it to the DesignPattern CIM
		DesignPatternModel oDesignPatternModel = this.oDesignPatternsLayerCIMFactory.createDesignPatternModel();
		ObserverPattern oObserverPattern = this.oDesignPatternsLayerCIMFactory.createObserverPattern();

		for(int i = 0 ; i < ObserversList.size() ; i++){
			oObserverPattern.getHasObserver().add(ObserversList.get(i));
			this.oDesignPatternsCIM.getHasAnnotatedElement().add(ObserversList.get(i).getReferencesAnnResource());
		}
		oDesignPatternModel.getHasDesignPattern().add(oObserverPattern);
		this.oDesignPatternsCIM.getHasAnnotation().add(oDesignPatternModel);
		this.oDesignPatternsCIM.getHasAnnotation().add(oObserverPattern);
		
		ObservableAnnCRUDActivity test  = this.oDesignPatternsLayerCIMFactory.createObservableAnnCRUDActivity();
		for(int i = 0 ; i < ObservableAnnCRUDActivitiesList.size() ; i ++){
			
			System.out.println("Added Annotated Element");
			if(!this.oDesignPatternsCIM.getHasAnnotatedElement().contains(ObservableAnnCRUDActivitiesList.get(i).getExtendsAnnCRUDActivity())){
				this.oDesignPatternsCIM.getHasAnnotatedElement().add(ObservableAnnCRUDActivitiesList.get(i).getExtendsAnnCRUDActivity());
			}
			
			System.out.println("Added Annotation");
			test = ObservableAnnCRUDActivitiesList.get(i);
			this.oDesignPatternsCIM.getHasAnnotation().add(test);
		}	
		
		for(int i = 0 ; i < this.oDesignPatternsCIM.getHasAnnotatedElement().size() ; i ++){
			System.out.println(this.oDesignPatternsCIM.getHasAnnotatedElement().get(i).toString());
		}
		for(int i = 0 ; i < this.oDesignPatternsCIM.getHasAnnotation().size() ; i ++){
			System.out.println(this.oDesignPatternsCIM.getHasAnnotation().get(i).toString());
		}

//		//debug
//		//ArrayList<String> includedProperties = new ArrayList<String>();
//		//System.out.println("Generating models. Adding Representations to the model:");
//		//for (int i = 0; i < ResourceRepresentationList.size(); i++){
//			//System.out.printf("%s \n",ResourceRepresentationList.get(i).getRefersTo().getAnnotatesResource().getName());
//		//}
//		//build a concrete builder for each representation
//		for(int i = 0; i < ResourceRepresentationList.size(); i++){
//			//add annotated elements to the CIM
//			this.oDesignPatternsCIM.getHasAnnotatedElement().add(ResourceRepresentationList.get(i).getRefersTo());
//			
//			oConcreteBuilder=this.oDesignPatternsLayerCIMFactory.createConcreteBuilder();
//			oConcreteBuilder.setBuildsRepresentation(ResourceRepresentationList.get(i));
//			oConcreteBuilder.setAssociatesAnnResource(ResourceRepresentationList.get(i).getRefersTo());
//			for(int j = 0; j < ResourceRepresentationList.get(i).getHas().size(); j++){
//				//if(!includedProperties.contains(ResourceRepresentationList.get(i).getHas().get(j).getAnnotatesProperty().getName())){
//					this.oDesignPatternsCIM.getHasAnnotatedElement().add(ResourceRepresentationList.get(i).getHas().get(j));
//					//includedProperties.add(ResourceRepresentationList.get(i).getHas().get(j).getAnnotatesProperty().getName());
//				//}
//			}
//			oBuilderPattern.getAssociatesAnnResource().add(ResourceRepresentationList.get(i).getRefersTo());
//			oDirector.getHasBuilder().add(oConcreteBuilder);
//			
//		}
//		
//		//add annotations to the CIM
//
//		oBuilderPattern.setHasDirector(oDirector);
//		oDesignPatternModel.getHasDesignPattern().add(oBuilderPattern);
//		this.oDesignPatternsCIM.getHasAnnotation().add(oBuilderPattern);
//		this.oDesignPatternsCIM.getHasAnnotation().add(oDesignPatternModel);
		
	}
	
	@Override
	public IWizardPage getNextPage(){
		if (DesignPatternsCIMWizard.BridgePatternBool==true){
			return this.getWizard().getPage("Bridge Pattern");
		} else{
			return null;
		}
	
	}
	
}
