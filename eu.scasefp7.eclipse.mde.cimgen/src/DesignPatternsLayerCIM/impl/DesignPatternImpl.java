/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DesignPatternImpl extends AnnotationImpl implements DesignPattern {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.DESIGN_PATTERN;
	}

} //DesignPatternImpl
