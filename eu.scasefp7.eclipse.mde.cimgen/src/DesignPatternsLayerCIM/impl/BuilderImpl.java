/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.Builder;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Representation;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.BuilderImpl#getAssociatesAnnResource <em>Associates Ann Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuilderImpl extends MinimalEObjectImpl.Container implements Builder {
	/**
	 * The cached value of the '{@link #getAssociatesAnnResource() <em>Associates Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatesAnnResource()
	 * @generated
	 * @ordered
	 */
	protected AnnResource associatesAnnResource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.BUILDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource getAssociatesAnnResource() {
		if (associatesAnnResource != null && associatesAnnResource.eIsProxy()) {
			InternalEObject oldAssociatesAnnResource = (InternalEObject)associatesAnnResource;
			associatesAnnResource = (AnnResource)eResolveProxy(oldAssociatesAnnResource);
			if (associatesAnnResource != oldAssociatesAnnResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE, oldAssociatesAnnResource, associatesAnnResource));
			}
		}
		return associatesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource basicGetAssociatesAnnResource() {
		return associatesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatesAnnResource(AnnResource newAssociatesAnnResource) {
		AnnResource oldAssociatesAnnResource = associatesAnnResource;
		associatesAnnResource = newAssociatesAnnResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE, oldAssociatesAnnResource, associatesAnnResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Representation buildRepresentation() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE:
				if (resolve) return getAssociatesAnnResource();
				return basicGetAssociatesAnnResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE:
				setAssociatesAnnResource((AnnResource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE:
				setAssociatesAnnResource((AnnResource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER__ASSOCIATES_ANN_RESOURCE:
				return associatesAnnResource != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DesignPatternsLayerCIMPackage.BUILDER___BUILD_REPRESENTATION:
				return buildRepresentation();
		}
		return super.eInvoke(operationID, arguments);
	}

} //BuilderImpl
