/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsLayerCIMFactoryImpl extends EFactoryImpl implements DesignPatternsLayerCIMFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DesignPatternsLayerCIMFactory init() {
		try {
			DesignPatternsLayerCIMFactory theDesignPatternsLayerCIMFactory = (DesignPatternsLayerCIMFactory)EPackage.Registry.INSTANCE.getEFactory(DesignPatternsLayerCIMPackage.eNS_URI);
			if (theDesignPatternsLayerCIMFactory != null) {
				return theDesignPatternsLayerCIMFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DesignPatternsLayerCIMFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerCIMFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DesignPatternsLayerCIMPackage.ANNOTATION_MODEL: return createAnnotationModel();
			case DesignPatternsLayerCIMPackage.ANNOTATED_ELEMENT: return createAnnotatedElement();
			case DesignPatternsLayerCIMPackage.ANNOTATION: return createAnnotation();
			case DesignPatternsLayerCIMPackage.ANN_PROPERTY: return createAnnProperty();
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE: return createAnnResource();
			case DesignPatternsLayerCIMPackage.ANN_ALGO_RESOURCE: return createAnnAlgoResource();
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN: return createDesignPattern();
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL: return createDesignPatternModel();
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN: return createBuilderPattern();
			case DesignPatternsLayerCIMPackage.DIRECTOR: return createDirector();
			case DesignPatternsLayerCIMPackage.BUILDER: return createBuilder();
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER: return createConcreteBuilder();
			case DesignPatternsLayerCIMPackage.REPRESENTATION: return createRepresentation();
			case DesignPatternsLayerCIMPackage.OBSERVER_PATTERN: return createObserverPattern();
			case DesignPatternsLayerCIMPackage.OBSERVER: return createObserver();
			case DesignPatternsLayerCIMPackage.NOTIFICATION: return createNotification();
			case DesignPatternsLayerCIMPackage.ANN_CRUD_ACTIVITY: return createAnnCRUDActivity();
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY: return createObservableAnnCRUDActivity();
			case DesignPatternsLayerCIMPackage.MEMENTO_PATTERN: return createMementoPattern();
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN: return createBridgePattern();
			case DesignPatternsLayerCIMPackage.MEMENTO: return createMemento();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationModel createAnnotationModel() {
		AnnotationModelImpl annotationModel = new AnnotationModelImpl();
		return annotationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement createAnnotatedElement() {
		AnnotatedElementImpl annotatedElement = new AnnotatedElementImpl();
		return annotatedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnProperty createAnnProperty() {
		AnnPropertyImpl annProperty = new AnnPropertyImpl();
		return annProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource createAnnResource() {
		AnnResourceImpl annResource = new AnnResourceImpl();
		return annResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnAlgoResource createAnnAlgoResource() {
		AnnAlgoResourceImpl annAlgoResource = new AnnAlgoResourceImpl();
		return annAlgoResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPattern createDesignPattern() {
		DesignPatternImpl designPattern = new DesignPatternImpl();
		return designPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternModel createDesignPatternModel() {
		DesignPatternModelImpl designPatternModel = new DesignPatternModelImpl();
		return designPatternModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuilderPattern createBuilderPattern() {
		BuilderPatternImpl builderPattern = new BuilderPatternImpl();
		return builderPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Director createDirector() {
		DirectorImpl director = new DirectorImpl();
		return director;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Builder createBuilder() {
		BuilderImpl builder = new BuilderImpl();
		return builder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteBuilder createConcreteBuilder() {
		ConcreteBuilderImpl concreteBuilder = new ConcreteBuilderImpl();
		return concreteBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Representation createRepresentation() {
		RepresentationImpl representation = new RepresentationImpl();
		return representation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObserverPattern createObserverPattern() {
		ObserverPatternImpl observerPattern = new ObserverPatternImpl();
		return observerPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Observer createObserver() {
		ObserverImpl observer = new ObserverImpl();
		return observer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Notification createNotification() {
		NotificationImpl notification = new NotificationImpl();
		return notification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnCRUDActivity createAnnCRUDActivity() {
		AnnCRUDActivityImpl annCRUDActivity = new AnnCRUDActivityImpl();
		return annCRUDActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObservableAnnCRUDActivity createObservableAnnCRUDActivity() {
		ObservableAnnCRUDActivityImpl observableAnnCRUDActivity = new ObservableAnnCRUDActivityImpl();
		return observableAnnCRUDActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MementoPattern createMementoPattern() {
		MementoPatternImpl mementoPattern = new MementoPatternImpl();
		return mementoPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BridgePattern createBridgePattern() {
		BridgePatternImpl bridgePattern = new BridgePatternImpl();
		return bridgePattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Memento createMemento() {
		MementoImpl memento = new MementoImpl();
		return memento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerCIMPackage getDesignPatternsLayerCIMPackage() {
		return (DesignPatternsLayerCIMPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DesignPatternsLayerCIMPackage getPackage() {
		return DesignPatternsLayerCIMPackage.eINSTANCE;
	}

} //DesignPatternsLayerCIMFactoryImpl
