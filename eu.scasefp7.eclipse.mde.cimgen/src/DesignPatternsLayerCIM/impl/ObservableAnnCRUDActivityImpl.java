/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnCRUDActivity;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.ObservableAnnCRUDActivity;
import DesignPatternsLayerCIM.Observer;
import ServiceCIM.CRUDVerb;
import ServiceCIM.Resource;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Observable Ann CRUD Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl#getIsObservedby <em>Is Observedby</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl#getExtendsAnnCRUDActivity <em>Extends Ann CRUD Activity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObservableAnnCRUDActivityImpl extends AnnotationImpl implements ObservableAnnCRUDActivity {
	/**
	 * The cached value of the '{@link #getReferencesAnnResource() <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnResource()
	 * @generated
	 * @ordered
	 */
	protected AnnResource referencesAnnResource;

	/**
	 * The cached value of the '{@link #getIsObservedby() <em>Is Observedby</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsObservedby()
	 * @generated
	 * @ordered
	 */
	protected EList<Observer> isObservedby;

	/**
	 * The cached value of the '{@link #getExtendsAnnCRUDActivity() <em>Extends Ann CRUD Activity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendsAnnCRUDActivity()
	 * @generated
	 * @ordered
	 */
	protected AnnCRUDActivity extendsAnnCRUDActivity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObservableAnnCRUDActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.OBSERVABLE_ANN_CRUD_ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource getReferencesAnnResource() {
		if (referencesAnnResource != null && referencesAnnResource.eIsProxy()) {
			InternalEObject oldReferencesAnnResource = (InternalEObject)referencesAnnResource;
			referencesAnnResource = (AnnResource)eResolveProxy(oldReferencesAnnResource);
			if (referencesAnnResource != oldReferencesAnnResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
			}
		}
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource basicGetReferencesAnnResource() {
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnResource(AnnResource newReferencesAnnResource) {
		AnnResource oldReferencesAnnResource = referencesAnnResource;
		referencesAnnResource = newReferencesAnnResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Observer> getIsObservedby() {
		if (isObservedby == null) {
			isObservedby = new EObjectResolvingEList<Observer>(Observer.class, this, DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY);
		}
		return isObservedby;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnCRUDActivity getExtendsAnnCRUDActivity() {
		if (extendsAnnCRUDActivity != null && extendsAnnCRUDActivity.eIsProxy()) {
			InternalEObject oldExtendsAnnCRUDActivity = (InternalEObject)extendsAnnCRUDActivity;
			extendsAnnCRUDActivity = (AnnCRUDActivity)eResolveProxy(oldExtendsAnnCRUDActivity);
			if (extendsAnnCRUDActivity != oldExtendsAnnCRUDActivity) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY, oldExtendsAnnCRUDActivity, extendsAnnCRUDActivity));
			}
		}
		return extendsAnnCRUDActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnCRUDActivity basicGetExtendsAnnCRUDActivity() {
		return extendsAnnCRUDActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendsAnnCRUDActivity(AnnCRUDActivity newExtendsAnnCRUDActivity) {
		AnnCRUDActivity oldExtendsAnnCRUDActivity = extendsAnnCRUDActivity;
		extendsAnnCRUDActivity = newExtendsAnnCRUDActivity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY, oldExtendsAnnCRUDActivity, extendsAnnCRUDActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CRUDVerb notifyObservers() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void registerObserver() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unregisterObserver() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setState() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getState() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE:
				if (resolve) return getReferencesAnnResource();
				return basicGetReferencesAnnResource();
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY:
				return getIsObservedby();
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY:
				if (resolve) return getExtendsAnnCRUDActivity();
				return basicGetExtendsAnnCRUDActivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY:
				getIsObservedby().clear();
				getIsObservedby().addAll((Collection<? extends Observer>)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY:
				setExtendsAnnCRUDActivity((AnnCRUDActivity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)null);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY:
				getIsObservedby().clear();
				return;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY:
				setExtendsAnnCRUDActivity((AnnCRUDActivity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE:
				return referencesAnnResource != null;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY:
				return isObservedby != null && !isObservedby.isEmpty();
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY:
				return extendsAnnCRUDActivity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY___NOTIFY_OBSERVERS:
				return notifyObservers();
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY___REGISTER_OBSERVER:
				registerObserver();
				return null;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY___UNREGISTER_OBSERVER:
				unregisterObserver();
				return null;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY___SET_STATE:
				setState();
				return null;
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY___GET_STATE:
				return getState();
		}
		return super.eInvoke(operationID, arguments);
	}

} //ObservableAnnCRUDActivityImpl
