/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Notification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.NotificationImpl#getNotificationId <em>Notification Id</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.NotificationImpl#getCRUDEvent <em>CRUD Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NotificationImpl extends MinimalEObjectImpl.Container implements Notification {
	/**
	 * The default value of the '{@link #getNotificationId() <em>Notification Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotificationId()
	 * @generated
	 * @ordered
	 */
	protected static final int NOTIFICATION_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNotificationId() <em>Notification Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotificationId()
	 * @generated
	 * @ordered
	 */
	protected int notificationId = NOTIFICATION_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getCRUDEvent() <em>CRUD Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCRUDEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String CRUD_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCRUDEvent() <em>CRUD Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCRUDEvent()
	 * @generated
	 * @ordered
	 */
	protected String crudEvent = CRUD_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NotificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.NOTIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNotificationId() {
		return notificationId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotificationId(int newNotificationId) {
		int oldNotificationId = notificationId;
		notificationId = newNotificationId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.NOTIFICATION__NOTIFICATION_ID, oldNotificationId, notificationId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCRUDEvent() {
		return crudEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCRUDEvent(String newCRUDEvent) {
		String oldCRUDEvent = crudEvent;
		crudEvent = newCRUDEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.NOTIFICATION__CRUD_EVENT, oldCRUDEvent, crudEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.NOTIFICATION__NOTIFICATION_ID:
				return getNotificationId();
			case DesignPatternsLayerCIMPackage.NOTIFICATION__CRUD_EVENT:
				return getCRUDEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.NOTIFICATION__NOTIFICATION_ID:
				setNotificationId((Integer)newValue);
				return;
			case DesignPatternsLayerCIMPackage.NOTIFICATION__CRUD_EVENT:
				setCRUDEvent((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.NOTIFICATION__NOTIFICATION_ID:
				setNotificationId(NOTIFICATION_ID_EDEFAULT);
				return;
			case DesignPatternsLayerCIMPackage.NOTIFICATION__CRUD_EVENT:
				setCRUDEvent(CRUD_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.NOTIFICATION__NOTIFICATION_ID:
				return notificationId != NOTIFICATION_ID_EDEFAULT;
			case DesignPatternsLayerCIMPackage.NOTIFICATION__CRUD_EVENT:
				return CRUD_EVENT_EDEFAULT == null ? crudEvent != null : !CRUD_EVENT_EDEFAULT.equals(crudEvent);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (notificationId: ");
		result.append(notificationId);
		result.append(", CRUDEvent: ");
		result.append(crudEvent);
		result.append(')');
		return result.toString();
	}

} //NotificationImpl
