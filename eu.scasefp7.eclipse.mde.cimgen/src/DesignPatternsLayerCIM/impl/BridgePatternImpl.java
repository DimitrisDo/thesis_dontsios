/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnAlgoResource;
import DesignPatternsLayerCIM.BridgePattern;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bridge Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.BridgePatternImpl#getAssociatesAnnAlgoResource <em>Associates Ann Algo Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.BridgePatternImpl#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.BridgePatternImpl#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BridgePatternImpl extends DesignPatternImpl implements BridgePattern {
	/**
	 * The cached value of the '{@link #getAssociatesAnnAlgoResource() <em>Associates Ann Algo Resource</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatesAnnAlgoResource()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnAlgoResource> associatesAnnAlgoResource;

	/**
	 * The default value of the '{@link #isBMakeBridgePatternForExternalService() <em>BMake Bridge Pattern For External Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBMakeBridgePatternForExternalService()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE_EDEFAULT = false;
	/**
	 * The cached value of the '{@link #isBMakeBridgePatternForExternalService() <em>BMake Bridge Pattern For External Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBMakeBridgePatternForExternalService()
	 * @generated
	 * @ordered
	 */
	protected boolean bMakeBridgePatternForExternalService = BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE_EDEFAULT;
	/**
	 * The default value of the '{@link #isBMakeBridgePatternForSearch() <em>BMake Bridge Pattern For Search</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBMakeBridgePatternForSearch()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BMAKE_BRIDGE_PATTERN_FOR_SEARCH_EDEFAULT = true;
	/**
	 * The cached value of the '{@link #isBMakeBridgePatternForSearch() <em>BMake Bridge Pattern For Search</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBMakeBridgePatternForSearch()
	 * @generated
	 * @ordered
	 */
	protected boolean bMakeBridgePatternForSearch = BMAKE_BRIDGE_PATTERN_FOR_SEARCH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BridgePatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.BRIDGE_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnAlgoResource> getAssociatesAnnAlgoResource() {
		if (associatesAnnAlgoResource == null) {
			associatesAnnAlgoResource = new EObjectResolvingEList<AnnAlgoResource>(AnnAlgoResource.class, this, DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE);
		}
		return associatesAnnAlgoResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBMakeBridgePatternForExternalService() {
		return bMakeBridgePatternForExternalService;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBMakeBridgePatternForExternalService(boolean newBMakeBridgePatternForExternalService) {
		boolean oldBMakeBridgePatternForExternalService = bMakeBridgePatternForExternalService;
		bMakeBridgePatternForExternalService = newBMakeBridgePatternForExternalService;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE, oldBMakeBridgePatternForExternalService, bMakeBridgePatternForExternalService));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBMakeBridgePatternForSearch() {
		return bMakeBridgePatternForSearch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBMakeBridgePatternForSearch(boolean newBMakeBridgePatternForSearch) {
		boolean oldBMakeBridgePatternForSearch = bMakeBridgePatternForSearch;
		bMakeBridgePatternForSearch = newBMakeBridgePatternForSearch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH, oldBMakeBridgePatternForSearch, bMakeBridgePatternForSearch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE:
				return getAssociatesAnnAlgoResource();
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE:
				return isBMakeBridgePatternForExternalService();
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH:
				return isBMakeBridgePatternForSearch();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE:
				getAssociatesAnnAlgoResource().clear();
				getAssociatesAnnAlgoResource().addAll((Collection<? extends AnnAlgoResource>)newValue);
				return;
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE:
				setBMakeBridgePatternForExternalService((Boolean)newValue);
				return;
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH:
				setBMakeBridgePatternForSearch((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE:
				getAssociatesAnnAlgoResource().clear();
				return;
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE:
				setBMakeBridgePatternForExternalService(BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE_EDEFAULT);
				return;
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH:
				setBMakeBridgePatternForSearch(BMAKE_BRIDGE_PATTERN_FOR_SEARCH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE:
				return associatesAnnAlgoResource != null && !associatesAnnAlgoResource.isEmpty();
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE:
				return bMakeBridgePatternForExternalService != BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE_EDEFAULT;
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH:
				return bMakeBridgePatternForSearch != BMAKE_BRIDGE_PATTERN_FOR_SEARCH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bMakeBridgePatternForExternalService: ");
		result.append(bMakeBridgePatternForExternalService);
		result.append(", bMakeBridgePatternForSearch: ");
		result.append(bMakeBridgePatternForSearch);
		result.append(')');
		return result.toString();
	}

} //BridgePatternImpl
