/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Representation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Representation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.RepresentationImpl#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.RepresentationImpl#getHas <em>Has</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.RepresentationImpl#getRefersTo <em>Refers To</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.RepresentationImpl#getPropertiesList <em>Properties List</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RepresentationImpl extends MinimalEObjectImpl.Container implements Representation {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHas() <em>Has</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHas()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnProperty> has;

	/**
	 * The cached value of the '{@link #getRefersTo() <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefersTo()
	 * @generated
	 * @ordered
	 */
	protected AnnResource refersTo;

	/**
	 * The cached value of the '{@link #getPropertiesList() <em>Properties List</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPropertiesList()
	 * @generated
	 * @ordered
	 */
	protected EList<EList> propertiesList;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RepresentationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.REPRESENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.REPRESENTATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnProperty> getHas() {
		if (has == null) {
			has = new EObjectResolvingEList<AnnProperty>(AnnProperty.class, this, DesignPatternsLayerCIMPackage.REPRESENTATION__HAS);
		}
		return has;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource getRefersTo() {
		if (refersTo != null && refersTo.eIsProxy()) {
			InternalEObject oldRefersTo = (InternalEObject)refersTo;
			refersTo = (AnnResource)eResolveProxy(oldRefersTo);
			if (refersTo != oldRefersTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO, oldRefersTo, refersTo));
			}
		}
		return refersTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource basicGetRefersTo() {
		return refersTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefersTo(AnnResource newRefersTo) {
		AnnResource oldRefersTo = refersTo;
		refersTo = newRefersTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO, oldRefersTo, refersTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EList> getPropertiesList() {
		if (propertiesList == null) {
			propertiesList = new EDataTypeUniqueEList<EList>(EList.class, this, DesignPatternsLayerCIMPackage.REPRESENTATION__PROPERTIES_LIST);
		}
		return propertiesList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.REPRESENTATION__NAME:
				return getName();
			case DesignPatternsLayerCIMPackage.REPRESENTATION__HAS:
				return getHas();
			case DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO:
				if (resolve) return getRefersTo();
				return basicGetRefersTo();
			case DesignPatternsLayerCIMPackage.REPRESENTATION__PROPERTIES_LIST:
				return getPropertiesList();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.REPRESENTATION__NAME:
				setName((String)newValue);
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__HAS:
				getHas().clear();
				getHas().addAll((Collection<? extends AnnProperty>)newValue);
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO:
				setRefersTo((AnnResource)newValue);
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__PROPERTIES_LIST:
				getPropertiesList().clear();
				getPropertiesList().addAll((Collection<? extends EList>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.REPRESENTATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__HAS:
				getHas().clear();
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO:
				setRefersTo((AnnResource)null);
				return;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__PROPERTIES_LIST:
				getPropertiesList().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.REPRESENTATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DesignPatternsLayerCIMPackage.REPRESENTATION__HAS:
				return has != null && !has.isEmpty();
			case DesignPatternsLayerCIMPackage.REPRESENTATION__REFERS_TO:
				return refersTo != null;
			case DesignPatternsLayerCIMPackage.REPRESENTATION__PROPERTIES_LIST:
				return propertiesList != null && !propertiesList.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", PropertiesList: ");
		result.append(propertiesList);
		result.append(')');
		return result.toString();
	}

} //RepresentationImpl
