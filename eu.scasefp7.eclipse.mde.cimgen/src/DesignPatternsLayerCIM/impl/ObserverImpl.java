/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Notification;
import DesignPatternsLayerCIM.ObservableAnnCRUDActivity;
import DesignPatternsLayerCIM.Observer;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Observer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObserverImpl#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObserverImpl#getObserves <em>Observes</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObserverImpl#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObserverImpl#getNotification <em>Notification</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.ObserverImpl#isCreatesNotification <em>Creates Notification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObserverImpl extends MinimalEObjectImpl.Container implements Observer {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getObserves() <em>Observes</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObserves()
	 * @generated
	 * @ordered
	 */
	protected ObservableAnnCRUDActivity observes;

	/**
	 * The cached value of the '{@link #getReferencesAnnResource() <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnResource()
	 * @generated
	 * @ordered
	 */
	protected AnnResource referencesAnnResource;

	/**
	 * The cached value of the '{@link #getNotification() <em>Notification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNotification()
	 * @generated
	 * @ordered
	 */
	protected EList<Notification> notification;

	/**
	 * The default value of the '{@link #isCreatesNotification() <em>Creates Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreatesNotification()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CREATES_NOTIFICATION_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isCreatesNotification() <em>Creates Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCreatesNotification()
	 * @generated
	 * @ordered
	 */
	protected boolean createsNotification = CREATES_NOTIFICATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObserverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.OBSERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.OBSERVER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObservableAnnCRUDActivity getObserves() {
		if (observes != null && observes.eIsProxy()) {
			InternalEObject oldObserves = (InternalEObject)observes;
			observes = (ObservableAnnCRUDActivity)eResolveProxy(oldObserves);
			if (observes != oldObserves) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.RESOLVE, DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES, oldObserves, observes));
			}
		}
		return observes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObservableAnnCRUDActivity basicGetObserves() {
		return observes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObserves(ObservableAnnCRUDActivity newObserves) {
		ObservableAnnCRUDActivity oldObserves = observes;
		observes = newObserves;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES, oldObserves, observes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource getReferencesAnnResource() {
		if (referencesAnnResource != null && referencesAnnResource.eIsProxy()) {
			InternalEObject oldReferencesAnnResource = (InternalEObject)referencesAnnResource;
			referencesAnnResource = (AnnResource)eResolveProxy(oldReferencesAnnResource);
			if (referencesAnnResource != oldReferencesAnnResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.RESOLVE, DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
			}
		}
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource basicGetReferencesAnnResource() {
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnResource(AnnResource newReferencesAnnResource) {
		AnnResource oldReferencesAnnResource = referencesAnnResource;
		referencesAnnResource = newReferencesAnnResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Notification> getNotification() {
		if (notification == null) {
			notification = new EObjectContainmentEList<Notification>(Notification.class, this, DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION);
		}
		return notification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCreatesNotification() {
		return createsNotification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreatesNotification(boolean newCreatesNotification) {
		boolean oldCreatesNotification = createsNotification;
		createsNotification = newCreatesNotification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.OBSERVER__CREATES_NOTIFICATION, oldCreatesNotification, createsNotification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void update() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION:
				return ((InternalEList<?>)getNotification()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVER__NAME:
				return getName();
			case DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES:
				if (resolve) return getObserves();
				return basicGetObserves();
			case DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE:
				if (resolve) return getReferencesAnnResource();
				return basicGetReferencesAnnResource();
			case DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION:
				return getNotification();
			case DesignPatternsLayerCIMPackage.OBSERVER__CREATES_NOTIFICATION:
				return isCreatesNotification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVER__NAME:
				setName((String)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES:
				setObserves((ObservableAnnCRUDActivity)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION:
				getNotification().clear();
				getNotification().addAll((Collection<? extends Notification>)newValue);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__CREATES_NOTIFICATION:
				setCreatesNotification((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES:
				setObserves((ObservableAnnCRUDActivity)null);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)null);
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION:
				getNotification().clear();
				return;
			case DesignPatternsLayerCIMPackage.OBSERVER__CREATES_NOTIFICATION:
				setCreatesNotification(CREATES_NOTIFICATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.OBSERVER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DesignPatternsLayerCIMPackage.OBSERVER__OBSERVES:
				return observes != null;
			case DesignPatternsLayerCIMPackage.OBSERVER__REFERENCES_ANN_RESOURCE:
				return referencesAnnResource != null;
			case DesignPatternsLayerCIMPackage.OBSERVER__NOTIFICATION:
				return notification != null && !notification.isEmpty();
			case DesignPatternsLayerCIMPackage.OBSERVER__CREATES_NOTIFICATION:
				return createsNotification != CREATES_NOTIFICATION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DesignPatternsLayerCIMPackage.OBSERVER___UPDATE:
				update();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", createsNotification: ");
		result.append(createsNotification);
		result.append(')');
		return result.toString();
	}

} //ObserverImpl
