/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnAlgoResource;
import DesignPatternsLayerCIM.AnnCRUDActivity;
import DesignPatternsLayerCIM.AnnProperty;
import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.AnnotatedElement;
import DesignPatternsLayerCIM.Annotation;
import DesignPatternsLayerCIM.AnnotationModel;
import DesignPatternsLayerCIM.BridgePattern;
import DesignPatternsLayerCIM.Builder;
import DesignPatternsLayerCIM.BuilderPattern;
import DesignPatternsLayerCIM.ConcreteBuilder;
import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Director;
import DesignPatternsLayerCIM.Memento;
import DesignPatternsLayerCIM.MementoPattern;
import DesignPatternsLayerCIM.Notification;
import DesignPatternsLayerCIM.ObservableAnnCRUDActivity;
import DesignPatternsLayerCIM.Observer;
import DesignPatternsLayerCIM.ObserverPattern;
import DesignPatternsLayerCIM.Representation;

import ServiceCIM.ServiceCIMPackage;

import ServiceCIM.impl.ServiceCIMPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsLayerCIMPackageImpl extends EPackageImpl implements DesignPatternsLayerCIMPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annAlgoResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass builderPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass directorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass builderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass concreteBuilderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass representationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass observerPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass observerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass notificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annCRUDActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass observableAnnCRUDActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mementoPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bridgePatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mementoEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DesignPatternsLayerCIMPackageImpl() {
		super(eNS_URI, DesignPatternsLayerCIMFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DesignPatternsLayerCIMPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DesignPatternsLayerCIMPackage init() {
		if (isInited) return (DesignPatternsLayerCIMPackage)EPackage.Registry.INSTANCE.getEPackage(DesignPatternsLayerCIMPackage.eNS_URI);

		// Obtain or create and register package
		DesignPatternsLayerCIMPackageImpl theDesignPatternsLayerCIMPackage = (DesignPatternsLayerCIMPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DesignPatternsLayerCIMPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DesignPatternsLayerCIMPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		ServiceCIMPackageImpl theServiceCIMPackage = (ServiceCIMPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ServiceCIMPackage.eNS_URI) instanceof ServiceCIMPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ServiceCIMPackage.eNS_URI) : ServiceCIMPackage.eINSTANCE);

		// Create package meta-data objects
		theDesignPatternsLayerCIMPackage.createPackageContents();
		theServiceCIMPackage.createPackageContents();

		// Initialize created meta-data
		theDesignPatternsLayerCIMPackage.initializePackageContents();
		theServiceCIMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDesignPatternsLayerCIMPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DesignPatternsLayerCIMPackage.eNS_URI, theDesignPatternsLayerCIMPackage);
		return theDesignPatternsLayerCIMPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotationModel() {
		return annotationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnnotationModel_Name() {
		return (EAttribute)annotationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotationModel_HasAnnotatedElement() {
		return (EReference)annotationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotationModel_HasAnnotation() {
		return (EReference)annotationModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatedElement() {
		return annotatedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotation() {
		return annotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnProperty() {
		return annPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnProperty_AnnotatesProperty() {
		return (EReference)annPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnResource() {
		return annResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnResource_AnnotatesResource() {
		return (EReference)annResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnResource_HasNotification() {
		return (EReference)annResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnAlgoResource() {
		return annAlgoResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnAlgoResource_AnnotatesAlgoResource() {
		return (EReference)annAlgoResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPattern() {
		return designPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPatternModel() {
		return designPatternModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternModel_HasDesignPattern() {
		return (EReference)designPatternModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuilderPattern() {
		return builderPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilderPattern_AssociatesAnnResource() {
		return (EReference)builderPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilderPattern_HasDirector() {
		return (EReference)builderPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirector() {
		return directorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDirector_HasBuilder() {
		return (EReference)directorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuilder() {
		return builderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBuilder_AssociatesAnnResource() {
		return (EReference)builderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getBuilder__BuildRepresentation() {
		return builderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConcreteBuilder() {
		return concreteBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConcreteBuilder_BuildsRepresentation() {
		return (EReference)concreteBuilderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getConcreteBuilder__BuildRepresentation() {
		return concreteBuilderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRepresentation() {
		return representationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRepresentation_Name() {
		return (EAttribute)representationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRepresentation_Has() {
		return (EReference)representationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRepresentation_RefersTo() {
		return (EReference)representationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRepresentation_PropertiesList() {
		return (EAttribute)representationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObserverPattern() {
		return observerPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObserverPattern_HasObserver() {
		return (EReference)observerPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObserver() {
		return observerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObserver_Name() {
		return (EAttribute)observerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObserver_Observes() {
		return (EReference)observerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObserver_ReferencesAnnResource() {
		return (EReference)observerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObserver_Notification() {
		return (EReference)observerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObserver_CreatesNotification() {
		return (EAttribute)observerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObserver__Update() {
		return observerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNotification() {
		return notificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNotification_NotificationId() {
		return (EAttribute)notificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNotification_CRUDEvent() {
		return (EAttribute)notificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnCRUDActivity() {
		return annCRUDActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnCRUDActivity_AnnotatesCRUDActivity() {
		return (EReference)annCRUDActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObservableAnnCRUDActivity() {
		return observableAnnCRUDActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObservableAnnCRUDActivity_ReferencesAnnResource() {
		return (EReference)observableAnnCRUDActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObservableAnnCRUDActivity_IsObservedby() {
		return (EReference)observableAnnCRUDActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObservableAnnCRUDActivity_ExtendsAnnCRUDActivity() {
		return (EReference)observableAnnCRUDActivityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObservableAnnCRUDActivity__NotifyObservers() {
		return observableAnnCRUDActivityEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObservableAnnCRUDActivity__RegisterObserver() {
		return observableAnnCRUDActivityEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObservableAnnCRUDActivity__UnregisterObserver() {
		return observableAnnCRUDActivityEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObservableAnnCRUDActivity__SetState() {
		return observableAnnCRUDActivityEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getObservableAnnCRUDActivity__GetState() {
		return observableAnnCRUDActivityEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMementoPattern() {
		return mementoPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMementoPattern_HasMemento() {
		return (EReference)mementoPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBridgePattern() {
		return bridgePatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBridgePattern_AssociatesAnnAlgoResource() {
		return (EReference)bridgePatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBridgePattern_BMakeBridgePatternForExternalService() {
		return (EAttribute)bridgePatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBridgePattern_BMakeBridgePatternForSearch() {
		return (EAttribute)bridgePatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMemento() {
		return mementoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMemento_MementoNum() {
		return (EAttribute)mementoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMemento_ReferencesAnnResource() {
		return (EReference)mementoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerCIMFactory getDesignPatternsLayerCIMFactory() {
		return (DesignPatternsLayerCIMFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		annotationModelEClass = createEClass(ANNOTATION_MODEL);
		createEAttribute(annotationModelEClass, ANNOTATION_MODEL__NAME);
		createEReference(annotationModelEClass, ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT);
		createEReference(annotationModelEClass, ANNOTATION_MODEL__HAS_ANNOTATION);

		annotatedElementEClass = createEClass(ANNOTATED_ELEMENT);

		annotationEClass = createEClass(ANNOTATION);

		annPropertyEClass = createEClass(ANN_PROPERTY);
		createEReference(annPropertyEClass, ANN_PROPERTY__ANNOTATES_PROPERTY);

		annResourceEClass = createEClass(ANN_RESOURCE);
		createEReference(annResourceEClass, ANN_RESOURCE__ANNOTATES_RESOURCE);
		createEReference(annResourceEClass, ANN_RESOURCE__HAS_NOTIFICATION);

		annAlgoResourceEClass = createEClass(ANN_ALGO_RESOURCE);
		createEReference(annAlgoResourceEClass, ANN_ALGO_RESOURCE__ANNOTATES_ALGO_RESOURCE);

		designPatternEClass = createEClass(DESIGN_PATTERN);

		designPatternModelEClass = createEClass(DESIGN_PATTERN_MODEL);
		createEReference(designPatternModelEClass, DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN);

		builderPatternEClass = createEClass(BUILDER_PATTERN);
		createEReference(builderPatternEClass, BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE);
		createEReference(builderPatternEClass, BUILDER_PATTERN__HAS_DIRECTOR);

		directorEClass = createEClass(DIRECTOR);
		createEReference(directorEClass, DIRECTOR__HAS_BUILDER);

		builderEClass = createEClass(BUILDER);
		createEReference(builderEClass, BUILDER__ASSOCIATES_ANN_RESOURCE);
		createEOperation(builderEClass, BUILDER___BUILD_REPRESENTATION);

		concreteBuilderEClass = createEClass(CONCRETE_BUILDER);
		createEReference(concreteBuilderEClass, CONCRETE_BUILDER__BUILDS_REPRESENTATION);
		createEOperation(concreteBuilderEClass, CONCRETE_BUILDER___BUILD_REPRESENTATION);

		representationEClass = createEClass(REPRESENTATION);
		createEAttribute(representationEClass, REPRESENTATION__NAME);
		createEReference(representationEClass, REPRESENTATION__HAS);
		createEReference(representationEClass, REPRESENTATION__REFERS_TO);
		createEAttribute(representationEClass, REPRESENTATION__PROPERTIES_LIST);

		observerPatternEClass = createEClass(OBSERVER_PATTERN);
		createEReference(observerPatternEClass, OBSERVER_PATTERN__HAS_OBSERVER);

		observerEClass = createEClass(OBSERVER);
		createEAttribute(observerEClass, OBSERVER__NAME);
		createEReference(observerEClass, OBSERVER__OBSERVES);
		createEReference(observerEClass, OBSERVER__REFERENCES_ANN_RESOURCE);
		createEReference(observerEClass, OBSERVER__NOTIFICATION);
		createEAttribute(observerEClass, OBSERVER__CREATES_NOTIFICATION);
		createEOperation(observerEClass, OBSERVER___UPDATE);

		notificationEClass = createEClass(NOTIFICATION);
		createEAttribute(notificationEClass, NOTIFICATION__NOTIFICATION_ID);
		createEAttribute(notificationEClass, NOTIFICATION__CRUD_EVENT);

		annCRUDActivityEClass = createEClass(ANN_CRUD_ACTIVITY);
		createEReference(annCRUDActivityEClass, ANN_CRUD_ACTIVITY__ANNOTATES_CRUD_ACTIVITY);

		observableAnnCRUDActivityEClass = createEClass(OBSERVABLE_ANN_CRUD_ACTIVITY);
		createEReference(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE);
		createEReference(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY);
		createEReference(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY);
		createEOperation(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY___NOTIFY_OBSERVERS);
		createEOperation(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY___REGISTER_OBSERVER);
		createEOperation(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY___UNREGISTER_OBSERVER);
		createEOperation(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY___SET_STATE);
		createEOperation(observableAnnCRUDActivityEClass, OBSERVABLE_ANN_CRUD_ACTIVITY___GET_STATE);

		mementoPatternEClass = createEClass(MEMENTO_PATTERN);
		createEReference(mementoPatternEClass, MEMENTO_PATTERN__HAS_MEMENTO);

		bridgePatternEClass = createEClass(BRIDGE_PATTERN);
		createEReference(bridgePatternEClass, BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE);
		createEAttribute(bridgePatternEClass, BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE);
		createEAttribute(bridgePatternEClass, BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH);

		mementoEClass = createEClass(MEMENTO);
		createEAttribute(mementoEClass, MEMENTO__MEMENTO_NUM);
		createEReference(mementoEClass, MEMENTO__REFERENCES_ANN_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ServiceCIMPackage theServiceCIMPackage = (ServiceCIMPackage)EPackage.Registry.INSTANCE.getEPackage(ServiceCIMPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		annPropertyEClass.getESuperTypes().add(this.getAnnotatedElement());
		annResourceEClass.getESuperTypes().add(this.getAnnotatedElement());
		annAlgoResourceEClass.getESuperTypes().add(this.getAnnotatedElement());
		designPatternEClass.getESuperTypes().add(this.getAnnotation());
		designPatternModelEClass.getESuperTypes().add(this.getAnnotation());
		builderPatternEClass.getESuperTypes().add(this.getDesignPattern());
		concreteBuilderEClass.getESuperTypes().add(this.getBuilder());
		observerPatternEClass.getESuperTypes().add(this.getDesignPattern());
		annCRUDActivityEClass.getESuperTypes().add(this.getAnnotatedElement());
		observableAnnCRUDActivityEClass.getESuperTypes().add(this.getAnnotation());
		mementoPatternEClass.getESuperTypes().add(this.getDesignPattern());
		bridgePatternEClass.getESuperTypes().add(this.getDesignPattern());

		// Initialize classes, features, and operations; add parameters
		initEClass(annotationModelEClass, AnnotationModel.class, "AnnotationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnnotationModel_Name(), ecorePackage.getEString(), "name", null, 0, 1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotationModel_HasAnnotatedElement(), this.getAnnotatedElement(), null, "hasAnnotatedElement", null, 1, -1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotationModel_HasAnnotation(), this.getAnnotation(), null, "hasAnnotation", null, 1, -1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotatedElementEClass, AnnotatedElement.class, "AnnotatedElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(annotationEClass, Annotation.class, "Annotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(annPropertyEClass, AnnProperty.class, "AnnProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnProperty_AnnotatesProperty(), theServiceCIMPackage.getProperty(), null, "annotatesProperty", null, 1, 1, AnnProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annResourceEClass, AnnResource.class, "AnnResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnResource_AnnotatesResource(), theServiceCIMPackage.getResource(), null, "annotatesResource", null, 1, 1, AnnResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnResource_HasNotification(), this.getNotification(), null, "hasNotification", null, 0, -1, AnnResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annAlgoResourceEClass, AnnAlgoResource.class, "AnnAlgoResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnAlgoResource_AnnotatesAlgoResource(), theServiceCIMPackage.getResource(), null, "annotatesAlgoResource", null, 1, 1, AnnAlgoResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(designPatternEClass, DesignPattern.class, "DesignPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(designPatternModelEClass, DesignPatternModel.class, "DesignPatternModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDesignPatternModel_HasDesignPattern(), this.getDesignPattern(), null, "hasDesignPattern", null, 0, -1, DesignPatternModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(builderPatternEClass, BuilderPattern.class, "BuilderPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBuilderPattern_AssociatesAnnResource(), this.getAnnResource(), null, "associatesAnnResource", null, 1, -1, BuilderPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBuilderPattern_HasDirector(), this.getDirector(), null, "hasDirector", null, 1, 1, BuilderPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(directorEClass, Director.class, "Director", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDirector_HasBuilder(), this.getBuilder(), null, "hasBuilder", null, 1, -1, Director.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(builderEClass, Builder.class, "Builder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBuilder_AssociatesAnnResource(), this.getAnnResource(), null, "associatesAnnResource", null, 1, 1, Builder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getBuilder__BuildRepresentation(), this.getRepresentation(), "buildRepresentation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(concreteBuilderEClass, ConcreteBuilder.class, "ConcreteBuilder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConcreteBuilder_BuildsRepresentation(), this.getRepresentation(), null, "buildsRepresentation", null, 1, 1, ConcreteBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getConcreteBuilder__BuildRepresentation(), this.getRepresentation(), "buildRepresentation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(representationEClass, Representation.class, "Representation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRepresentation_Name(), ecorePackage.getEString(), "name", null, 0, 1, Representation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepresentation_Has(), this.getAnnProperty(), null, "has", null, 0, -1, Representation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRepresentation_RefersTo(), this.getAnnResource(), null, "refersTo", null, 1, 1, Representation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRepresentation_PropertiesList(), ecorePackage.getEEList(), "PropertiesList", null, 0, -1, Representation.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(observerPatternEClass, ObserverPattern.class, "ObserverPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObserverPattern_HasObserver(), this.getObserver(), null, "hasObserver", null, 1, -1, ObserverPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(observerEClass, Observer.class, "Observer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObserver_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, Observer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObserver_Observes(), this.getObservableAnnCRUDActivity(), null, "observes", null, 1, 1, Observer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObserver_ReferencesAnnResource(), this.getAnnResource(), null, "referencesAnnResource", null, 1, 1, Observer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObserver_Notification(), this.getNotification(), null, "notification", null, 0, -1, Observer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObserver_CreatesNotification(), theXMLTypePackage.getBoolean(), "createsNotification", null, 0, 1, Observer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getObserver__Update(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(notificationEClass, Notification.class, "Notification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNotification_NotificationId(), theXMLTypePackage.getInt(), "notificationId", null, 0, 1, Notification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNotification_CRUDEvent(), theXMLTypePackage.getString(), "CRUDEvent", null, 0, 1, Notification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annCRUDActivityEClass, AnnCRUDActivity.class, "AnnCRUDActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnCRUDActivity_AnnotatesCRUDActivity(), theServiceCIMPackage.getCRUDActivity(), null, "annotatesCRUDActivity", null, 1, 1, AnnCRUDActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(observableAnnCRUDActivityEClass, ObservableAnnCRUDActivity.class, "ObservableAnnCRUDActivity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObservableAnnCRUDActivity_ReferencesAnnResource(), this.getAnnResource(), null, "referencesAnnResource", null, 1, 1, ObservableAnnCRUDActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObservableAnnCRUDActivity_IsObservedby(), this.getObserver(), null, "isObservedby", null, 0, -1, ObservableAnnCRUDActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObservableAnnCRUDActivity_ExtendsAnnCRUDActivity(), this.getAnnCRUDActivity(), null, "extendsAnnCRUDActivity", null, 1, 1, ObservableAnnCRUDActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getObservableAnnCRUDActivity__NotifyObservers(), theServiceCIMPackage.getCRUDVerb(), "notifyObservers", 0, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getObservableAnnCRUDActivity__RegisterObserver(), null, "registerObserver", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getObservableAnnCRUDActivity__UnregisterObserver(), null, "unregisterObserver", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getObservableAnnCRUDActivity__SetState(), null, "setState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getObservableAnnCRUDActivity__GetState(), theServiceCIMPackage.getResource(), "getState", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mementoPatternEClass, MementoPattern.class, "MementoPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMementoPattern_HasMemento(), this.getMemento(), null, "hasMemento", null, 1, -1, MementoPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bridgePatternEClass, BridgePattern.class, "BridgePattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBridgePattern_AssociatesAnnAlgoResource(), this.getAnnAlgoResource(), null, "associatesAnnAlgoResource", null, 1, -1, BridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBridgePattern_BMakeBridgePatternForExternalService(), theXMLTypePackage.getBoolean(), "bMakeBridgePatternForExternalService", "false", 0, 1, BridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBridgePattern_BMakeBridgePatternForSearch(), theXMLTypePackage.getBoolean(), "bMakeBridgePatternForSearch", "true", 0, 1, BridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mementoEClass, Memento.class, "Memento", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMemento_MementoNum(), theXMLTypePackage.getInt(), "mementoNum", "1", 0, 1, Memento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMemento_ReferencesAnnResource(), this.getAnnResource(), null, "referencesAnnResource", null, 1, 1, Memento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DesignPatternsLayerCIMPackageImpl
