/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Memento;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Memento</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.MementoImpl#getMementoNum <em>Memento Num</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.MementoImpl#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MementoImpl extends MinimalEObjectImpl.Container implements Memento {
	/**
	 * The default value of the '{@link #getMementoNum() <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMementoNum()
	 * @generated
	 * @ordered
	 */
	protected static final int MEMENTO_NUM_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getMementoNum() <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMementoNum()
	 * @generated
	 * @ordered
	 */
	protected int mementoNum = MEMENTO_NUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferencesAnnResource() <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnResource()
	 * @generated
	 * @ordered
	 */
	protected AnnResource referencesAnnResource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MementoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.MEMENTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMementoNum() {
		return mementoNum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMementoNum(int newMementoNum) {
		int oldMementoNum = mementoNum;
		mementoNum = newMementoNum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.MEMENTO__MEMENTO_NUM, oldMementoNum, mementoNum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource getReferencesAnnResource() {
		if (referencesAnnResource != null && referencesAnnResource.eIsProxy()) {
			InternalEObject oldReferencesAnnResource = (InternalEObject)referencesAnnResource;
			referencesAnnResource = (AnnResource)eResolveProxy(oldReferencesAnnResource);
			if (referencesAnnResource != oldReferencesAnnResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
			}
		}
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnResource basicGetReferencesAnnResource() {
		return referencesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnResource(AnnResource newReferencesAnnResource) {
		AnnResource oldReferencesAnnResource = referencesAnnResource;
		referencesAnnResource = newReferencesAnnResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE, oldReferencesAnnResource, referencesAnnResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.MEMENTO__MEMENTO_NUM:
				return getMementoNum();
			case DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE:
				if (resolve) return getReferencesAnnResource();
				return basicGetReferencesAnnResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.MEMENTO__MEMENTO_NUM:
				setMementoNum((Integer)newValue);
				return;
			case DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.MEMENTO__MEMENTO_NUM:
				setMementoNum(MEMENTO_NUM_EDEFAULT);
				return;
			case DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE:
				setReferencesAnnResource((AnnResource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.MEMENTO__MEMENTO_NUM:
				return mementoNum != MEMENTO_NUM_EDEFAULT;
			case DesignPatternsLayerCIMPackage.MEMENTO__REFERENCES_ANN_RESOURCE:
				return referencesAnnResource != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mementoNum: ");
		result.append(mementoNum);
		result.append(')');
		return result.toString();
	}

} //MementoImpl
