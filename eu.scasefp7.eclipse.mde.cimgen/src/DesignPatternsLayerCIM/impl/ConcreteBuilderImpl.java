/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.ConcreteBuilder;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Representation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concrete Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.ConcreteBuilderImpl#getBuildsRepresentation <em>Builds Representation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConcreteBuilderImpl extends BuilderImpl implements ConcreteBuilder {
	/**
	 * The cached value of the '{@link #getBuildsRepresentation() <em>Builds Representation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildsRepresentation()
	 * @generated
	 * @ordered
	 */
	protected Representation buildsRepresentation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConcreteBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.CONCRETE_BUILDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Representation getBuildsRepresentation() {
		return buildsRepresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBuildsRepresentation(Representation newBuildsRepresentation, NotificationChain msgs) {
		Representation oldBuildsRepresentation = buildsRepresentation;
		buildsRepresentation = newBuildsRepresentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION, oldBuildsRepresentation, newBuildsRepresentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuildsRepresentation(Representation newBuildsRepresentation) {
		if (newBuildsRepresentation != buildsRepresentation) {
			NotificationChain msgs = null;
			if (buildsRepresentation != null)
				msgs = ((InternalEObject)buildsRepresentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION, null, msgs);
			if (newBuildsRepresentation != null)
				msgs = ((InternalEObject)newBuildsRepresentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION, null, msgs);
			msgs = basicSetBuildsRepresentation(newBuildsRepresentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION, newBuildsRepresentation, newBuildsRepresentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION:
				return basicSetBuildsRepresentation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION:
				return getBuildsRepresentation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION:
				setBuildsRepresentation((Representation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION:
				setBuildsRepresentation((Representation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER__BUILDS_REPRESENTATION:
				return buildsRepresentation != null;
		}
		return super.eIsSet(featureID);
	}

} //ConcreteBuilderImpl
