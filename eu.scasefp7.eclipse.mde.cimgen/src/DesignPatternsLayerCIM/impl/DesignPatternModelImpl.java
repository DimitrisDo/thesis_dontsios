/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.DesignPattern;
import DesignPatternsLayerCIM.DesignPatternModel;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Pattern Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.DesignPatternModelImpl#getHasDesignPattern <em>Has Design Pattern</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DesignPatternModelImpl extends AnnotationImpl implements DesignPatternModel {
	/**
	 * The cached value of the '{@link #getHasDesignPattern() <em>Has Design Pattern</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasDesignPattern()
	 * @generated
	 * @ordered
	 */
	protected EList<DesignPattern> hasDesignPattern;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.DESIGN_PATTERN_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DesignPattern> getHasDesignPattern() {
		if (hasDesignPattern == null) {
			hasDesignPattern = new EObjectContainmentEList<DesignPattern>(DesignPattern.class, this, DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN);
		}
		return hasDesignPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN:
				return ((InternalEList<?>)getHasDesignPattern()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN:
				return getHasDesignPattern();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN:
				getHasDesignPattern().clear();
				getHasDesignPattern().addAll((Collection<? extends DesignPattern>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN:
				getHasDesignPattern().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN:
				return hasDesignPattern != null && !hasDesignPattern.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DesignPatternModelImpl
