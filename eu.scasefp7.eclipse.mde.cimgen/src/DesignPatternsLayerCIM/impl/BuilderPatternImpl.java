/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.BuilderPattern;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Director;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Builder Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.BuilderPatternImpl#getAssociatesAnnResource <em>Associates Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.BuilderPatternImpl#getHasDirector <em>Has Director</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuilderPatternImpl extends DesignPatternImpl implements BuilderPattern {
	/**
	 * The cached value of the '{@link #getAssociatesAnnResource() <em>Associates Ann Resource</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatesAnnResource()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnResource> associatesAnnResource;

	/**
	 * The cached value of the '{@link #getHasDirector() <em>Has Director</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasDirector()
	 * @generated
	 * @ordered
	 */
	protected Director hasDirector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuilderPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.BUILDER_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnResource> getAssociatesAnnResource() {
		if (associatesAnnResource == null) {
			associatesAnnResource = new EObjectResolvingEList<AnnResource>(AnnResource.class, this, DesignPatternsLayerCIMPackage.BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE);
		}
		return associatesAnnResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Director getHasDirector() {
		return hasDirector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasDirector(Director newHasDirector, NotificationChain msgs) {
		Director oldHasDirector = hasDirector;
		hasDirector = newHasDirector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR, oldHasDirector, newHasDirector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasDirector(Director newHasDirector) {
		if (newHasDirector != hasDirector) {
			NotificationChain msgs = null;
			if (hasDirector != null)
				msgs = ((InternalEObject)hasDirector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR, null, msgs);
			if (newHasDirector != null)
				msgs = ((InternalEObject)newHasDirector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR, null, msgs);
			msgs = basicSetHasDirector(newHasDirector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR, newHasDirector, newHasDirector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR:
				return basicSetHasDirector(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE:
				return getAssociatesAnnResource();
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR:
				return getHasDirector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE:
				getAssociatesAnnResource().clear();
				getAssociatesAnnResource().addAll((Collection<? extends AnnResource>)newValue);
				return;
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR:
				setHasDirector((Director)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE:
				getAssociatesAnnResource().clear();
				return;
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR:
				setHasDirector((Director)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE:
				return associatesAnnResource != null && !associatesAnnResource.isEmpty();
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN__HAS_DIRECTOR:
				return hasDirector != null;
		}
		return super.eIsSet(featureID);
	}

} //BuilderPatternImpl
