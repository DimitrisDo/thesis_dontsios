/**
 */
package DesignPatternsLayerCIM.impl;

import DesignPatternsLayerCIM.AnnResource;
import DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage;
import DesignPatternsLayerCIM.Notification;

import ServiceCIM.Resource;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ann Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.impl.AnnResourceImpl#getAnnotatesResource <em>Annotates Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.impl.AnnResourceImpl#getHasNotification <em>Has Notification</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnResourceImpl extends AnnotatedElementImpl implements AnnResource {
	/**
	 * The cached value of the '{@link #getAnnotatesResource() <em>Annotates Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatesResource()
	 * @generated
	 * @ordered
	 */
	protected Resource annotatesResource;

	/**
	 * The cached value of the '{@link #getHasNotification() <em>Has Notification</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasNotification()
	 * @generated
	 * @ordered
	 */
	protected EList<Notification> hasNotification;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerCIMPackage.Literals.ANN_RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getAnnotatesResource() {
		if (annotatesResource != null && annotatesResource.eIsProxy()) {
			InternalEObject oldAnnotatesResource = (InternalEObject)annotatesResource;
			annotatesResource = (Resource)eResolveProxy(oldAnnotatesResource);
			if (annotatesResource != oldAnnotatesResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.RESOLVE, DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE, oldAnnotatesResource, annotatesResource));
			}
		}
		return annotatesResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetAnnotatesResource() {
		return annotatesResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotatesResource(Resource newAnnotatesResource) {
		Resource oldAnnotatesResource = annotatesResource;
		annotatesResource = newAnnotatesResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, org.eclipse.emf.common.notify.Notification.SET, DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE, oldAnnotatesResource, annotatesResource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Notification> getHasNotification() {
		if (hasNotification == null) {
			hasNotification = new EObjectContainmentEList<Notification>(Notification.class, this, DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION);
		}
		return hasNotification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION:
				return ((InternalEList<?>)getHasNotification()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE:
				if (resolve) return getAnnotatesResource();
				return basicGetAnnotatesResource();
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION:
				return getHasNotification();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE:
				setAnnotatesResource((Resource)newValue);
				return;
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION:
				getHasNotification().clear();
				getHasNotification().addAll((Collection<? extends Notification>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE:
				setAnnotatesResource((Resource)null);
				return;
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION:
				getHasNotification().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__ANNOTATES_RESOURCE:
				return annotatesResource != null;
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE__HAS_NOTIFICATION:
				return hasNotification != null && !hasNotification.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AnnResourceImpl
