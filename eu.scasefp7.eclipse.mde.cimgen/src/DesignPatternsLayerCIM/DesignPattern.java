/**
 */
package DesignPatternsLayerCIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getDesignPattern()
 * @model
 * @generated
 */
public interface DesignPattern extends Annotation {
} // DesignPattern
