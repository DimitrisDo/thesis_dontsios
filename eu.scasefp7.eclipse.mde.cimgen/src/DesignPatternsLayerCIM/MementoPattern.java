/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memento Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.MementoPattern#getHasMemento <em>Has Memento</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getMementoPattern()
 * @model
 * @generated
 */
public interface MementoPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Has Memento</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Memento}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Memento</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Memento</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getMementoPattern_HasMemento()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Memento> getHasMemento();

} // MementoPattern
