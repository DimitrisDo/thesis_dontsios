/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMFactory
 * @model kind="package"
 * @generated
 */
public interface DesignPatternsLayerCIMPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "DesignPatternsLayerCIM";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/eu.scasefp7.eclipse.mde.m2m/Metamodels/DesignPatternsLayerCIMMetamodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Eu.fp7.scase";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DesignPatternsLayerCIMPackage eINSTANCE = DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl.init();

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnotationModelImpl <em>Annotation Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnotationModelImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotationModel()
	 * @generated
	 */
	int ANNOTATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__NAME = 0;

	/**
	 * The feature id for the '<em><b>Has Annotated Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Has Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__HAS_ANNOTATION = 2;

	/**
	 * The number of structural features of the '<em>Annotation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Annotation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnotatedElementImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotatedElement()
	 * @generated
	 */
	int ANNOTATED_ELEMENT = 1;

	/**
	 * The number of structural features of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnotationImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 2;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnPropertyImpl <em>Ann Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnPropertyImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnProperty()
	 * @generated
	 */
	int ANN_PROPERTY = 3;

	/**
	 * The feature id for the '<em><b>Annotates Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PROPERTY__ANNOTATES_PROPERTY = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PROPERTY_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PROPERTY_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnResourceImpl <em>Ann Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnResourceImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnResource()
	 * @generated
	 */
	int ANN_RESOURCE = 4;

	/**
	 * The feature id for the '<em><b>Annotates Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_RESOURCE__ANNOTATES_RESOURCE = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Notification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_RESOURCE__HAS_NOTIFICATION = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Ann Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_RESOURCE_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Ann Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_RESOURCE_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnAlgoResourceImpl <em>Ann Algo Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnAlgoResourceImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnAlgoResource()
	 * @generated
	 */
	int ANN_ALGO_RESOURCE = 5;

	/**
	 * The feature id for the '<em><b>Annotates Algo Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_ALGO_RESOURCE__ANNOTATES_ALGO_RESOURCE = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann Algo Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_ALGO_RESOURCE_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann Algo Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_ALGO_RESOURCE_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.DesignPatternImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDesignPattern()
	 * @generated
	 */
	int DESIGN_PATTERN = 6;

	/**
	 * The number of structural features of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_OPERATION_COUNT = ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.DesignPatternModelImpl <em>Design Pattern Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.DesignPatternModelImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDesignPatternModel()
	 * @generated
	 */
	int DESIGN_PATTERN_MODEL = 7;

	/**
	 * The feature id for the '<em><b>Has Design Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Design Pattern Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Design Pattern Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL_OPERATION_COUNT = ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.BuilderPatternImpl <em>Builder Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.BuilderPatternImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBuilderPattern()
	 * @generated
	 */
	int BUILDER_PATTERN = 8;

	/**
	 * The feature id for the '<em><b>Associates Ann Resource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Director</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_PATTERN__HAS_DIRECTOR = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Builder Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Builder Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.DirectorImpl <em>Director</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.DirectorImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDirector()
	 * @generated
	 */
	int DIRECTOR = 9;

	/**
	 * The feature id for the '<em><b>Has Builder</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTOR__HAS_BUILDER = 0;

	/**
	 * The number of structural features of the '<em>Director</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Director</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIRECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.BuilderImpl <em>Builder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.BuilderImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBuilder()
	 * @generated
	 */
	int BUILDER = 10;

	/**
	 * The feature id for the '<em><b>Associates Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER__ASSOCIATES_ANN_RESOURCE = 0;

	/**
	 * The number of structural features of the '<em>Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Build Representation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER___BUILD_REPRESENTATION = 0;

	/**
	 * The number of operations of the '<em>Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILDER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.ConcreteBuilderImpl <em>Concrete Builder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.ConcreteBuilderImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getConcreteBuilder()
	 * @generated
	 */
	int CONCRETE_BUILDER = 11;

	/**
	 * The feature id for the '<em><b>Associates Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_BUILDER__ASSOCIATES_ANN_RESOURCE = BUILDER__ASSOCIATES_ANN_RESOURCE;

	/**
	 * The feature id for the '<em><b>Builds Representation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_BUILDER__BUILDS_REPRESENTATION = BUILDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Concrete Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_BUILDER_FEATURE_COUNT = BUILDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Build Representation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_BUILDER___BUILD_REPRESENTATION = BUILDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Concrete Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_BUILDER_OPERATION_COUNT = BUILDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.RepresentationImpl <em>Representation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.RepresentationImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getRepresentation()
	 * @generated
	 */
	int REPRESENTATION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Has</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__HAS = 1;

	/**
	 * The feature id for the '<em><b>Refers To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__REFERS_TO = 2;

	/**
	 * The feature id for the '<em><b>Properties List</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION__PROPERTIES_LIST = 3;

	/**
	 * The number of structural features of the '<em>Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPRESENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.ObserverPatternImpl <em>Observer Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.ObserverPatternImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObserverPattern()
	 * @generated
	 */
	int OBSERVER_PATTERN = 13;

	/**
	 * The feature id for the '<em><b>Has Observer</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_PATTERN__HAS_OBSERVER = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Observer Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Observer Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.ObserverImpl <em>Observer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.ObserverImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObserver()
	 * @generated
	 */
	int OBSERVER = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER__NAME = 0;

	/**
	 * The feature id for the '<em><b>Observes</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER__OBSERVES = 1;

	/**
	 * The feature id for the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER__REFERENCES_ANN_RESOURCE = 2;

	/**
	 * The feature id for the '<em><b>Notification</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER__NOTIFICATION = 3;

	/**
	 * The feature id for the '<em><b>Creates Notification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER__CREATES_NOTIFICATION = 4;

	/**
	 * The number of structural features of the '<em>Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER___UPDATE = 0;

	/**
	 * The number of operations of the '<em>Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.NotificationImpl <em>Notification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.NotificationImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getNotification()
	 * @generated
	 */
	int NOTIFICATION = 15;

	/**
	 * The feature id for the '<em><b>Notification Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFICATION__NOTIFICATION_ID = 0;

	/**
	 * The feature id for the '<em><b>CRUD Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFICATION__CRUD_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Notification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFICATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Notification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.AnnCRUDActivityImpl <em>Ann CRUD Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.AnnCRUDActivityImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnCRUDActivity()
	 * @generated
	 */
	int ANN_CRUD_ACTIVITY = 16;

	/**
	 * The feature id for the '<em><b>Annotates CRUD Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_CRUD_ACTIVITY__ANNOTATES_CRUD_ACTIVITY = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann CRUD Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_CRUD_ACTIVITY_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann CRUD Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_CRUD_ACTIVITY_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl <em>Observable Ann CRUD Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObservableAnnCRUDActivity()
	 * @generated
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY = 17;

	/**
	 * The feature id for the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Observedby</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY = ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extends Ann CRUD Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY = ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Observable Ann CRUD Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Notify Observers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY___NOTIFY_OBSERVERS = ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Register Observer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY___REGISTER_OBSERVER = ANNOTATION_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Unregister Observer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY___UNREGISTER_OBSERVER = ANNOTATION_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Set State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY___SET_STATE = ANNOTATION_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY___GET_STATE = ANNOTATION_OPERATION_COUNT + 4;

	/**
	 * The number of operations of the '<em>Observable Ann CRUD Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSERVABLE_ANN_CRUD_ACTIVITY_OPERATION_COUNT = ANNOTATION_OPERATION_COUNT + 5;


	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.MementoPatternImpl <em>Memento Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.MementoPatternImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getMementoPattern()
	 * @generated
	 */
	int MEMENTO_PATTERN = 18;

	/**
	 * The feature id for the '<em><b>Has Memento</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO_PATTERN__HAS_MEMENTO = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Memento Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Memento Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.BridgePatternImpl <em>Bridge Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.BridgePatternImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBridgePattern()
	 * @generated
	 */
	int BRIDGE_PATTERN = 19;

	/**
	 * The feature id for the '<em><b>Associates Ann Algo Resource</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH = DESIGN_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Bridge Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRIDGE_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Bridge Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BRIDGE_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerCIM.impl.MementoImpl <em>Memento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerCIM.impl.MementoImpl
	 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getMemento()
	 * @generated
	 */
	int MEMENTO = 20;

	/**
	 * The feature id for the '<em><b>Memento Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO__MEMENTO_NUM = 0;

	/**
	 * The feature id for the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO__REFERENCES_ANN_RESOURCE = 1;

	/**
	 * The number of structural features of the '<em>Memento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Memento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMENTO_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnotationModel <em>Annotation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Model</em>'.
	 * @see DesignPatternsLayerCIM.AnnotationModel
	 * @generated
	 */
	EClass getAnnotationModel();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.AnnotationModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerCIM.AnnotationModel#getName()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EAttribute getAnnotationModel_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.AnnotationModel#getHasAnnotatedElement <em>Has Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Annotated Element</em>'.
	 * @see DesignPatternsLayerCIM.AnnotationModel#getHasAnnotatedElement()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EReference getAnnotationModel_HasAnnotatedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.AnnotationModel#getHasAnnotation <em>Has Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Annotation</em>'.
	 * @see DesignPatternsLayerCIM.AnnotationModel#getHasAnnotation()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EReference getAnnotationModel_HasAnnotation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Element</em>'.
	 * @see DesignPatternsLayerCIM.AnnotatedElement
	 * @generated
	 */
	EClass getAnnotatedElement();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see DesignPatternsLayerCIM.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnProperty <em>Ann Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Property</em>'.
	 * @see DesignPatternsLayerCIM.AnnProperty
	 * @generated
	 */
	EClass getAnnProperty();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.AnnProperty#getAnnotatesProperty <em>Annotates Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates Property</em>'.
	 * @see DesignPatternsLayerCIM.AnnProperty#getAnnotatesProperty()
	 * @see #getAnnProperty()
	 * @generated
	 */
	EReference getAnnProperty_AnnotatesProperty();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnResource <em>Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.AnnResource
	 * @generated
	 */
	EClass getAnnResource();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.AnnResource#getAnnotatesResource <em>Annotates Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates Resource</em>'.
	 * @see DesignPatternsLayerCIM.AnnResource#getAnnotatesResource()
	 * @see #getAnnResource()
	 * @generated
	 */
	EReference getAnnResource_AnnotatesResource();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.AnnResource#getHasNotification <em>Has Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Notification</em>'.
	 * @see DesignPatternsLayerCIM.AnnResource#getHasNotification()
	 * @see #getAnnResource()
	 * @generated
	 */
	EReference getAnnResource_HasNotification();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnAlgoResource <em>Ann Algo Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Algo Resource</em>'.
	 * @see DesignPatternsLayerCIM.AnnAlgoResource
	 * @generated
	 */
	EClass getAnnAlgoResource();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.AnnAlgoResource#getAnnotatesAlgoResource <em>Annotates Algo Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates Algo Resource</em>'.
	 * @see DesignPatternsLayerCIM.AnnAlgoResource#getAnnotatesAlgoResource()
	 * @see #getAnnAlgoResource()
	 * @generated
	 */
	EReference getAnnAlgoResource_AnnotatesAlgoResource();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.DesignPattern <em>Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern</em>'.
	 * @see DesignPatternsLayerCIM.DesignPattern
	 * @generated
	 */
	EClass getDesignPattern();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.DesignPatternModel <em>Design Pattern Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern Model</em>'.
	 * @see DesignPatternsLayerCIM.DesignPatternModel
	 * @generated
	 */
	EClass getDesignPatternModel();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.DesignPatternModel#getHasDesignPattern <em>Has Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Design Pattern</em>'.
	 * @see DesignPatternsLayerCIM.DesignPatternModel#getHasDesignPattern()
	 * @see #getDesignPatternModel()
	 * @generated
	 */
	EReference getDesignPatternModel_HasDesignPattern();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.BuilderPattern <em>Builder Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Builder Pattern</em>'.
	 * @see DesignPatternsLayerCIM.BuilderPattern
	 * @generated
	 */
	EClass getBuilderPattern();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerCIM.BuilderPattern#getAssociatesAnnResource <em>Associates Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Associates Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.BuilderPattern#getAssociatesAnnResource()
	 * @see #getBuilderPattern()
	 * @generated
	 */
	EReference getBuilderPattern_AssociatesAnnResource();

	/**
	 * Returns the meta object for the containment reference '{@link DesignPatternsLayerCIM.BuilderPattern#getHasDirector <em>Has Director</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Has Director</em>'.
	 * @see DesignPatternsLayerCIM.BuilderPattern#getHasDirector()
	 * @see #getBuilderPattern()
	 * @generated
	 */
	EReference getBuilderPattern_HasDirector();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Director <em>Director</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Director</em>'.
	 * @see DesignPatternsLayerCIM.Director
	 * @generated
	 */
	EClass getDirector();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.Director#getHasBuilder <em>Has Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Builder</em>'.
	 * @see DesignPatternsLayerCIM.Director#getHasBuilder()
	 * @see #getDirector()
	 * @generated
	 */
	EReference getDirector_HasBuilder();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Builder <em>Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Builder</em>'.
	 * @see DesignPatternsLayerCIM.Builder
	 * @generated
	 */
	EClass getBuilder();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.Builder#getAssociatesAnnResource <em>Associates Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associates Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.Builder#getAssociatesAnnResource()
	 * @see #getBuilder()
	 * @generated
	 */
	EReference getBuilder_AssociatesAnnResource();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.Builder#buildRepresentation() <em>Build Representation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Build Representation</em>' operation.
	 * @see DesignPatternsLayerCIM.Builder#buildRepresentation()
	 * @generated
	 */
	EOperation getBuilder__BuildRepresentation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.ConcreteBuilder <em>Concrete Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concrete Builder</em>'.
	 * @see DesignPatternsLayerCIM.ConcreteBuilder
	 * @generated
	 */
	EClass getConcreteBuilder();

	/**
	 * Returns the meta object for the containment reference '{@link DesignPatternsLayerCIM.ConcreteBuilder#getBuildsRepresentation <em>Builds Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Builds Representation</em>'.
	 * @see DesignPatternsLayerCIM.ConcreteBuilder#getBuildsRepresentation()
	 * @see #getConcreteBuilder()
	 * @generated
	 */
	EReference getConcreteBuilder_BuildsRepresentation();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ConcreteBuilder#buildRepresentation() <em>Build Representation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Build Representation</em>' operation.
	 * @see DesignPatternsLayerCIM.ConcreteBuilder#buildRepresentation()
	 * @generated
	 */
	EOperation getConcreteBuilder__BuildRepresentation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Representation <em>Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Representation</em>'.
	 * @see DesignPatternsLayerCIM.Representation
	 * @generated
	 */
	EClass getRepresentation();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Representation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerCIM.Representation#getName()
	 * @see #getRepresentation()
	 * @generated
	 */
	EAttribute getRepresentation_Name();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerCIM.Representation#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has</em>'.
	 * @see DesignPatternsLayerCIM.Representation#getHas()
	 * @see #getRepresentation()
	 * @generated
	 */
	EReference getRepresentation_Has();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.Representation#getRefersTo <em>Refers To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refers To</em>'.
	 * @see DesignPatternsLayerCIM.Representation#getRefersTo()
	 * @see #getRepresentation()
	 * @generated
	 */
	EReference getRepresentation_RefersTo();

	/**
	 * Returns the meta object for the attribute list '{@link DesignPatternsLayerCIM.Representation#getPropertiesList <em>Properties List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Properties List</em>'.
	 * @see DesignPatternsLayerCIM.Representation#getPropertiesList()
	 * @see #getRepresentation()
	 * @generated
	 */
	EAttribute getRepresentation_PropertiesList();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.ObserverPattern <em>Observer Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observer Pattern</em>'.
	 * @see DesignPatternsLayerCIM.ObserverPattern
	 * @generated
	 */
	EClass getObserverPattern();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.ObserverPattern#getHasObserver <em>Has Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Observer</em>'.
	 * @see DesignPatternsLayerCIM.ObserverPattern#getHasObserver()
	 * @see #getObserverPattern()
	 * @generated
	 */
	EReference getObserverPattern_HasObserver();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Observer <em>Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observer</em>'.
	 * @see DesignPatternsLayerCIM.Observer
	 * @generated
	 */
	EClass getObserver();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Observer#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerCIM.Observer#getName()
	 * @see #getObserver()
	 * @generated
	 */
	EAttribute getObserver_Name();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.Observer#getObserves <em>Observes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observes</em>'.
	 * @see DesignPatternsLayerCIM.Observer#getObserves()
	 * @see #getObserver()
	 * @generated
	 */
	EReference getObserver_Observes();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.Observer#getReferencesAnnResource <em>References Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.Observer#getReferencesAnnResource()
	 * @see #getObserver()
	 * @generated
	 */
	EReference getObserver_ReferencesAnnResource();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.Observer#getNotification <em>Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Notification</em>'.
	 * @see DesignPatternsLayerCIM.Observer#getNotification()
	 * @see #getObserver()
	 * @generated
	 */
	EReference getObserver_Notification();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Observer#isCreatesNotification <em>Creates Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Creates Notification</em>'.
	 * @see DesignPatternsLayerCIM.Observer#isCreatesNotification()
	 * @see #getObserver()
	 * @generated
	 */
	EAttribute getObserver_CreatesNotification();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.Observer#update() <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see DesignPatternsLayerCIM.Observer#update()
	 * @generated
	 */
	EOperation getObserver__Update();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Notification <em>Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Notification</em>'.
	 * @see DesignPatternsLayerCIM.Notification
	 * @generated
	 */
	EClass getNotification();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Notification#getNotificationId <em>Notification Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Notification Id</em>'.
	 * @see DesignPatternsLayerCIM.Notification#getNotificationId()
	 * @see #getNotification()
	 * @generated
	 */
	EAttribute getNotification_NotificationId();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Notification#getCRUDEvent <em>CRUD Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>CRUD Event</em>'.
	 * @see DesignPatternsLayerCIM.Notification#getCRUDEvent()
	 * @see #getNotification()
	 * @generated
	 */
	EAttribute getNotification_CRUDEvent();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.AnnCRUDActivity <em>Ann CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann CRUD Activity</em>'.
	 * @see DesignPatternsLayerCIM.AnnCRUDActivity
	 * @generated
	 */
	EClass getAnnCRUDActivity();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.AnnCRUDActivity#getAnnotatesCRUDActivity <em>Annotates CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates CRUD Activity</em>'.
	 * @see DesignPatternsLayerCIM.AnnCRUDActivity#getAnnotatesCRUDActivity()
	 * @see #getAnnCRUDActivity()
	 * @generated
	 */
	EReference getAnnCRUDActivity_AnnotatesCRUDActivity();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity <em>Observable Ann CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Observable Ann CRUD Activity</em>'.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity
	 * @generated
	 */
	EClass getObservableAnnCRUDActivity();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getReferencesAnnResource <em>References Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getReferencesAnnResource()
	 * @see #getObservableAnnCRUDActivity()
	 * @generated
	 */
	EReference getObservableAnnCRUDActivity_ReferencesAnnResource();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getIsObservedby <em>Is Observedby</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Is Observedby</em>'.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getIsObservedby()
	 * @see #getObservableAnnCRUDActivity()
	 * @generated
	 */
	EReference getObservableAnnCRUDActivity_IsObservedby();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getExtendsAnnCRUDActivity <em>Extends Ann CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extends Ann CRUD Activity</em>'.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getExtendsAnnCRUDActivity()
	 * @see #getObservableAnnCRUDActivity()
	 * @generated
	 */
	EReference getObservableAnnCRUDActivity_ExtendsAnnCRUDActivity();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#notifyObservers() <em>Notify Observers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Notify Observers</em>' operation.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#notifyObservers()
	 * @generated
	 */
	EOperation getObservableAnnCRUDActivity__NotifyObservers();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#registerObserver() <em>Register Observer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Register Observer</em>' operation.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#registerObserver()
	 * @generated
	 */
	EOperation getObservableAnnCRUDActivity__RegisterObserver();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#unregisterObserver() <em>Unregister Observer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unregister Observer</em>' operation.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#unregisterObserver()
	 * @generated
	 */
	EOperation getObservableAnnCRUDActivity__UnregisterObserver();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#setState() <em>Set State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set State</em>' operation.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#setState()
	 * @generated
	 */
	EOperation getObservableAnnCRUDActivity__SetState();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getState() <em>Get State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get State</em>' operation.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getState()
	 * @generated
	 */
	EOperation getObservableAnnCRUDActivity__GetState();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.MementoPattern <em>Memento Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memento Pattern</em>'.
	 * @see DesignPatternsLayerCIM.MementoPattern
	 * @generated
	 */
	EClass getMementoPattern();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerCIM.MementoPattern#getHasMemento <em>Has Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Memento</em>'.
	 * @see DesignPatternsLayerCIM.MementoPattern#getHasMemento()
	 * @see #getMementoPattern()
	 * @generated
	 */
	EReference getMementoPattern_HasMemento();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.BridgePattern <em>Bridge Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bridge Pattern</em>'.
	 * @see DesignPatternsLayerCIM.BridgePattern
	 * @generated
	 */
	EClass getBridgePattern();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerCIM.BridgePattern#getAssociatesAnnAlgoResource <em>Associates Ann Algo Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Associates Ann Algo Resource</em>'.
	 * @see DesignPatternsLayerCIM.BridgePattern#getAssociatesAnnAlgoResource()
	 * @see #getBridgePattern()
	 * @generated
	 */
	EReference getBridgePattern_AssociatesAnnAlgoResource();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>BMake Bridge Pattern For External Service</em>'.
	 * @see DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForExternalService()
	 * @see #getBridgePattern()
	 * @generated
	 */
	EAttribute getBridgePattern_BMakeBridgePatternForExternalService();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>BMake Bridge Pattern For Search</em>'.
	 * @see DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForSearch()
	 * @see #getBridgePattern()
	 * @generated
	 */
	EAttribute getBridgePattern_BMakeBridgePatternForSearch();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerCIM.Memento <em>Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Memento</em>'.
	 * @see DesignPatternsLayerCIM.Memento
	 * @generated
	 */
	EClass getMemento();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerCIM.Memento#getMementoNum <em>Memento Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Memento Num</em>'.
	 * @see DesignPatternsLayerCIM.Memento#getMementoNum()
	 * @see #getMemento()
	 * @generated
	 */
	EAttribute getMemento_MementoNum();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerCIM.Memento#getReferencesAnnResource <em>References Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Resource</em>'.
	 * @see DesignPatternsLayerCIM.Memento#getReferencesAnnResource()
	 * @see #getMemento()
	 * @generated
	 */
	EReference getMemento_ReferencesAnnResource();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DesignPatternsLayerCIMFactory getDesignPatternsLayerCIMFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnotationModelImpl <em>Annotation Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnotationModelImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotationModel()
		 * @generated
		 */
		EClass ANNOTATION_MODEL = eINSTANCE.getAnnotationModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION_MODEL__NAME = eINSTANCE.getAnnotationModel_Name();

		/**
		 * The meta object literal for the '<em><b>Has Annotated Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT = eINSTANCE.getAnnotationModel_HasAnnotatedElement();

		/**
		 * The meta object literal for the '<em><b>Has Annotation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_MODEL__HAS_ANNOTATION = eINSTANCE.getAnnotationModel_HasAnnotation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnotatedElementImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotatedElement()
		 * @generated
		 */
		EClass ANNOTATED_ELEMENT = eINSTANCE.getAnnotatedElement();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnotationImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnPropertyImpl <em>Ann Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnPropertyImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnProperty()
		 * @generated
		 */
		EClass ANN_PROPERTY = eINSTANCE.getAnnProperty();

		/**
		 * The meta object literal for the '<em><b>Annotates Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_PROPERTY__ANNOTATES_PROPERTY = eINSTANCE.getAnnProperty_AnnotatesProperty();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnResourceImpl <em>Ann Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnResourceImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnResource()
		 * @generated
		 */
		EClass ANN_RESOURCE = eINSTANCE.getAnnResource();

		/**
		 * The meta object literal for the '<em><b>Annotates Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_RESOURCE__ANNOTATES_RESOURCE = eINSTANCE.getAnnResource_AnnotatesResource();

		/**
		 * The meta object literal for the '<em><b>Has Notification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_RESOURCE__HAS_NOTIFICATION = eINSTANCE.getAnnResource_HasNotification();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnAlgoResourceImpl <em>Ann Algo Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnAlgoResourceImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnAlgoResource()
		 * @generated
		 */
		EClass ANN_ALGO_RESOURCE = eINSTANCE.getAnnAlgoResource();

		/**
		 * The meta object literal for the '<em><b>Annotates Algo Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_ALGO_RESOURCE__ANNOTATES_ALGO_RESOURCE = eINSTANCE.getAnnAlgoResource_AnnotatesAlgoResource();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.DesignPatternImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDesignPattern()
		 * @generated
		 */
		EClass DESIGN_PATTERN = eINSTANCE.getDesignPattern();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.DesignPatternModelImpl <em>Design Pattern Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.DesignPatternModelImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDesignPatternModel()
		 * @generated
		 */
		EClass DESIGN_PATTERN_MODEL = eINSTANCE.getDesignPatternModel();

		/**
		 * The meta object literal for the '<em><b>Has Design Pattern</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN = eINSTANCE.getDesignPatternModel_HasDesignPattern();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.BuilderPatternImpl <em>Builder Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.BuilderPatternImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBuilderPattern()
		 * @generated
		 */
		EClass BUILDER_PATTERN = eINSTANCE.getBuilderPattern();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Resource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDER_PATTERN__ASSOCIATES_ANN_RESOURCE = eINSTANCE.getBuilderPattern_AssociatesAnnResource();

		/**
		 * The meta object literal for the '<em><b>Has Director</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDER_PATTERN__HAS_DIRECTOR = eINSTANCE.getBuilderPattern_HasDirector();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.DirectorImpl <em>Director</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.DirectorImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getDirector()
		 * @generated
		 */
		EClass DIRECTOR = eINSTANCE.getDirector();

		/**
		 * The meta object literal for the '<em><b>Has Builder</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIRECTOR__HAS_BUILDER = eINSTANCE.getDirector_HasBuilder();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.BuilderImpl <em>Builder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.BuilderImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBuilder()
		 * @generated
		 */
		EClass BUILDER = eINSTANCE.getBuilder();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BUILDER__ASSOCIATES_ANN_RESOURCE = eINSTANCE.getBuilder_AssociatesAnnResource();

		/**
		 * The meta object literal for the '<em><b>Build Representation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BUILDER___BUILD_REPRESENTATION = eINSTANCE.getBuilder__BuildRepresentation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.ConcreteBuilderImpl <em>Concrete Builder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.ConcreteBuilderImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getConcreteBuilder()
		 * @generated
		 */
		EClass CONCRETE_BUILDER = eINSTANCE.getConcreteBuilder();

		/**
		 * The meta object literal for the '<em><b>Builds Representation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONCRETE_BUILDER__BUILDS_REPRESENTATION = eINSTANCE.getConcreteBuilder_BuildsRepresentation();

		/**
		 * The meta object literal for the '<em><b>Build Representation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CONCRETE_BUILDER___BUILD_REPRESENTATION = eINSTANCE.getConcreteBuilder__BuildRepresentation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.RepresentationImpl <em>Representation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.RepresentationImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getRepresentation()
		 * @generated
		 */
		EClass REPRESENTATION = eINSTANCE.getRepresentation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPRESENTATION__NAME = eINSTANCE.getRepresentation_Name();

		/**
		 * The meta object literal for the '<em><b>Has</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPRESENTATION__HAS = eINSTANCE.getRepresentation_Has();

		/**
		 * The meta object literal for the '<em><b>Refers To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPRESENTATION__REFERS_TO = eINSTANCE.getRepresentation_RefersTo();

		/**
		 * The meta object literal for the '<em><b>Properties List</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPRESENTATION__PROPERTIES_LIST = eINSTANCE.getRepresentation_PropertiesList();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.ObserverPatternImpl <em>Observer Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.ObserverPatternImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObserverPattern()
		 * @generated
		 */
		EClass OBSERVER_PATTERN = eINSTANCE.getObserverPattern();

		/**
		 * The meta object literal for the '<em><b>Has Observer</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVER_PATTERN__HAS_OBSERVER = eINSTANCE.getObserverPattern_HasObserver();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.ObserverImpl <em>Observer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.ObserverImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObserver()
		 * @generated
		 */
		EClass OBSERVER = eINSTANCE.getObserver();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVER__NAME = eINSTANCE.getObserver_Name();

		/**
		 * The meta object literal for the '<em><b>Observes</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVER__OBSERVES = eINSTANCE.getObserver_Observes();

		/**
		 * The meta object literal for the '<em><b>References Ann Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVER__REFERENCES_ANN_RESOURCE = eINSTANCE.getObserver_ReferencesAnnResource();

		/**
		 * The meta object literal for the '<em><b>Notification</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVER__NOTIFICATION = eINSTANCE.getObserver_Notification();

		/**
		 * The meta object literal for the '<em><b>Creates Notification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSERVER__CREATES_NOTIFICATION = eINSTANCE.getObserver_CreatesNotification();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVER___UPDATE = eINSTANCE.getObserver__Update();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.NotificationImpl <em>Notification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.NotificationImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getNotification()
		 * @generated
		 */
		EClass NOTIFICATION = eINSTANCE.getNotification();

		/**
		 * The meta object literal for the '<em><b>Notification Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTIFICATION__NOTIFICATION_ID = eINSTANCE.getNotification_NotificationId();

		/**
		 * The meta object literal for the '<em><b>CRUD Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NOTIFICATION__CRUD_EVENT = eINSTANCE.getNotification_CRUDEvent();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.AnnCRUDActivityImpl <em>Ann CRUD Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.AnnCRUDActivityImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getAnnCRUDActivity()
		 * @generated
		 */
		EClass ANN_CRUD_ACTIVITY = eINSTANCE.getAnnCRUDActivity();

		/**
		 * The meta object literal for the '<em><b>Annotates CRUD Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_CRUD_ACTIVITY__ANNOTATES_CRUD_ACTIVITY = eINSTANCE.getAnnCRUDActivity_AnnotatesCRUDActivity();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl <em>Observable Ann CRUD Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.ObservableAnnCRUDActivityImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getObservableAnnCRUDActivity()
		 * @generated
		 */
		EClass OBSERVABLE_ANN_CRUD_ACTIVITY = eINSTANCE.getObservableAnnCRUDActivity();

		/**
		 * The meta object literal for the '<em><b>References Ann Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVABLE_ANN_CRUD_ACTIVITY__REFERENCES_ANN_RESOURCE = eINSTANCE.getObservableAnnCRUDActivity_ReferencesAnnResource();

		/**
		 * The meta object literal for the '<em><b>Is Observedby</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVABLE_ANN_CRUD_ACTIVITY__IS_OBSERVEDBY = eINSTANCE.getObservableAnnCRUDActivity_IsObservedby();

		/**
		 * The meta object literal for the '<em><b>Extends Ann CRUD Activity</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSERVABLE_ANN_CRUD_ACTIVITY__EXTENDS_ANN_CRUD_ACTIVITY = eINSTANCE.getObservableAnnCRUDActivity_ExtendsAnnCRUDActivity();

		/**
		 * The meta object literal for the '<em><b>Notify Observers</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVABLE_ANN_CRUD_ACTIVITY___NOTIFY_OBSERVERS = eINSTANCE.getObservableAnnCRUDActivity__NotifyObservers();

		/**
		 * The meta object literal for the '<em><b>Register Observer</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVABLE_ANN_CRUD_ACTIVITY___REGISTER_OBSERVER = eINSTANCE.getObservableAnnCRUDActivity__RegisterObserver();

		/**
		 * The meta object literal for the '<em><b>Unregister Observer</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVABLE_ANN_CRUD_ACTIVITY___UNREGISTER_OBSERVER = eINSTANCE.getObservableAnnCRUDActivity__UnregisterObserver();

		/**
		 * The meta object literal for the '<em><b>Set State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVABLE_ANN_CRUD_ACTIVITY___SET_STATE = eINSTANCE.getObservableAnnCRUDActivity__SetState();

		/**
		 * The meta object literal for the '<em><b>Get State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation OBSERVABLE_ANN_CRUD_ACTIVITY___GET_STATE = eINSTANCE.getObservableAnnCRUDActivity__GetState();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.MementoPatternImpl <em>Memento Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.MementoPatternImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getMementoPattern()
		 * @generated
		 */
		EClass MEMENTO_PATTERN = eINSTANCE.getMementoPattern();

		/**
		 * The meta object literal for the '<em><b>Has Memento</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMENTO_PATTERN__HAS_MEMENTO = eINSTANCE.getMementoPattern_HasMemento();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.BridgePatternImpl <em>Bridge Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.BridgePatternImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getBridgePattern()
		 * @generated
		 */
		EClass BRIDGE_PATTERN = eINSTANCE.getBridgePattern();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Algo Resource</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BRIDGE_PATTERN__ASSOCIATES_ANN_ALGO_RESOURCE = eINSTANCE.getBridgePattern_AssociatesAnnAlgoResource();

		/**
		 * The meta object literal for the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE = eINSTANCE.getBridgePattern_BMakeBridgePatternForExternalService();

		/**
		 * The meta object literal for the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH = eINSTANCE.getBridgePattern_BMakeBridgePatternForSearch();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerCIM.impl.MementoImpl <em>Memento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerCIM.impl.MementoImpl
		 * @see DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMPackageImpl#getMemento()
		 * @generated
		 */
		EClass MEMENTO = eINSTANCE.getMemento();

		/**
		 * The meta object literal for the '<em><b>Memento Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMENTO__MEMENTO_NUM = eINSTANCE.getMemento_MementoNum();

		/**
		 * The meta object literal for the '<em><b>References Ann Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEMENTO__REFERENCES_ANN_RESOURCE = eINSTANCE.getMemento_ReferencesAnnResource();

	}

} //DesignPatternsLayerCIMPackage
