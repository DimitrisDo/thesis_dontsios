/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage
 * @generated
 */
public interface DesignPatternsLayerCIMFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DesignPatternsLayerCIMFactory eINSTANCE = DesignPatternsLayerCIM.impl.DesignPatternsLayerCIMFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Annotation Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation Model</em>'.
	 * @generated
	 */
	AnnotationModel createAnnotationModel();

	/**
	 * Returns a new object of class '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotated Element</em>'.
	 * @generated
	 */
	AnnotatedElement createAnnotatedElement();

	/**
	 * Returns a new object of class '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation</em>'.
	 * @generated
	 */
	Annotation createAnnotation();

	/**
	 * Returns a new object of class '<em>Ann Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Property</em>'.
	 * @generated
	 */
	AnnProperty createAnnProperty();

	/**
	 * Returns a new object of class '<em>Ann Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Resource</em>'.
	 * @generated
	 */
	AnnResource createAnnResource();

	/**
	 * Returns a new object of class '<em>Ann Algo Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Algo Resource</em>'.
	 * @generated
	 */
	AnnAlgoResource createAnnAlgoResource();

	/**
	 * Returns a new object of class '<em>Design Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Design Pattern</em>'.
	 * @generated
	 */
	DesignPattern createDesignPattern();

	/**
	 * Returns a new object of class '<em>Design Pattern Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Design Pattern Model</em>'.
	 * @generated
	 */
	DesignPatternModel createDesignPatternModel();

	/**
	 * Returns a new object of class '<em>Builder Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Builder Pattern</em>'.
	 * @generated
	 */
	BuilderPattern createBuilderPattern();

	/**
	 * Returns a new object of class '<em>Director</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Director</em>'.
	 * @generated
	 */
	Director createDirector();

	/**
	 * Returns a new object of class '<em>Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Builder</em>'.
	 * @generated
	 */
	Builder createBuilder();

	/**
	 * Returns a new object of class '<em>Concrete Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concrete Builder</em>'.
	 * @generated
	 */
	ConcreteBuilder createConcreteBuilder();

	/**
	 * Returns a new object of class '<em>Representation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Representation</em>'.
	 * @generated
	 */
	Representation createRepresentation();

	/**
	 * Returns a new object of class '<em>Observer Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Observer Pattern</em>'.
	 * @generated
	 */
	ObserverPattern createObserverPattern();

	/**
	 * Returns a new object of class '<em>Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Observer</em>'.
	 * @generated
	 */
	Observer createObserver();

	/**
	 * Returns a new object of class '<em>Notification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Notification</em>'.
	 * @generated
	 */
	Notification createNotification();

	/**
	 * Returns a new object of class '<em>Ann CRUD Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann CRUD Activity</em>'.
	 * @generated
	 */
	AnnCRUDActivity createAnnCRUDActivity();

	/**
	 * Returns a new object of class '<em>Observable Ann CRUD Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Observable Ann CRUD Activity</em>'.
	 * @generated
	 */
	ObservableAnnCRUDActivity createObservableAnnCRUDActivity();

	/**
	 * Returns a new object of class '<em>Memento Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memento Pattern</em>'.
	 * @generated
	 */
	MementoPattern createMementoPattern();

	/**
	 * Returns a new object of class '<em>Bridge Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bridge Pattern</em>'.
	 * @generated
	 */
	BridgePattern createBridgePattern();

	/**
	 * Returns a new object of class '<em>Memento</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Memento</em>'.
	 * @generated
	 */
	Memento createMemento();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DesignPatternsLayerCIMPackage getDesignPatternsLayerCIMPackage();

} //DesignPatternsLayerCIMFactory
