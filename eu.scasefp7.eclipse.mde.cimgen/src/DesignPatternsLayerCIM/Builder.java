/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Builder#getAssociatesAnnResource <em>Associates Ann Resource</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBuilder()
 * @model
 * @generated
 */
public interface Builder extends EObject {
	/**
	 * Returns the value of the '<em><b>Associates Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Resource</em>' reference.
	 * @see #setAssociatesAnnResource(AnnResource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBuilder_AssociatesAnnResource()
	 * @model required="true"
	 * @generated
	 */
	AnnResource getAssociatesAnnResource();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Builder#getAssociatesAnnResource <em>Associates Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associates Ann Resource</em>' reference.
	 * @see #getAssociatesAnnResource()
	 * @generated
	 */
	void setAssociatesAnnResource(AnnResource value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Representation buildRepresentation();

} // Builder
