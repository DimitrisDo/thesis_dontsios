/**
 */
package DesignPatternsLayerCIM;

import ServiceCIM.CRUDVerb;
import ServiceCIM.Resource;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observable Ann CRUD Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getIsObservedby <em>Is Observedby</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getExtendsAnnCRUDActivity <em>Extends Ann CRUD Activity</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObservableAnnCRUDActivity()
 * @model
 * @generated
 */
public interface ObservableAnnCRUDActivity extends Annotation {
	/**
	 * Returns the value of the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Resource</em>' reference.
	 * @see #setReferencesAnnResource(AnnResource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObservableAnnCRUDActivity_ReferencesAnnResource()
	 * @model required="true"
	 * @generated
	 */
	AnnResource getReferencesAnnResource();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getReferencesAnnResource <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Resource</em>' reference.
	 * @see #getReferencesAnnResource()
	 * @generated
	 */
	void setReferencesAnnResource(AnnResource value);

	/**
	 * Returns the value of the '<em><b>Is Observedby</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Observer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Observedby</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Observedby</em>' reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObservableAnnCRUDActivity_IsObservedby()
	 * @model
	 * @generated
	 */
	EList<Observer> getIsObservedby();

	/**
	 * Returns the value of the '<em><b>Extends Ann CRUD Activity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends Ann CRUD Activity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends Ann CRUD Activity</em>' reference.
	 * @see #setExtendsAnnCRUDActivity(AnnCRUDActivity)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObservableAnnCRUDActivity_ExtendsAnnCRUDActivity()
	 * @model required="true"
	 * @generated
	 */
	AnnCRUDActivity getExtendsAnnCRUDActivity();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity#getExtendsAnnCRUDActivity <em>Extends Ann CRUD Activity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extends Ann CRUD Activity</em>' reference.
	 * @see #getExtendsAnnCRUDActivity()
	 * @generated
	 */
	void setExtendsAnnCRUDActivity(AnnCRUDActivity value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	CRUDVerb notifyObservers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void registerObserver();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void unregisterObserver();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void setState();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	Resource getState();

} // ObservableAnnCRUDActivity
