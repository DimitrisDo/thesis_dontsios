/**
 */
package DesignPatternsLayerCIM;

import ServiceCIM.Resource;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ann Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.AnnResource#getAnnotatesResource <em>Annotates Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.AnnResource#getHasNotification <em>Has Notification</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getAnnResource()
 * @model
 * @generated
 */
public interface AnnResource extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Annotates Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotates Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotates Resource</em>' reference.
	 * @see #setAnnotatesResource(Resource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getAnnResource_AnnotatesResource()
	 * @model required="true"
	 * @generated
	 */
	Resource getAnnotatesResource();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.AnnResource#getAnnotatesResource <em>Annotates Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotates Resource</em>' reference.
	 * @see #getAnnotatesResource()
	 * @generated
	 */
	void setAnnotatesResource(Resource value);

	/**
	 * Returns the value of the '<em><b>Has Notification</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Notification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Notification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Notification</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getAnnResource_HasNotification()
	 * @model containment="true"
	 * @generated
	 */
	EList<Notification> getHasNotification();

} // AnnResource
