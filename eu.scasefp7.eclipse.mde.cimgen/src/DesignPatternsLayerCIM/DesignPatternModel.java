/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.DesignPatternModel#getHasDesignPattern <em>Has Design Pattern</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getDesignPatternModel()
 * @model
 * @generated
 */
public interface DesignPatternModel extends Annotation {
	/**
	 * Returns the value of the '<em><b>Has Design Pattern</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.DesignPattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Design Pattern</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Design Pattern</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getDesignPatternModel_HasDesignPattern()
	 * @model containment="true"
	 * @generated
	 */
	EList<DesignPattern> getHasDesignPattern();

} // DesignPatternModel
