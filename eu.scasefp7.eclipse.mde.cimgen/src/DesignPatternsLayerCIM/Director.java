/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Director</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Director#getHasBuilder <em>Has Builder</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getDirector()
 * @model
 * @generated
 */
public interface Director extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Builder</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Builder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Builder</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Builder</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getDirector_HasBuilder()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Builder> getHasBuilder();

} // Director
