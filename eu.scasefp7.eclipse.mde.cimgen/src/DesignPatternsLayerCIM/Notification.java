/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Notification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Notification#getNotificationId <em>Notification Id</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Notification#getCRUDEvent <em>CRUD Event</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getNotification()
 * @model
 * @generated
 */
public interface Notification extends EObject {
	/**
	 * Returns the value of the '<em><b>Notification Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notification Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notification Id</em>' attribute.
	 * @see #setNotificationId(int)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getNotification_NotificationId()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.Int"
	 * @generated
	 */
	int getNotificationId();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Notification#getNotificationId <em>Notification Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Notification Id</em>' attribute.
	 * @see #getNotificationId()
	 * @generated
	 */
	void setNotificationId(int value);

	/**
	 * Returns the value of the '<em><b>CRUD Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>CRUD Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>CRUD Event</em>' attribute.
	 * @see #setCRUDEvent(String)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getNotification_CRUDEvent()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getCRUDEvent();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Notification#getCRUDEvent <em>CRUD Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>CRUD Event</em>' attribute.
	 * @see #getCRUDEvent()
	 * @generated
	 */
	void setCRUDEvent(String value);

} // Notification
