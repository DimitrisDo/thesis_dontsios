/**
 */
package DesignPatternsLayerCIM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concrete Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.ConcreteBuilder#getBuildsRepresentation <em>Builds Representation</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getConcreteBuilder()
 * @model
 * @generated
 */
public interface ConcreteBuilder extends Builder {
	/**
	 * Returns the value of the '<em><b>Builds Representation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Builds Representation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Builds Representation</em>' containment reference.
	 * @see #setBuildsRepresentation(Representation)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getConcreteBuilder_BuildsRepresentation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Representation getBuildsRepresentation();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.ConcreteBuilder#getBuildsRepresentation <em>Builds Representation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Builds Representation</em>' containment reference.
	 * @see #getBuildsRepresentation()
	 * @generated
	 */
	void setBuildsRepresentation(Representation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	Representation buildRepresentation();

} // ConcreteBuilder
