/**
 */
package DesignPatternsLayerCIM.util;

import DesignPatternsLayerCIM.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage
 * @generated
 */
public class DesignPatternsLayerCIMAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DesignPatternsLayerCIMPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerCIMAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DesignPatternsLayerCIMPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternsLayerCIMSwitch<Adapter> modelSwitch =
		new DesignPatternsLayerCIMSwitch<Adapter>() {
			@Override
			public Adapter caseAnnotationModel(AnnotationModel object) {
				return createAnnotationModelAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseAnnotation(Annotation object) {
				return createAnnotationAdapter();
			}
			@Override
			public Adapter caseAnnProperty(AnnProperty object) {
				return createAnnPropertyAdapter();
			}
			@Override
			public Adapter caseAnnResource(AnnResource object) {
				return createAnnResourceAdapter();
			}
			@Override
			public Adapter caseAnnAlgoResource(AnnAlgoResource object) {
				return createAnnAlgoResourceAdapter();
			}
			@Override
			public Adapter caseDesignPattern(DesignPattern object) {
				return createDesignPatternAdapter();
			}
			@Override
			public Adapter caseDesignPatternModel(DesignPatternModel object) {
				return createDesignPatternModelAdapter();
			}
			@Override
			public Adapter caseBuilderPattern(BuilderPattern object) {
				return createBuilderPatternAdapter();
			}
			@Override
			public Adapter caseDirector(Director object) {
				return createDirectorAdapter();
			}
			@Override
			public Adapter caseBuilder(Builder object) {
				return createBuilderAdapter();
			}
			@Override
			public Adapter caseConcreteBuilder(ConcreteBuilder object) {
				return createConcreteBuilderAdapter();
			}
			@Override
			public Adapter caseRepresentation(Representation object) {
				return createRepresentationAdapter();
			}
			@Override
			public Adapter caseObserverPattern(ObserverPattern object) {
				return createObserverPatternAdapter();
			}
			@Override
			public Adapter caseObserver(Observer object) {
				return createObserverAdapter();
			}
			@Override
			public Adapter caseNotification(Notification object) {
				return createNotificationAdapter();
			}
			@Override
			public Adapter caseAnnCRUDActivity(AnnCRUDActivity object) {
				return createAnnCRUDActivityAdapter();
			}
			@Override
			public Adapter caseObservableAnnCRUDActivity(ObservableAnnCRUDActivity object) {
				return createObservableAnnCRUDActivityAdapter();
			}
			@Override
			public Adapter caseMementoPattern(MementoPattern object) {
				return createMementoPatternAdapter();
			}
			@Override
			public Adapter caseBridgePattern(BridgePattern object) {
				return createBridgePatternAdapter();
			}
			@Override
			public Adapter caseMemento(Memento object) {
				return createMementoAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnotationModel <em>Annotation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnotationModel
	 * @generated
	 */
	public Adapter createAnnotationModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Annotation
	 * @generated
	 */
	public Adapter createAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnProperty <em>Ann Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnProperty
	 * @generated
	 */
	public Adapter createAnnPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnResource <em>Ann Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnResource
	 * @generated
	 */
	public Adapter createAnnResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnAlgoResource <em>Ann Algo Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnAlgoResource
	 * @generated
	 */
	public Adapter createAnnAlgoResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.DesignPattern <em>Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.DesignPattern
	 * @generated
	 */
	public Adapter createDesignPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.DesignPatternModel <em>Design Pattern Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.DesignPatternModel
	 * @generated
	 */
	public Adapter createDesignPatternModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.BuilderPattern <em>Builder Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.BuilderPattern
	 * @generated
	 */
	public Adapter createBuilderPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Director <em>Director</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Director
	 * @generated
	 */
	public Adapter createDirectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Builder <em>Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Builder
	 * @generated
	 */
	public Adapter createBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.ConcreteBuilder <em>Concrete Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.ConcreteBuilder
	 * @generated
	 */
	public Adapter createConcreteBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Representation <em>Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Representation
	 * @generated
	 */
	public Adapter createRepresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.ObserverPattern <em>Observer Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.ObserverPattern
	 * @generated
	 */
	public Adapter createObserverPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Observer <em>Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Observer
	 * @generated
	 */
	public Adapter createObserverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Notification <em>Notification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Notification
	 * @generated
	 */
	public Adapter createNotificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.AnnCRUDActivity <em>Ann CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.AnnCRUDActivity
	 * @generated
	 */
	public Adapter createAnnCRUDActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.ObservableAnnCRUDActivity <em>Observable Ann CRUD Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.ObservableAnnCRUDActivity
	 * @generated
	 */
	public Adapter createObservableAnnCRUDActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.MementoPattern <em>Memento Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.MementoPattern
	 * @generated
	 */
	public Adapter createMementoPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.BridgePattern <em>Bridge Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.BridgePattern
	 * @generated
	 */
	public Adapter createBridgePatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerCIM.Memento <em>Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerCIM.Memento
	 * @generated
	 */
	public Adapter createMementoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DesignPatternsLayerCIMAdapterFactory
