/**
 */
package DesignPatternsLayerCIM.util;

import DesignPatternsLayerCIM.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage
 * @generated
 */
public class DesignPatternsLayerCIMSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DesignPatternsLayerCIMPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerCIMSwitch() {
		if (modelPackage == null) {
			modelPackage = DesignPatternsLayerCIMPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DesignPatternsLayerCIMPackage.ANNOTATION_MODEL: {
				AnnotationModel annotationModel = (AnnotationModel)theEObject;
				T result = caseAnnotationModel(annotationModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANNOTATED_ELEMENT: {
				AnnotatedElement annotatedElement = (AnnotatedElement)theEObject;
				T result = caseAnnotatedElement(annotatedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANN_PROPERTY: {
				AnnProperty annProperty = (AnnProperty)theEObject;
				T result = caseAnnProperty(annProperty);
				if (result == null) result = caseAnnotatedElement(annProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANN_RESOURCE: {
				AnnResource annResource = (AnnResource)theEObject;
				T result = caseAnnResource(annResource);
				if (result == null) result = caseAnnotatedElement(annResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANN_ALGO_RESOURCE: {
				AnnAlgoResource annAlgoResource = (AnnAlgoResource)theEObject;
				T result = caseAnnAlgoResource(annAlgoResource);
				if (result == null) result = caseAnnotatedElement(annAlgoResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN: {
				DesignPattern designPattern = (DesignPattern)theEObject;
				T result = caseDesignPattern(designPattern);
				if (result == null) result = caseAnnotation(designPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.DESIGN_PATTERN_MODEL: {
				DesignPatternModel designPatternModel = (DesignPatternModel)theEObject;
				T result = caseDesignPatternModel(designPatternModel);
				if (result == null) result = caseAnnotation(designPatternModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.BUILDER_PATTERN: {
				BuilderPattern builderPattern = (BuilderPattern)theEObject;
				T result = caseBuilderPattern(builderPattern);
				if (result == null) result = caseDesignPattern(builderPattern);
				if (result == null) result = caseAnnotation(builderPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.DIRECTOR: {
				Director director = (Director)theEObject;
				T result = caseDirector(director);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.BUILDER: {
				Builder builder = (Builder)theEObject;
				T result = caseBuilder(builder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.CONCRETE_BUILDER: {
				ConcreteBuilder concreteBuilder = (ConcreteBuilder)theEObject;
				T result = caseConcreteBuilder(concreteBuilder);
				if (result == null) result = caseBuilder(concreteBuilder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.REPRESENTATION: {
				Representation representation = (Representation)theEObject;
				T result = caseRepresentation(representation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.OBSERVER_PATTERN: {
				ObserverPattern observerPattern = (ObserverPattern)theEObject;
				T result = caseObserverPattern(observerPattern);
				if (result == null) result = caseDesignPattern(observerPattern);
				if (result == null) result = caseAnnotation(observerPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.OBSERVER: {
				Observer observer = (Observer)theEObject;
				T result = caseObserver(observer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.NOTIFICATION: {
				Notification notification = (Notification)theEObject;
				T result = caseNotification(notification);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.ANN_CRUD_ACTIVITY: {
				AnnCRUDActivity annCRUDActivity = (AnnCRUDActivity)theEObject;
				T result = caseAnnCRUDActivity(annCRUDActivity);
				if (result == null) result = caseAnnotatedElement(annCRUDActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.OBSERVABLE_ANN_CRUD_ACTIVITY: {
				ObservableAnnCRUDActivity observableAnnCRUDActivity = (ObservableAnnCRUDActivity)theEObject;
				T result = caseObservableAnnCRUDActivity(observableAnnCRUDActivity);
				if (result == null) result = caseAnnotation(observableAnnCRUDActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.MEMENTO_PATTERN: {
				MementoPattern mementoPattern = (MementoPattern)theEObject;
				T result = caseMementoPattern(mementoPattern);
				if (result == null) result = caseDesignPattern(mementoPattern);
				if (result == null) result = caseAnnotation(mementoPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.BRIDGE_PATTERN: {
				BridgePattern bridgePattern = (BridgePattern)theEObject;
				T result = caseBridgePattern(bridgePattern);
				if (result == null) result = caseDesignPattern(bridgePattern);
				if (result == null) result = caseAnnotation(bridgePattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerCIMPackage.MEMENTO: {
				Memento memento = (Memento)theEObject;
				T result = caseMemento(memento);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotationModel(AnnotationModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnProperty(AnnProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnResource(AnnResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Algo Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Algo Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnAlgoResource(AnnAlgoResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPattern(DesignPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPatternModel(DesignPatternModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Builder Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Builder Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBuilderPattern(BuilderPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Director</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Director</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDirector(Director object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Builder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBuilder(Builder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concrete Builder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concrete Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConcreteBuilder(ConcreteBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Representation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Representation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRepresentation(Representation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Observer Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Observer Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObserverPattern(ObserverPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Observer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Observer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObserver(Observer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Notification</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Notification</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNotification(Notification object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann CRUD Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann CRUD Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnCRUDActivity(AnnCRUDActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Observable Ann CRUD Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Observable Ann CRUD Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObservableAnnCRUDActivity(ObservableAnnCRUDActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memento Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memento Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMementoPattern(MementoPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bridge Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bridge Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBridgePattern(BridgePattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Memento</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Memento</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMemento(Memento object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DesignPatternsLayerCIMSwitch
