/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bridge Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.BridgePattern#getAssociatesAnnAlgoResource <em>Associates Ann Algo Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBridgePattern()
 * @model
 * @generated
 */
public interface BridgePattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Associates Ann Algo Resource</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.AnnAlgoResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Algo Resource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Algo Resource</em>' reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBridgePattern_AssociatesAnnAlgoResource()
	 * @model required="true"
	 * @generated
	 */
	EList<AnnAlgoResource> getAssociatesAnnAlgoResource();

	/**
	 * Returns the value of the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BMake Bridge Pattern For External Service</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BMake Bridge Pattern For External Service</em>' attribute.
	 * @see #setBMakeBridgePatternForExternalService(boolean)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBridgePattern_BMakeBridgePatternForExternalService()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBMakeBridgePatternForExternalService();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BMake Bridge Pattern For External Service</em>' attribute.
	 * @see #isBMakeBridgePatternForExternalService()
	 * @generated
	 */
	void setBMakeBridgePatternForExternalService(boolean value);

	/**
	 * Returns the value of the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BMake Bridge Pattern For Search</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BMake Bridge Pattern For Search</em>' attribute.
	 * @see #setBMakeBridgePatternForSearch(boolean)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBridgePattern_BMakeBridgePatternForSearch()
	 * @model default="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBMakeBridgePatternForSearch();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.BridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BMake Bridge Pattern For Search</em>' attribute.
	 * @see #isBMakeBridgePatternForSearch()
	 * @generated
	 */
	void setBMakeBridgePatternForSearch(boolean value);

} // BridgePattern
