/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Observer#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Observer#getObserves <em>Observes</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Observer#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Observer#getNotification <em>Notification</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Observer#isCreatesNotification <em>Creates Notification</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver()
 * @model
 * @generated
 */
public interface Observer extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver_Name()
	 * @model id="true" dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Observer#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Observes</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observes</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observes</em>' reference.
	 * @see #setObserves(ObservableAnnCRUDActivity)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver_Observes()
	 * @model required="true"
	 * @generated
	 */
	ObservableAnnCRUDActivity getObserves();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Observer#getObserves <em>Observes</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observes</em>' reference.
	 * @see #getObserves()
	 * @generated
	 */
	void setObserves(ObservableAnnCRUDActivity value);

	/**
	 * Returns the value of the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Resource</em>' reference.
	 * @see #setReferencesAnnResource(AnnResource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver_ReferencesAnnResource()
	 * @model required="true"
	 * @generated
	 */
	AnnResource getReferencesAnnResource();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Observer#getReferencesAnnResource <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Resource</em>' reference.
	 * @see #getReferencesAnnResource()
	 * @generated
	 */
	void setReferencesAnnResource(AnnResource value);

	/**
	 * Returns the value of the '<em><b>Notification</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Notification}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Notification</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Notification</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver_Notification()
	 * @model containment="true"
	 * @generated
	 */
	EList<Notification> getNotification();

	/**
	 * Returns the value of the '<em><b>Creates Notification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Creates Notification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Creates Notification</em>' attribute.
	 * @see #setCreatesNotification(boolean)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserver_CreatesNotification()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isCreatesNotification();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Observer#isCreatesNotification <em>Creates Notification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Creates Notification</em>' attribute.
	 * @see #isCreatesNotification()
	 * @generated
	 */
	void setCreatesNotification(boolean value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update();

} // Observer
