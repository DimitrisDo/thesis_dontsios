/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Representation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Representation#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Representation#getHas <em>Has</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Representation#getRefersTo <em>Refers To</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Representation#getPropertiesList <em>Properties List</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getRepresentation()
 * @model
 * @generated
 */
public interface Representation extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getRepresentation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Representation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Has</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.AnnProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getRepresentation_Has()
	 * @model
	 * @generated
	 */
	EList<AnnProperty> getHas();

	/**
	 * Returns the value of the '<em><b>Refers To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refers To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refers To</em>' reference.
	 * @see #setRefersTo(AnnResource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getRepresentation_RefersTo()
	 * @model required="true"
	 * @generated
	 */
	AnnResource getRefersTo();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Representation#getRefersTo <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refers To</em>' reference.
	 * @see #getRefersTo()
	 * @generated
	 */
	void setRefersTo(AnnResource value);

	/**
	 * Returns the value of the '<em><b>Properties List</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.common.util.EList}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Properties List</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties List</em>' attribute list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getRepresentation_PropertiesList()
	 * @model transient="true"
	 * @generated
	 */
	EList<EList> getPropertiesList();

} // Representation
