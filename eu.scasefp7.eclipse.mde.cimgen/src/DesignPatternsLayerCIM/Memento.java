/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Memento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.Memento#getMementoNum <em>Memento Num</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.Memento#getReferencesAnnResource <em>References Ann Resource</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getMemento()
 * @model
 * @generated
 */
public interface Memento extends EObject {
	/**
	 * Returns the value of the '<em><b>Memento Num</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memento Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memento Num</em>' attribute.
	 * @see #setMementoNum(int)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getMemento_MementoNum()
	 * @model default="1" unique="false" dataType="org.eclipse.emf.ecore.xml.type.Int" ordered="false"
	 * @generated
	 */
	int getMementoNum();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Memento#getMementoNum <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memento Num</em>' attribute.
	 * @see #getMementoNum()
	 * @generated
	 */
	void setMementoNum(int value);

	/**
	 * Returns the value of the '<em><b>References Ann Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Resource</em>' reference.
	 * @see #setReferencesAnnResource(AnnResource)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getMemento_ReferencesAnnResource()
	 * @model required="true"
	 * @generated
	 */
	AnnResource getReferencesAnnResource();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.Memento#getReferencesAnnResource <em>References Ann Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Resource</em>' reference.
	 * @see #getReferencesAnnResource()
	 * @generated
	 */
	void setReferencesAnnResource(AnnResource value);

} // Memento
