/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Builder Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.BuilderPattern#getAssociatesAnnResource <em>Associates Ann Resource</em>}</li>
 *   <li>{@link DesignPatternsLayerCIM.BuilderPattern#getHasDirector <em>Has Director</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBuilderPattern()
 * @model
 * @generated
 */
public interface BuilderPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Associates Ann Resource</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.AnnResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Resource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Resource</em>' reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBuilderPattern_AssociatesAnnResource()
	 * @model required="true"
	 * @generated
	 */
	EList<AnnResource> getAssociatesAnnResource();

	/**
	 * Returns the value of the '<em><b>Has Director</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Director</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Director</em>' containment reference.
	 * @see #setHasDirector(Director)
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getBuilderPattern_HasDirector()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Director getHasDirector();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerCIM.BuilderPattern#getHasDirector <em>Has Director</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Director</em>' containment reference.
	 * @see #getHasDirector()
	 * @generated
	 */
	void setHasDirector(Director value);

} // BuilderPattern
