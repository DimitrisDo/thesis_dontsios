/**
 */
package DesignPatternsLayerCIM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Observer Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerCIM.ObserverPattern#getHasObserver <em>Has Observer</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserverPattern()
 * @model
 * @generated
 */
public interface ObserverPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Has Observer</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerCIM.Observer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Observer</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Observer</em>' containment reference list.
	 * @see DesignPatternsLayerCIM.DesignPatternsLayerCIMPackage#getObserverPattern_HasObserver()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Observer> getHasObserver();

} // ObserverPattern
