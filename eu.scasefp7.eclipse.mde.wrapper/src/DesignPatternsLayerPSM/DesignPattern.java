/**
 */
package DesignPatternsLayerPSM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getDesignPattern()
 * @model
 * @generated
 */
public interface DesignPattern extends Annotation {
} // DesignPattern
