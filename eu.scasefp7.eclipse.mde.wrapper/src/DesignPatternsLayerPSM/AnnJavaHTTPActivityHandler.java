/**
 */
package DesignPatternsLayerPSM;

import RESTfulServicePSM.HTTPActivityHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ann Java HTTP Activity Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler#getAnnotatesHTTPActivityHandler <em>Annotates HTTP Activity Handler</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnJavaHTTPActivityHandler()
 * @model
 * @generated
 */
public interface AnnJavaHTTPActivityHandler extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Annotates HTTP Activity Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotates HTTP Activity Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotates HTTP Activity Handler</em>' reference.
	 * @see #setAnnotatesHTTPActivityHandler(HTTPActivityHandler)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnJavaHTTPActivityHandler_AnnotatesHTTPActivityHandler()
	 * @model
	 * @generated
	 */
	HTTPActivityHandler getAnnotatesHTTPActivityHandler();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler#getAnnotatesHTTPActivityHandler <em>Annotates HTTP Activity Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotates HTTP Activity Handler</em>' reference.
	 * @see #getAnnotatesHTTPActivityHandler()
	 * @generated
	 */
	void setAnnotatesHTTPActivityHandler(HTTPActivityHandler value);

} // AnnJavaHTTPActivityHandler
