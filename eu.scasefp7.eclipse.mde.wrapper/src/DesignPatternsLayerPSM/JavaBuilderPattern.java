/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Builder Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaBuilderPattern#getHasJavaDirector <em>Has Java Director</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaBuilderPattern#getAssociatesAnnJavaResourceModels <em>Associates Ann Java Resource Models</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBuilderPattern()
 * @model
 * @generated
 */
public interface JavaBuilderPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Has Java Director</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Java Director</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Java Director</em>' containment reference.
	 * @see #setHasJavaDirector(JavaDirector)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBuilderPattern_HasJavaDirector()
	 * @model containment="true" required="true"
	 * @generated
	 */
	JavaDirector getHasJavaDirector();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaBuilderPattern#getHasJavaDirector <em>Has Java Director</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Java Director</em>' containment reference.
	 * @see #getHasJavaDirector()
	 * @generated
	 */
	void setHasJavaDirector(JavaDirector value);

	/**
	 * Returns the value of the '<em><b>Associates Ann Java Resource Models</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.AnnJavaResourceModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Java Resource Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Java Resource Models</em>' reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBuilderPattern_AssociatesAnnJavaResourceModels()
	 * @model required="true"
	 * @generated
	 */
	EList<AnnJavaResourceModel> getAssociatesAnnJavaResourceModels();

} // JavaBuilderPattern
