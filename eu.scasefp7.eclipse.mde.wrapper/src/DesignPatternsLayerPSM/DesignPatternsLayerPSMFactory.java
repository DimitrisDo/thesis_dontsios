/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage
 * @generated
 */
public interface DesignPatternsLayerPSMFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DesignPatternsLayerPSMFactory eINSTANCE = DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Annotation Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation Model</em>'.
	 * @generated
	 */
	AnnotationModel createAnnotationModel();

	/**
	 * Returns a new object of class '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotated Element</em>'.
	 * @generated
	 */
	AnnotatedElement createAnnotatedElement();

	/**
	 * Returns a new object of class '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation</em>'.
	 * @generated
	 */
	Annotation createAnnotation();

	/**
	 * Returns a new object of class '<em>Ann PSM Component Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann PSM Component Property</em>'.
	 * @generated
	 */
	AnnPSMComponentProperty createAnnPSMComponentProperty();

	/**
	 * Returns a new object of class '<em>Ann Java Resource Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Java Resource Model</em>'.
	 * @generated
	 */
	AnnJavaResourceModel createAnnJavaResourceModel();

	/**
	 * Returns a new object of class '<em>Ann Java Algo Resource Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Java Algo Resource Model</em>'.
	 * @generated
	 */
	AnnJavaAlgoResourceModel createAnnJavaAlgoResourceModel();
	
	/**
	 * Returns a new object of class '<em>Design Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Design Pattern</em>'.
	 * @generated
	 */
	DesignPattern createDesignPattern();
	
	/**
	 * Returns a new object of class '<em>Java Bridge Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Bridge Pattern</em>'.
	 * @generated
	 */
	JavaBridgePattern createJavaBridgePattern();

	/**
	 * Returns a new object of class '<em>Java Builder Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Builder Pattern</em>'.
	 * @generated
	 */
	JavaBuilderPattern createJavaBuilderPattern();

	/**
	 * Returns a new object of class '<em>Java Director</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Director</em>'.
	 * @generated
	 */
	JavaDirector createJavaDirector();

	/**
	 * Returns a new object of class '<em>Java Concrete Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Concrete Builder</em>'.
	 * @generated
	 */
	JavaConcreteBuilder createJavaConcreteBuilder();

	/**
	 * Returns a new object of class '<em>Java Builder</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Builder</em>'.
	 * @generated
	 */
	JavaBuilder createJavaBuilder();

	/**
	 * Returns a new object of class '<em>Java Representation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Representation</em>'.
	 * @generated
	 */
	JavaRepresentation createJavaRepresentation();

	/**
	 * Returns a new object of class '<em>Design Pattern Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Design Pattern Model</em>'.
	 * @generated
	 */
	DesignPatternModel createDesignPatternModel();
	

	/**
	 * Returns a new object of class '<em>Java Observer Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Observer Pattern</em>'.
	 * @generated
	 */
	JavaObserverPattern createJavaObserverPattern();

	/**
	 * Returns a new object of class '<em>Java Observer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Observer</em>'.
	 * @generated
	 */
	JavaObserver createJavaObserver();

	/**
	 * Returns a new object of class '<em>Java Observable Ann HTTP Activity Handler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Observable Ann HTTP Activity Handler</em>'.
	 * @generated
	 */
	JavaObservableAnnHTTPActivityHandler createJavaObservableAnnHTTPActivityHandler();

	/**
	 * Returns a new object of class '<em>Ann Java HTTP Activity Handler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ann Java HTTP Activity Handler</em>'.
	 * @generated
	 */
	AnnJavaHTTPActivityHandler createAnnJavaHTTPActivityHandler();

	/**
	 * Returns a new object of class '<em>Java Memento Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Memento Pattern</em>'.
	 * @generated
	 */
	JavaMementoPattern createJavaMementoPattern();

	/**
	 * Returns a new object of class '<em>Java Resource Model Memento</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Java Resource Model Memento</em>'.
	 * @generated
	 */
	JavaResourceModelMemento createJavaResourceModelMemento();
	

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DesignPatternsLayerPSMPackage getDesignPatternsLayerPSMPackage();

} //DesignPatternsLayerPSMFactory
