/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaObserver;
import DesignPatternsLayerPSM.JavaObserverPattern;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Observer Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObserverPatternImpl#getHasJavaObserver <em>Has Java Observer</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaObserverPatternImpl extends DesignPatternImpl implements JavaObserverPattern {
	/**
	 * The cached value of the '{@link #getHasJavaObserver() <em>Has Java Observer</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasJavaObserver()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaObserver> hasJavaObserver;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaObserverPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_OBSERVER_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaObserver> getHasJavaObserver() {
		if (hasJavaObserver == null) {
			hasJavaObserver = new EObjectContainmentEList<JavaObserver>(JavaObserver.class, this, DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER);
		}
		return hasJavaObserver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER:
				return ((InternalEList<?>)getHasJavaObserver()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER:
				return getHasJavaObserver();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER:
				getHasJavaObserver().clear();
				getHasJavaObserver().addAll((Collection<? extends JavaObserver>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER:
				getHasJavaObserver().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER:
				return hasJavaObserver != null && !hasJavaObserver.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaObserverPatternImpl
