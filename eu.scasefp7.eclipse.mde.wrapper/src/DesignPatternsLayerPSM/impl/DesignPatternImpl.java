/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.DesignPattern;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Design Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DesignPatternImpl extends AnnotationImpl implements DesignPattern {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.DESIGN_PATTERN;
	}

} //DesignPatternImpl
