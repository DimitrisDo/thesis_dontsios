/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaResourceModelMemento;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Resource Model Memento</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl#getMementoNum <em>Memento Num</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaResourceModelMementoImpl extends MinimalEObjectImpl.Container implements JavaResourceModelMemento {
	/**
	 * The default value of the '{@link #getMementoNum() <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMementoNum()
	 * @generated
	 * @ordered
	 */
	protected static final int MEMENTO_NUM_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getMementoNum() <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMementoNum()
	 * @generated
	 * @ordered
	 */
	protected int mementoNum = MEMENTO_NUM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferencesAnnJavaResourceModel() <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaResourceModel referencesAnnJavaResourceModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaResourceModelMementoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_RESOURCE_MODEL_MEMENTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMementoNum() {
		return mementoNum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMementoNum(int newMementoNum) {
		int oldMementoNum = mementoNum;
		mementoNum = newMementoNum;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM, oldMementoNum, mementoNum));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel getReferencesAnnJavaResourceModel() {
		if (referencesAnnJavaResourceModel != null && referencesAnnJavaResourceModel.eIsProxy()) {
			InternalEObject oldReferencesAnnJavaResourceModel = (InternalEObject)referencesAnnJavaResourceModel;
			referencesAnnJavaResourceModel = (AnnJavaResourceModel)eResolveProxy(oldReferencesAnnJavaResourceModel);
			if (referencesAnnJavaResourceModel != oldReferencesAnnJavaResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
			}
		}
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel basicGetReferencesAnnJavaResourceModel() {
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnJavaResourceModel(AnnJavaResourceModel newReferencesAnnJavaResourceModel) {
		AnnJavaResourceModel oldReferencesAnnJavaResourceModel = referencesAnnJavaResourceModel;
		referencesAnnJavaResourceModel = newReferencesAnnJavaResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM:
				return getMementoNum();
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				if (resolve) return getReferencesAnnJavaResourceModel();
				return basicGetReferencesAnnJavaResourceModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM:
				setMementoNum((Integer)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM:
				setMementoNum(MEMENTO_NUM_EDEFAULT);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM:
				return mementoNum != MEMENTO_NUM_EDEFAULT;
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				return referencesAnnJavaResourceModel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mementoNum: ");
		result.append(mementoNum);
		result.append(')');
		return result.toString();
	}

} //JavaResourceModelMementoImpl
