/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaConcreteBuilder;
import DesignPatternsLayerPSM.JavaRepresentation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Concrete Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaConcreteBuilderImpl#getBuildsJavaRepresentation <em>Builds Java Representation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaConcreteBuilderImpl extends JavaBuilderImpl implements JavaConcreteBuilder {
	/**
	 * The cached value of the '{@link #getBuildsJavaRepresentation() <em>Builds Java Representation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBuildsJavaRepresentation()
	 * @generated
	 * @ordered
	 */
	protected JavaRepresentation buildsJavaRepresentation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaConcreteBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_CONCRETE_BUILDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaRepresentation getBuildsJavaRepresentation() {
		return buildsJavaRepresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBuildsJavaRepresentation(JavaRepresentation newBuildsJavaRepresentation, NotificationChain msgs) {
		JavaRepresentation oldBuildsJavaRepresentation = buildsJavaRepresentation;
		buildsJavaRepresentation = newBuildsJavaRepresentation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION, oldBuildsJavaRepresentation, newBuildsJavaRepresentation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuildsJavaRepresentation(JavaRepresentation newBuildsJavaRepresentation) {
		if (newBuildsJavaRepresentation != buildsJavaRepresentation) {
			NotificationChain msgs = null;
			if (buildsJavaRepresentation != null)
				msgs = ((InternalEObject)buildsJavaRepresentation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION, null, msgs);
			if (newBuildsJavaRepresentation != null)
				msgs = ((InternalEObject)newBuildsJavaRepresentation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION, null, msgs);
			msgs = basicSetBuildsJavaRepresentation(newBuildsJavaRepresentation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION, newBuildsJavaRepresentation, newBuildsJavaRepresentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION:
				return basicSetBuildsJavaRepresentation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION:
				return getBuildsJavaRepresentation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION:
				setBuildsJavaRepresentation((JavaRepresentation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION:
				setBuildsJavaRepresentation((JavaRepresentation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION:
				return buildsJavaRepresentation != null;
		}
		return super.eIsSet(featureID);
	}

} //JavaConcreteBuilderImpl
