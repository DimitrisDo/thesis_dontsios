/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.AnnPSMComponentProperty;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaRepresentation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Representation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl#getRefersTo <em>Refers To</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl#getHas <em>Has</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl#getResourceInstanceId <em>Resource Instance Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaRepresentationImpl extends MinimalEObjectImpl.Container implements JavaRepresentation {
	/**
	 * The cached value of the '{@link #getRefersTo() <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefersTo()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaResourceModel refersTo;

	/**
	 * The cached value of the '{@link #getHas() <em>Has</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHas()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnPSMComponentProperty> has;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getResourceInstanceId() <em>Resource Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceInstanceId()
	 * @generated
	 * @ordered
	 */
	protected static final String RESOURCE_INSTANCE_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResourceInstanceId() <em>Resource Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceInstanceId()
	 * @generated
	 * @ordered
	 */
	protected String resourceInstanceId = RESOURCE_INSTANCE_ID_EDEFAULT;

	/**
	 * This is true if the Resource Instance Id attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resourceInstanceIdESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaRepresentationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_REPRESENTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel getRefersTo() {
		if (refersTo != null && refersTo.eIsProxy()) {
			InternalEObject oldRefersTo = (InternalEObject)refersTo;
			refersTo = (AnnJavaResourceModel)eResolveProxy(oldRefersTo);
			if (refersTo != oldRefersTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO, oldRefersTo, refersTo));
			}
		}
		return refersTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel basicGetRefersTo() {
		return refersTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefersTo(AnnJavaResourceModel newRefersTo) {
		AnnJavaResourceModel oldRefersTo = refersTo;
		refersTo = newRefersTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO, oldRefersTo, refersTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnPSMComponentProperty> getHas() {
		if (has == null) {
			has = new EObjectResolvingEList<AnnPSMComponentProperty>(AnnPSMComponentProperty.class, this, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__HAS);
		}
		return has;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResourceInstanceId() {
		return resourceInstanceId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceInstanceId(String newResourceInstanceId) {
		String oldResourceInstanceId = resourceInstanceId;
		resourceInstanceId = newResourceInstanceId;
		boolean oldResourceInstanceIdESet = resourceInstanceIdESet;
		resourceInstanceIdESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID, oldResourceInstanceId, resourceInstanceId, !oldResourceInstanceIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResourceInstanceId() {
		String oldResourceInstanceId = resourceInstanceId;
		boolean oldResourceInstanceIdESet = resourceInstanceIdESet;
		resourceInstanceId = RESOURCE_INSTANCE_ID_EDEFAULT;
		resourceInstanceIdESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID, oldResourceInstanceId, RESOURCE_INSTANCE_ID_EDEFAULT, oldResourceInstanceIdESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResourceInstanceId() {
		return resourceInstanceIdESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO:
				if (resolve) return getRefersTo();
				return basicGetRefersTo();
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__HAS:
				return getHas();
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__NAME:
				return getName();
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID:
				return getResourceInstanceId();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO:
				setRefersTo((AnnJavaResourceModel)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__HAS:
				getHas().clear();
				getHas().addAll((Collection<? extends AnnPSMComponentProperty>)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__NAME:
				setName((String)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID:
				setResourceInstanceId((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO:
				setRefersTo((AnnJavaResourceModel)null);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__HAS:
				getHas().clear();
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID:
				unsetResourceInstanceId();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__REFERS_TO:
				return refersTo != null;
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__HAS:
				return has != null && !has.isEmpty();
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID:
				return isSetResourceInstanceId();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", resourceInstanceId: ");
		if (resourceInstanceIdESet) result.append(resourceInstanceId); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //JavaRepresentationImpl
