/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaAlgoResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;

import RESTfulServicePSM.JavaAlgoResourceModel;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ann Java Algo Resource Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.AnnJavaAlgoResourceModelImpl#getAnnotatesJavaAlgoResourceModel <em>Annotates Java Algo Resource Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AnnJavaAlgoResourceModelImpl extends AnnotatedElementImpl implements AnnJavaAlgoResourceModel {
	/**
	 * The cached value of the '{@link #getAnnotatesJavaAlgoResourceModel() <em>Annotates Java Algo Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatesJavaAlgoResourceModel()
	 * @generated
	 * @ordered
	 */
	protected JavaAlgoResourceModel annotatesJavaAlgoResourceModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AnnJavaAlgoResourceModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.ANN_JAVA_ALGO_RESOURCE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaAlgoResourceModel getAnnotatesJavaAlgoResourceModel() {
		if (annotatesJavaAlgoResourceModel != null && annotatesJavaAlgoResourceModel.eIsProxy()) {
			InternalEObject oldAnnotatesJavaAlgoResourceModel = (InternalEObject)annotatesJavaAlgoResourceModel;
			annotatesJavaAlgoResourceModel = (JavaAlgoResourceModel)eResolveProxy(oldAnnotatesJavaAlgoResourceModel);
			if (annotatesJavaAlgoResourceModel != oldAnnotatesJavaAlgoResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL, oldAnnotatesJavaAlgoResourceModel, annotatesJavaAlgoResourceModel));
			}
		}
		return annotatesJavaAlgoResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaAlgoResourceModel basicGetAnnotatesJavaAlgoResourceModel() {
		return annotatesJavaAlgoResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAnnotatesJavaAlgoResourceModel(JavaAlgoResourceModel newAnnotatesJavaAlgoResourceModel) {
		JavaAlgoResourceModel oldAnnotatesJavaAlgoResourceModel = annotatesJavaAlgoResourceModel;
		annotatesJavaAlgoResourceModel = newAnnotatesJavaAlgoResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL, oldAnnotatesJavaAlgoResourceModel, annotatesJavaAlgoResourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL:
				if (resolve) return getAnnotatesJavaAlgoResourceModel();
				return basicGetAnnotatesJavaAlgoResourceModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL:
				setAnnotatesJavaAlgoResourceModel((JavaAlgoResourceModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL:
				setAnnotatesJavaAlgoResourceModel((JavaAlgoResourceModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL:
				return annotatesJavaAlgoResourceModel != null;
		}
		return super.eIsSet(featureID);
	}

} //AnnJavaAlgoResourceModelImpl
