/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaMementoPattern;
import DesignPatternsLayerPSM.JavaResourceModelMemento;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Memento Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaMementoPatternImpl#getHasJavaResourceModelMemento <em>Has Java Resource Model Memento</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaMementoPatternImpl extends DesignPatternImpl implements JavaMementoPattern {
	/**
	 * The cached value of the '{@link #getHasJavaResourceModelMemento() <em>Has Java Resource Model Memento</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasJavaResourceModelMemento()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaResourceModelMemento> hasJavaResourceModelMemento;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaMementoPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_MEMENTO_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaResourceModelMemento> getHasJavaResourceModelMemento() {
		if (hasJavaResourceModelMemento == null) {
			hasJavaResourceModelMemento = new EObjectContainmentEList<JavaResourceModelMemento>(JavaResourceModelMemento.class, this, DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO);
		}
		return hasJavaResourceModelMemento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO:
				return ((InternalEList<?>)getHasJavaResourceModelMemento()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO:
				return getHasJavaResourceModelMemento();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO:
				getHasJavaResourceModelMemento().clear();
				getHasJavaResourceModelMemento().addAll((Collection<? extends JavaResourceModelMemento>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO:
				getHasJavaResourceModelMemento().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO:
				return hasJavaResourceModelMemento != null && !hasJavaResourceModelMemento.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaMementoPatternImpl
