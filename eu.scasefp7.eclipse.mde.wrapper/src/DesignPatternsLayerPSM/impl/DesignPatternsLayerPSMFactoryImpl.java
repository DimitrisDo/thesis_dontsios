/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsLayerPSMFactoryImpl extends EFactoryImpl implements DesignPatternsLayerPSMFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DesignPatternsLayerPSMFactory init() {
		try {
			DesignPatternsLayerPSMFactory theDesignPatternsLayerPSMFactory = (DesignPatternsLayerPSMFactory)EPackage.Registry.INSTANCE.getEFactory(DesignPatternsLayerPSMPackage.eNS_URI);
			if (theDesignPatternsLayerPSMFactory != null) {
				return theDesignPatternsLayerPSMFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DesignPatternsLayerPSMFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerPSMFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DesignPatternsLayerPSMPackage.ANNOTATION_MODEL: return createAnnotationModel();
			case DesignPatternsLayerPSMPackage.ANNOTATED_ELEMENT: return createAnnotatedElement();
			case DesignPatternsLayerPSMPackage.ANNOTATION: return createAnnotation();
			case DesignPatternsLayerPSMPackage.ANN_PSM_COMPONENT_PROPERTY: return createAnnPSMComponentProperty();
			case DesignPatternsLayerPSMPackage.ANN_JAVA_RESOURCE_MODEL: return createAnnJavaResourceModel();
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL: return createAnnJavaAlgoResourceModel();
			case DesignPatternsLayerPSMPackage.DESIGN_PATTERN: return createDesignPattern();
			case DesignPatternsLayerPSMPackage.JAVA_BRIDGE_PATTERN: return createJavaBridgePattern();
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN: return createJavaBuilderPattern();
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR: return createJavaDirector();
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER: return createJavaConcreteBuilder();
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER: return createJavaBuilder();
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION: return createJavaRepresentation();
			case DesignPatternsLayerPSMPackage.DESIGN_PATTERN_MODEL: return createDesignPatternModel();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN: return createJavaObserverPattern();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER: return createJavaObserver();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER: return createJavaObservableAnnHTTPActivityHandler();
			case DesignPatternsLayerPSMPackage.ANN_JAVA_HTTP_ACTIVITY_HANDLER: return createAnnJavaHTTPActivityHandler();
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN: return createJavaMementoPattern();
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO: return createJavaResourceModelMemento();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationModel createAnnotationModel() {
		AnnotationModelImpl annotationModel = new AnnotationModelImpl();
		return annotationModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotatedElement createAnnotatedElement() {
		AnnotatedElementImpl annotatedElement = new AnnotatedElementImpl();
		return annotatedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnPSMComponentProperty createAnnPSMComponentProperty() {
		AnnPSMComponentPropertyImpl annPSMComponentProperty = new AnnPSMComponentPropertyImpl();
		return annPSMComponentProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel createAnnJavaResourceModel() {
		AnnJavaResourceModelImpl annJavaResourceModel = new AnnJavaResourceModelImpl();
		return annJavaResourceModel;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaAlgoResourceModel createAnnJavaAlgoResourceModel() {
		AnnJavaAlgoResourceModelImpl annJavaAlgoResourceModel = new AnnJavaAlgoResourceModelImpl();
		return annJavaAlgoResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPattern createDesignPattern() {
		DesignPatternImpl designPattern = new DesignPatternImpl();
		return designPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaBridgePattern createJavaBridgePattern() {
		JavaBridgePatternImpl javaBridgePattern = new JavaBridgePatternImpl();
		return javaBridgePattern;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaBuilderPattern createJavaBuilderPattern() {
		JavaBuilderPatternImpl javaBuilderPattern = new JavaBuilderPatternImpl();
		return javaBuilderPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaDirector createJavaDirector() {
		JavaDirectorImpl javaDirector = new JavaDirectorImpl();
		return javaDirector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaConcreteBuilder createJavaConcreteBuilder() {
		JavaConcreteBuilderImpl javaConcreteBuilder = new JavaConcreteBuilderImpl();
		return javaConcreteBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaBuilder createJavaBuilder() {
		JavaBuilderImpl javaBuilder = new JavaBuilderImpl();
		return javaBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaRepresentation createJavaRepresentation() {
		JavaRepresentationImpl javaRepresentation = new JavaRepresentationImpl();
		return javaRepresentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternModel createDesignPatternModel() {
		DesignPatternModelImpl designPatternModel = new DesignPatternModelImpl();
		return designPatternModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	 public JavaObserverPattern createJavaObserverPattern() {
		JavaObserverPatternImpl javaObserverPattern = new JavaObserverPatternImpl();
		return javaObserverPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaObserver createJavaObserver() {
		JavaObserverImpl javaObserver = new JavaObserverImpl();
		return javaObserver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaObservableAnnHTTPActivityHandler createJavaObservableAnnHTTPActivityHandler() {
		JavaObservableAnnHTTPActivityHandlerImpl javaObservableAnnHTTPActivityHandler = new JavaObservableAnnHTTPActivityHandlerImpl();
		return javaObservableAnnHTTPActivityHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaHTTPActivityHandler createAnnJavaHTTPActivityHandler() {
		AnnJavaHTTPActivityHandlerImpl annJavaHTTPActivityHandler = new AnnJavaHTTPActivityHandlerImpl();
		return annJavaHTTPActivityHandler;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaMementoPattern createJavaMementoPattern() {
		JavaMementoPatternImpl javaMementoPattern = new JavaMementoPatternImpl();
		return javaMementoPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaResourceModelMemento createJavaResourceModelMemento() {
		JavaResourceModelMementoImpl javaResourceModelMemento = new JavaResourceModelMementoImpl();
		return javaResourceModelMemento;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerPSMPackage getDesignPatternsLayerPSMPackage() {
		return (DesignPatternsLayerPSMPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DesignPatternsLayerPSMPackage getPackage() {
		return DesignPatternsLayerPSMPackage.eINSTANCE;
	}

} //DesignPatternsLayerPSMFactoryImpl
