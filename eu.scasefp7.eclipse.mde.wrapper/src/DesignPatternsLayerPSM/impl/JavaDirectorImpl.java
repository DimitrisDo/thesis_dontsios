/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaBuilder;
import DesignPatternsLayerPSM.JavaDirector;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Director</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaDirectorImpl#getHasJavaBuilder <em>Has Java Builder</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaDirectorImpl extends MinimalEObjectImpl.Container implements JavaDirector {
	/**
	 * The cached value of the '{@link #getHasJavaBuilder() <em>Has Java Builder</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasJavaBuilder()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaBuilder> hasJavaBuilder;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaDirectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_DIRECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaBuilder> getHasJavaBuilder() {
		if (hasJavaBuilder == null) {
			hasJavaBuilder = new EObjectContainmentEList<JavaBuilder>(JavaBuilder.class, this, DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER);
		}
		return hasJavaBuilder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER:
				return ((InternalEList<?>)getHasJavaBuilder()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER:
				return getHasJavaBuilder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER:
				getHasJavaBuilder().clear();
				getHasJavaBuilder().addAll((Collection<? extends JavaBuilder>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER:
				getHasJavaBuilder().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR__HAS_JAVA_BUILDER:
				return hasJavaBuilder != null && !hasJavaBuilder.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaDirectorImpl
