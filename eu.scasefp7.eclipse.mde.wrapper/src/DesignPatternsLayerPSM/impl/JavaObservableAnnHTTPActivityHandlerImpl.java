/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler;
import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler;
import DesignPatternsLayerPSM.JavaObserver;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Observable Ann HTTP Activity Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl#getIsObservedBy <em>Is Observed By</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl#getExtendsAnnJavaHTTPActivityHandler <em>Extends Ann Java HTTP Activity Handler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaObservableAnnHTTPActivityHandlerImpl extends AnnotationImpl implements JavaObservableAnnHTTPActivityHandler {
	/**
	 * The cached value of the '{@link #getIsObservedBy() <em>Is Observed By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsObservedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaObserver> isObservedBy;

	/**
	 * The cached value of the '{@link #getReferencesAnnJavaResourceModel() <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaResourceModel referencesAnnJavaResourceModel;

	/**
	 * The cached value of the '{@link #getExtendsAnnJavaHTTPActivityHandler() <em>Extends Ann Java HTTP Activity Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtendsAnnJavaHTTPActivityHandler()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaHTTPActivityHandler extendsAnnJavaHTTPActivityHandler;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaObservableAnnHTTPActivityHandlerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaObserver> getIsObservedBy() {
		if (isObservedBy == null) {
			isObservedBy = new EObjectResolvingEList<JavaObserver>(JavaObserver.class, this, DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY);
		}
		return isObservedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel getReferencesAnnJavaResourceModel() {
		if (referencesAnnJavaResourceModel != null && referencesAnnJavaResourceModel.eIsProxy()) {
			InternalEObject oldReferencesAnnJavaResourceModel = (InternalEObject)referencesAnnJavaResourceModel;
			referencesAnnJavaResourceModel = (AnnJavaResourceModel)eResolveProxy(oldReferencesAnnJavaResourceModel);
			if (referencesAnnJavaResourceModel != oldReferencesAnnJavaResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
			}
		}
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel basicGetReferencesAnnJavaResourceModel() {
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnJavaResourceModel(AnnJavaResourceModel newReferencesAnnJavaResourceModel) {
		AnnJavaResourceModel oldReferencesAnnJavaResourceModel = referencesAnnJavaResourceModel;
		referencesAnnJavaResourceModel = newReferencesAnnJavaResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaHTTPActivityHandler getExtendsAnnJavaHTTPActivityHandler() {
		if (extendsAnnJavaHTTPActivityHandler != null && extendsAnnJavaHTTPActivityHandler.eIsProxy()) {
			InternalEObject oldExtendsAnnJavaHTTPActivityHandler = (InternalEObject)extendsAnnJavaHTTPActivityHandler;
			extendsAnnJavaHTTPActivityHandler = (AnnJavaHTTPActivityHandler)eResolveProxy(oldExtendsAnnJavaHTTPActivityHandler);
			if (extendsAnnJavaHTTPActivityHandler != oldExtendsAnnJavaHTTPActivityHandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER, oldExtendsAnnJavaHTTPActivityHandler, extendsAnnJavaHTTPActivityHandler));
			}
		}
		return extendsAnnJavaHTTPActivityHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaHTTPActivityHandler basicGetExtendsAnnJavaHTTPActivityHandler() {
		return extendsAnnJavaHTTPActivityHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExtendsAnnJavaHTTPActivityHandler(AnnJavaHTTPActivityHandler newExtendsAnnJavaHTTPActivityHandler) {
		AnnJavaHTTPActivityHandler oldExtendsAnnJavaHTTPActivityHandler = extendsAnnJavaHTTPActivityHandler;
		extendsAnnJavaHTTPActivityHandler = newExtendsAnnJavaHTTPActivityHandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER, oldExtendsAnnJavaHTTPActivityHandler, extendsAnnJavaHTTPActivityHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY:
				return getIsObservedBy();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				if (resolve) return getReferencesAnnJavaResourceModel();
				return basicGetReferencesAnnJavaResourceModel();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER:
				if (resolve) return getExtendsAnnJavaHTTPActivityHandler();
				return basicGetExtendsAnnJavaHTTPActivityHandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY:
				getIsObservedBy().clear();
				getIsObservedBy().addAll((Collection<? extends JavaObserver>)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER:
				setExtendsAnnJavaHTTPActivityHandler((AnnJavaHTTPActivityHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY:
				getIsObservedBy().clear();
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)null);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER:
				setExtendsAnnJavaHTTPActivityHandler((AnnJavaHTTPActivityHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY:
				return isObservedBy != null && !isObservedBy.isEmpty();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				return referencesAnnJavaResourceModel != null;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER:
				return extendsAnnJavaHTTPActivityHandler != null;
		}
		return super.eIsSet(featureID);
	}

} //JavaObservableAnnHTTPActivityHandlerImpl
