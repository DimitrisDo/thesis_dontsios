/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler;
import DesignPatternsLayerPSM.JavaObserver;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Observer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObserverImpl#getObserves <em>Observes</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObserverImpl#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaObserverImpl#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaObserverImpl extends MinimalEObjectImpl.Container implements JavaObserver {
	/**
	 * The cached value of the '{@link #getObserves() <em>Observes</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObserves()
	 * @generated
	 * @ordered
	 */
	protected JavaObservableAnnHTTPActivityHandler observes;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferencesAnnJavaResourceModel() <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaResourceModel referencesAnnJavaResourceModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaObserverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_OBSERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaObservableAnnHTTPActivityHandler getObserves() {
		if (observes != null && observes.eIsProxy()) {
			InternalEObject oldObserves = (InternalEObject)observes;
			observes = (JavaObservableAnnHTTPActivityHandler)eResolveProxy(oldObserves);
			if (observes != oldObserves) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES, oldObserves, observes));
			}
		}
		return observes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaObservableAnnHTTPActivityHandler basicGetObserves() {
		return observes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObserves(JavaObservableAnnHTTPActivityHandler newObserves) {
		JavaObservableAnnHTTPActivityHandler oldObserves = observes;
		observes = newObserves;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES, oldObserves, observes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_OBSERVER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel getReferencesAnnJavaResourceModel() {
		if (referencesAnnJavaResourceModel != null && referencesAnnJavaResourceModel.eIsProxy()) {
			InternalEObject oldReferencesAnnJavaResourceModel = (InternalEObject)referencesAnnJavaResourceModel;
			referencesAnnJavaResourceModel = (AnnJavaResourceModel)eResolveProxy(oldReferencesAnnJavaResourceModel);
			if (referencesAnnJavaResourceModel != oldReferencesAnnJavaResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
			}
		}
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel basicGetReferencesAnnJavaResourceModel() {
		return referencesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencesAnnJavaResourceModel(AnnJavaResourceModel newReferencesAnnJavaResourceModel) {
		AnnJavaResourceModel oldReferencesAnnJavaResourceModel = referencesAnnJavaResourceModel;
		referencesAnnJavaResourceModel = newReferencesAnnJavaResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL, oldReferencesAnnJavaResourceModel, referencesAnnJavaResourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void update() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES:
				if (resolve) return getObserves();
				return basicGetObserves();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__NAME:
				return getName();
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				if (resolve) return getReferencesAnnJavaResourceModel();
				return basicGetReferencesAnnJavaResourceModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES:
				setObserves((JavaObservableAnnHTTPActivityHandler)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__NAME:
				setName((String)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES:
				setObserves((JavaObservableAnnHTTPActivityHandler)null);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				setReferencesAnnJavaResourceModel((AnnJavaResourceModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__OBSERVES:
				return observes != null;
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL:
				return referencesAnnJavaResourceModel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER___UPDATE:
				update();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //JavaObserverImpl
