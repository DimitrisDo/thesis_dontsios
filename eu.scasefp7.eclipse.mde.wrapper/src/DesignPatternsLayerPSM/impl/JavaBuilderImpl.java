/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaBuilder;
import DesignPatternsLayerPSM.JavaRepresentation;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Builder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaBuilderImpl#getAssociatesAnnJavaResourceModel <em>Associates Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaBuilderImpl extends MinimalEObjectImpl.Container implements JavaBuilder {
	/**
	 * The cached value of the '{@link #getAssociatesAnnJavaResourceModel() <em>Associates Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatesAnnJavaResourceModel()
	 * @generated
	 * @ordered
	 */
	protected AnnJavaResourceModel associatesAnnJavaResourceModel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaBuilderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_BUILDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel getAssociatesAnnJavaResourceModel() {
		if (associatesAnnJavaResourceModel != null && associatesAnnJavaResourceModel.eIsProxy()) {
			InternalEObject oldAssociatesAnnJavaResourceModel = (InternalEObject)associatesAnnJavaResourceModel;
			associatesAnnJavaResourceModel = (AnnJavaResourceModel)eResolveProxy(oldAssociatesAnnJavaResourceModel);
			if (associatesAnnJavaResourceModel != oldAssociatesAnnJavaResourceModel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL, oldAssociatesAnnJavaResourceModel, associatesAnnJavaResourceModel));
			}
		}
		return associatesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnJavaResourceModel basicGetAssociatesAnnJavaResourceModel() {
		return associatesAnnJavaResourceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatesAnnJavaResourceModel(AnnJavaResourceModel newAssociatesAnnJavaResourceModel) {
		AnnJavaResourceModel oldAssociatesAnnJavaResourceModel = associatesAnnJavaResourceModel;
		associatesAnnJavaResourceModel = newAssociatesAnnJavaResourceModel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL, oldAssociatesAnnJavaResourceModel, associatesAnnJavaResourceModel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaRepresentation buildsJavaRepresentation() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL:
				if (resolve) return getAssociatesAnnJavaResourceModel();
				return basicGetAssociatesAnnJavaResourceModel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL:
				setAssociatesAnnJavaResourceModel((AnnJavaResourceModel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL:
				setAssociatesAnnJavaResourceModel((AnnJavaResourceModel)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL:
				return associatesAnnJavaResourceModel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER___BUILDS_JAVA_REPRESENTATION:
				return buildsJavaRepresentation();
		}
		return super.eInvoke(operationID, arguments);
	}

} //JavaBuilderImpl
