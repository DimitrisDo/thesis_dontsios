/**
 */
package DesignPatternsLayerPSM.impl;

import AnnotationLayerStack.AnnotationLayerStackPackage;

import AnnotationLayerStack.impl.AnnotationLayerStackPackageImpl;

import AuthenticationLayerPSM.AuthenticationLayerPSMPackage;

import AuthenticationLayerPSM.impl.AuthenticationLayerPSMPackageImpl;

import DesignPatternsLayerPSM.AnnJavaAlgoResourceModel;
import DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler;
import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.AnnPSMComponentProperty;
import DesignPatternsLayerPSM.AnnotatedElement;
import DesignPatternsLayerPSM.Annotation;
import DesignPatternsLayerPSM.AnnotationModel;
import DesignPatternsLayerPSM.DesignPattern;
import DesignPatternsLayerPSM.DesignPatternModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMFactory;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaBridgePattern;
import DesignPatternsLayerPSM.JavaBuilder;
import DesignPatternsLayerPSM.JavaBuilderPattern;
import DesignPatternsLayerPSM.JavaConcreteBuilder;
import DesignPatternsLayerPSM.JavaDirector;
import DesignPatternsLayerPSM.JavaMementoPattern;
import DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler;
import DesignPatternsLayerPSM.JavaObserver;
import DesignPatternsLayerPSM.JavaObserverPattern;
import DesignPatternsLayerPSM.JavaRepresentation;
import DesignPatternsLayerPSM.JavaResourceModelMemento;

import ExternalServiceLayerPSM.ExternalServiceLayerPSMPackage;

import ExternalServiceLayerPSM.impl.ExternalServiceLayerPSMPackageImpl;

import RESTfulServicePSM.RESTfulServicePSMPackage;

import RESTfulServicePSM.impl.RESTfulServicePSMPackageImpl;

import SearchLayerPSM.SearchLayerPSMPackage;

import SearchLayerPSM.impl.SearchLayerPSMPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsLayerPSMPackageImpl extends EPackageImpl implements DesignPatternsLayerPSMPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotatedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annPSMComponentPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annJavaResourceModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annJavaAlgoResourceModelEClass = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaBridgePatternEClass = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaBuilderPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaDirectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaConcreteBuilderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaBuilderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaRepresentationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass designPatternModelEClass = null;

	/**
 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaObserverPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaObserverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaObservableAnnHTTPActivityHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annJavaHTTPActivityHandlerEClass = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaMementoPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javaResourceModelMementoEClass = null;
	
	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DesignPatternsLayerPSMPackageImpl() {
		super(eNS_URI, DesignPatternsLayerPSMFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DesignPatternsLayerPSMPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DesignPatternsLayerPSMPackage init() {
		if (isInited) return (DesignPatternsLayerPSMPackage)EPackage.Registry.INSTANCE.getEPackage(DesignPatternsLayerPSMPackage.eNS_URI);

		// Obtain or create and register package
		DesignPatternsLayerPSMPackageImpl theDesignPatternsLayerPSMPackage = (DesignPatternsLayerPSMPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DesignPatternsLayerPSMPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DesignPatternsLayerPSMPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		AnnotationLayerStackPackageImpl theAnnotationLayerStackPackage = (AnnotationLayerStackPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AnnotationLayerStackPackage.eNS_URI) instanceof AnnotationLayerStackPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AnnotationLayerStackPackage.eNS_URI) : AnnotationLayerStackPackage.eINSTANCE);
		AuthenticationLayerPSMPackageImpl theAuthenticationLayerPSMPackage = (AuthenticationLayerPSMPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(AuthenticationLayerPSMPackage.eNS_URI) instanceof AuthenticationLayerPSMPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(AuthenticationLayerPSMPackage.eNS_URI) : AuthenticationLayerPSMPackage.eINSTANCE);
		RESTfulServicePSMPackageImpl theRESTfulServicePSMPackage = (RESTfulServicePSMPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RESTfulServicePSMPackage.eNS_URI) instanceof RESTfulServicePSMPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RESTfulServicePSMPackage.eNS_URI) : RESTfulServicePSMPackage.eINSTANCE);
		SearchLayerPSMPackageImpl theSearchLayerPSMPackage = (SearchLayerPSMPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SearchLayerPSMPackage.eNS_URI) instanceof SearchLayerPSMPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SearchLayerPSMPackage.eNS_URI) : SearchLayerPSMPackage.eINSTANCE);
		ExternalServiceLayerPSMPackageImpl theExternalServiceLayerPSMPackage = (ExternalServiceLayerPSMPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExternalServiceLayerPSMPackage.eNS_URI) instanceof ExternalServiceLayerPSMPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExternalServiceLayerPSMPackage.eNS_URI) : ExternalServiceLayerPSMPackage.eINSTANCE);

		// Create package meta-data objects
		theDesignPatternsLayerPSMPackage.createPackageContents();
		theAnnotationLayerStackPackage.createPackageContents();
		theAuthenticationLayerPSMPackage.createPackageContents();
		theRESTfulServicePSMPackage.createPackageContents();
		theSearchLayerPSMPackage.createPackageContents();
		theExternalServiceLayerPSMPackage.createPackageContents();

		// Initialize created meta-data
		theDesignPatternsLayerPSMPackage.initializePackageContents();
		theAnnotationLayerStackPackage.initializePackageContents();
		theAuthenticationLayerPSMPackage.initializePackageContents();
		theRESTfulServicePSMPackage.initializePackageContents();
		theSearchLayerPSMPackage.initializePackageContents();
		theExternalServiceLayerPSMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDesignPatternsLayerPSMPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DesignPatternsLayerPSMPackage.eNS_URI, theDesignPatternsLayerPSMPackage);
		return theDesignPatternsLayerPSMPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotationModel() {
		return annotationModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotationModel_HasAnnotatedElement() {
		return (EReference)annotationModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotationModel_HasAnnotation() {
		return (EReference)annotationModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnnotationModel_Name() {
		return (EAttribute)annotationModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnotationModel_AnnotatesRESTfulServicePSM() {
		return (EReference)annotationModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnnotationModel_AnnotationType() {
		return (EAttribute)annotationModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotatedElement() {
		return annotatedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotation() {
		return annotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnPSMComponentProperty() {
		return annPSMComponentPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnPSMComponentProperty_AnnotatesPSMComponentProperty() {
		return (EReference)annPSMComponentPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnJavaResourceModel() {
		return annJavaResourceModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnJavaResourceModel_AnnotatesJavaResourceModel() {
		return (EReference)annJavaResourceModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnJavaAlgoResourceModel() {
		return annJavaAlgoResourceModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnJavaAlgoResourceModel_AnnotatesJavaAlgoResourceModel() {
		return (EReference)annJavaAlgoResourceModelEClass.getEStructuralFeatures().get(0);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPattern() {
		return designPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaBridgePattern() {
		return javaBridgePatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaBridgePattern_AssociatesAnnJavaAlgoResourceModel() {
		return (EReference)javaBridgePatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaBridgePattern_BMakeBridgePatternForExternalService() {
		return (EAttribute)javaBridgePatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaBridgePattern_BMakeBridgePatternForSearch() {
		return (EAttribute)javaBridgePatternEClass.getEStructuralFeatures().get(2);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaBuilderPattern() {
		return javaBuilderPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaBuilderPattern_HasJavaDirector() {
		return (EReference)javaBuilderPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaBuilderPattern_AssociatesAnnJavaResourceModels() {
		return (EReference)javaBuilderPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaDirector() {
		return javaDirectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaDirector_HasJavaBuilder() {
		return (EReference)javaDirectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaConcreteBuilder() {
		return javaConcreteBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaConcreteBuilder_BuildsJavaRepresentation() {
		return (EReference)javaConcreteBuilderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getJavaConcreteBuilder__BuildsJavaRepresentation() {
		return javaConcreteBuilderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaBuilder() {
		return javaBuilderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaBuilder_AssociatesAnnJavaResourceModel() {
		return (EReference)javaBuilderEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getJavaBuilder__BuildsJavaRepresentation() {
		return javaBuilderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaRepresentation() {
		return javaRepresentationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaRepresentation_RefersTo() {
		return (EReference)javaRepresentationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaRepresentation_Has() {
		return (EReference)javaRepresentationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaRepresentation_Name() {
		return (EAttribute)javaRepresentationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaRepresentation_ResourceInstanceId() {
		return (EAttribute)javaRepresentationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDesignPatternModel() {
		return designPatternModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDesignPatternModel_HasDesignPattern() {
		return (EReference)designPatternModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaObserverPattern() {
		return javaObserverPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObserverPattern_HasJavaObserver() {
		return (EReference)javaObserverPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaObserver() {
		return javaObserverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObserver_Observes() {
		return (EReference)javaObserverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaObserver_Name() {
		return (EAttribute)javaObserverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObserver_ReferencesAnnJavaResourceModel() {
		return (EReference)javaObserverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getJavaObserver__Update() {
		return javaObserverEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaObservableAnnHTTPActivityHandler() {
		return javaObservableAnnHTTPActivityHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObservableAnnHTTPActivityHandler_IsObservedBy() {
		return (EReference)javaObservableAnnHTTPActivityHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObservableAnnHTTPActivityHandler_ReferencesAnnJavaResourceModel() {
		return (EReference)javaObservableAnnHTTPActivityHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaObservableAnnHTTPActivityHandler_ExtendsAnnJavaHTTPActivityHandler() {
		return (EReference)javaObservableAnnHTTPActivityHandlerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnJavaHTTPActivityHandler() {
		return annJavaHTTPActivityHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAnnJavaHTTPActivityHandler_AnnotatesHTTPActivityHandler() {
		return (EReference)annJavaHTTPActivityHandlerEClass.getEStructuralFeatures().get(0);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaMementoPattern() {
		return javaMementoPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaMementoPattern_HasJavaResourceModelMemento() {
		return (EReference)javaMementoPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavaResourceModelMemento() {
		return javaResourceModelMementoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJavaResourceModelMemento_MementoNum() {
		return (EAttribute)javaResourceModelMementoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJavaResourceModelMemento_ReferencesAnnJavaResourceModel() {
		return (EReference)javaResourceModelMementoEClass.getEStructuralFeatures().get(1);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */	
	 public DesignPatternsLayerPSMFactory getDesignPatternsLayerPSMFactory() {
		return (DesignPatternsLayerPSMFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		annotationModelEClass = createEClass(ANNOTATION_MODEL);
		createEReference(annotationModelEClass, ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT);
		createEReference(annotationModelEClass, ANNOTATION_MODEL__HAS_ANNOTATION);
		createEAttribute(annotationModelEClass, ANNOTATION_MODEL__NAME);
		createEReference(annotationModelEClass, ANNOTATION_MODEL__ANNOTATES_RES_TFUL_SERVICE_PSM);
		createEAttribute(annotationModelEClass, ANNOTATION_MODEL__ANNOTATION_TYPE);

		annotatedElementEClass = createEClass(ANNOTATED_ELEMENT);

		annotationEClass = createEClass(ANNOTATION);

		annPSMComponentPropertyEClass = createEClass(ANN_PSM_COMPONENT_PROPERTY);
		createEReference(annPSMComponentPropertyEClass, ANN_PSM_COMPONENT_PROPERTY__ANNOTATES_PSM_COMPONENT_PROPERTY);

		annJavaResourceModelEClass = createEClass(ANN_JAVA_RESOURCE_MODEL);
		createEReference(annJavaResourceModelEClass, ANN_JAVA_RESOURCE_MODEL__ANNOTATES_JAVA_RESOURCE_MODEL);

		annJavaAlgoResourceModelEClass = createEClass(ANN_JAVA_ALGO_RESOURCE_MODEL);
		createEReference(annJavaAlgoResourceModelEClass, ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL);
		designPatternEClass = createEClass(DESIGN_PATTERN);

		javaBridgePatternEClass = createEClass(JAVA_BRIDGE_PATTERN);
		createEReference(javaBridgePatternEClass, JAVA_BRIDGE_PATTERN__ASSOCIATES_ANN_JAVA_ALGO_RESOURCE_MODEL);
		createEAttribute(javaBridgePatternEClass, JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE);
		createEAttribute(javaBridgePatternEClass, JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH);
		javaBuilderPatternEClass = createEClass(JAVA_BUILDER_PATTERN);
		createEReference(javaBuilderPatternEClass, JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR);
		createEReference(javaBuilderPatternEClass, JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS);

		javaDirectorEClass = createEClass(JAVA_DIRECTOR);
		createEReference(javaDirectorEClass, JAVA_DIRECTOR__HAS_JAVA_BUILDER);

		javaConcreteBuilderEClass = createEClass(JAVA_CONCRETE_BUILDER);
		createEReference(javaConcreteBuilderEClass, JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION);
		createEOperation(javaConcreteBuilderEClass, JAVA_CONCRETE_BUILDER___BUILDS_JAVA_REPRESENTATION);

		javaBuilderEClass = createEClass(JAVA_BUILDER);
		createEReference(javaBuilderEClass, JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL);
		createEOperation(javaBuilderEClass, JAVA_BUILDER___BUILDS_JAVA_REPRESENTATION);

		javaRepresentationEClass = createEClass(JAVA_REPRESENTATION);
		createEReference(javaRepresentationEClass, JAVA_REPRESENTATION__REFERS_TO);
		createEReference(javaRepresentationEClass, JAVA_REPRESENTATION__HAS);
		createEAttribute(javaRepresentationEClass, JAVA_REPRESENTATION__NAME);
		createEAttribute(javaRepresentationEClass, JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID);

		designPatternModelEClass = createEClass(DESIGN_PATTERN_MODEL);
		createEReference(designPatternModelEClass, DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN);

		javaObserverPatternEClass = createEClass(JAVA_OBSERVER_PATTERN);
		createEReference(javaObserverPatternEClass, JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER);

		javaObserverEClass = createEClass(JAVA_OBSERVER);
		createEReference(javaObserverEClass, JAVA_OBSERVER__OBSERVES);
		createEAttribute(javaObserverEClass, JAVA_OBSERVER__NAME);
		createEReference(javaObserverEClass, JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL);
		createEOperation(javaObserverEClass, JAVA_OBSERVER___UPDATE);

		javaObservableAnnHTTPActivityHandlerEClass = createEClass(JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER);
		createEReference(javaObservableAnnHTTPActivityHandlerEClass, JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY);
		createEReference(javaObservableAnnHTTPActivityHandlerEClass, JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL);
		createEReference(javaObservableAnnHTTPActivityHandlerEClass, JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER);

		annJavaHTTPActivityHandlerEClass = createEClass(ANN_JAVA_HTTP_ACTIVITY_HANDLER);
		createEReference(annJavaHTTPActivityHandlerEClass, ANN_JAVA_HTTP_ACTIVITY_HANDLER__ANNOTATES_HTTP_ACTIVITY_HANDLER);

		javaMementoPatternEClass = createEClass(JAVA_MEMENTO_PATTERN);
		createEReference(javaMementoPatternEClass, JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO);

		javaResourceModelMementoEClass = createEClass(JAVA_RESOURCE_MODEL_MEMENTO);
		createEAttribute(javaResourceModelMementoEClass, JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM);
		createEReference(javaResourceModelMementoEClass, JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RESTfulServicePSMPackage theRESTfulServicePSMPackage = (RESTfulServicePSMPackage)EPackage.Registry.INSTANCE.getEPackage(RESTfulServicePSMPackage.eNS_URI);
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		annPSMComponentPropertyEClass.getESuperTypes().add(this.getAnnotatedElement());
		annJavaResourceModelEClass.getESuperTypes().add(this.getAnnotatedElement());
		annJavaAlgoResourceModelEClass.getESuperTypes().add(this.getAnnotatedElement());
		designPatternEClass.getESuperTypes().add(this.getAnnotation());
		javaBridgePatternEClass.getESuperTypes().add(this.getDesignPattern());
		javaBuilderPatternEClass.getESuperTypes().add(this.getDesignPattern());
		javaConcreteBuilderEClass.getESuperTypes().add(this.getJavaBuilder());
		javaObserverPatternEClass.getESuperTypes().add(this.getDesignPattern());
		javaObservableAnnHTTPActivityHandlerEClass.getESuperTypes().add(this.getAnnotation());
		annJavaHTTPActivityHandlerEClass.getESuperTypes().add(this.getAnnotatedElement());
		javaMementoPatternEClass.getESuperTypes().add(this.getDesignPattern());

		// Initialize classes, features, and operations; add parameters
		initEClass(annotationModelEClass, AnnotationModel.class, "AnnotationModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnotationModel_HasAnnotatedElement(), this.getAnnotatedElement(), null, "hasAnnotatedElement", null, 1, -1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotationModel_HasAnnotation(), this.getAnnotation(), null, "hasAnnotation", null, 1, -1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnnotationModel_Name(), ecorePackage.getEString(), "name", null, 0, 1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAnnotationModel_AnnotatesRESTfulServicePSM(), theRESTfulServicePSMPackage.getServicePSM(), null, "annotatesRESTfulServicePSM", null, 1, 1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAnnotationModel_AnnotationType(), ecorePackage.getEString(), "annotationType", "DesignPatternsLayer", 0, 1, AnnotationModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(annotatedElementEClass, AnnotatedElement.class, "AnnotatedElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(annotationEClass, Annotation.class, "Annotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(annPSMComponentPropertyEClass, AnnPSMComponentProperty.class, "AnnPSMComponentProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnPSMComponentProperty_AnnotatesPSMComponentProperty(), theRESTfulServicePSMPackage.getPSMComponentProperty(), null, "annotatesPSMComponentProperty", null, 1, 1, AnnPSMComponentProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annJavaResourceModelEClass, AnnJavaResourceModel.class, "AnnJavaResourceModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnJavaResourceModel_AnnotatesJavaResourceModel(), theRESTfulServicePSMPackage.getJavaResourceModel(), null, "annotatesJavaResourceModel", null, 1, 1, AnnJavaResourceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annJavaAlgoResourceModelEClass, AnnJavaAlgoResourceModel.class, "AnnJavaAlgoResourceModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnJavaAlgoResourceModel_AnnotatesJavaAlgoResourceModel(), theRESTfulServicePSMPackage.getJavaAlgoResourceModel(), null, "annotatesJavaAlgoResourceModel", null, 1, 1, AnnJavaAlgoResourceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEClass(designPatternEClass, DesignPattern.class, "DesignPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(javaBridgePatternEClass, JavaBridgePattern.class, "JavaBridgePattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaBridgePattern_AssociatesAnnJavaAlgoResourceModel(), this.getAnnJavaAlgoResourceModel(), null, "associatesAnnJavaAlgoResourceModel", null, 1, -1, JavaBridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaBridgePattern_BMakeBridgePatternForExternalService(), theXMLTypePackage.getBoolean(), "bMakeBridgePatternForExternalService", "false", 0, 1, JavaBridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaBridgePattern_BMakeBridgePatternForSearch(), theXMLTypePackage.getBoolean(), "bMakeBridgePatternForSearch", "true", 0, 1, JavaBridgePattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaBuilderPatternEClass, JavaBuilderPattern.class, "JavaBuilderPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaBuilderPattern_HasJavaDirector(), this.getJavaDirector(), null, "hasJavaDirector", null, 1, 1, JavaBuilderPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJavaBuilderPattern_AssociatesAnnJavaResourceModels(), this.getAnnJavaResourceModel(), null, "associatesAnnJavaResourceModels", null, 1, -1, JavaBuilderPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaDirectorEClass, JavaDirector.class, "JavaDirector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaDirector_HasJavaBuilder(), this.getJavaBuilder(), null, "hasJavaBuilder", null, 1, -1, JavaDirector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaConcreteBuilderEClass, JavaConcreteBuilder.class, "JavaConcreteBuilder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaConcreteBuilder_BuildsJavaRepresentation(), this.getJavaRepresentation(), null, "buildsJavaRepresentation", null, 1, 1, JavaConcreteBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getJavaConcreteBuilder__BuildsJavaRepresentation(), this.getJavaRepresentation(), "buildsJavaRepresentation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(javaBuilderEClass, JavaBuilder.class, "JavaBuilder", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaBuilder_AssociatesAnnJavaResourceModel(), this.getAnnJavaResourceModel(), null, "associatesAnnJavaResourceModel", null, 1, 1, JavaBuilder.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getJavaBuilder__BuildsJavaRepresentation(), this.getJavaRepresentation(), "buildsJavaRepresentation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(javaRepresentationEClass, JavaRepresentation.class, "JavaRepresentation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaRepresentation_RefersTo(), this.getAnnJavaResourceModel(), null, "refersTo", null, 1, 1, JavaRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJavaRepresentation_Has(), this.getAnnPSMComponentProperty(), null, "has", null, 0, -1, JavaRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaRepresentation_Name(), ecorePackage.getEString(), "name", null, 0, 1, JavaRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaRepresentation_ResourceInstanceId(), ecorePackage.getEString(), "resourceInstanceId", null, 0, 1, JavaRepresentation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(designPatternModelEClass, DesignPatternModel.class, "DesignPatternModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDesignPatternModel_HasDesignPattern(), this.getDesignPattern(), null, "hasDesignPattern", null, 0, -1, DesignPatternModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaObserverPatternEClass, JavaObserverPattern.class, "JavaObserverPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaObserverPattern_HasJavaObserver(), this.getJavaObserver(), null, "hasJavaObserver", null, 1, -1, JavaObserverPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaObserverEClass, JavaObserver.class, "JavaObserver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaObserver_Observes(), this.getJavaObservableAnnHTTPActivityHandler(), null, "observes", null, 1, 1, JavaObserver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJavaObserver_Name(), theXMLTypePackage.getString(), "name", null, 0, 1, JavaObserver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJavaObserver_ReferencesAnnJavaResourceModel(), this.getAnnJavaResourceModel(), null, "referencesAnnJavaResourceModel", null, 1, 1, JavaObserver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getJavaObserver__Update(), null, "update", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(javaObservableAnnHTTPActivityHandlerEClass, JavaObservableAnnHTTPActivityHandler.class, "JavaObservableAnnHTTPActivityHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaObservableAnnHTTPActivityHandler_IsObservedBy(), this.getJavaObserver(), null, "isObservedBy", null, 0, -1, JavaObservableAnnHTTPActivityHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJavaObservableAnnHTTPActivityHandler_ReferencesAnnJavaResourceModel(), this.getAnnJavaResourceModel(), null, "referencesAnnJavaResourceModel", null, 1, 1, JavaObservableAnnHTTPActivityHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJavaObservableAnnHTTPActivityHandler_ExtendsAnnJavaHTTPActivityHandler(), this.getAnnJavaHTTPActivityHandler(), null, "extendsAnnJavaHTTPActivityHandler", null, 1, 1, JavaObservableAnnHTTPActivityHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annJavaHTTPActivityHandlerEClass, AnnJavaHTTPActivityHandler.class, "AnnJavaHTTPActivityHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAnnJavaHTTPActivityHandler_AnnotatesHTTPActivityHandler(), theRESTfulServicePSMPackage.getHTTPActivityHandler(), null, "annotatesHTTPActivityHandler", null, 0, 1, AnnJavaHTTPActivityHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaMementoPatternEClass, JavaMementoPattern.class, "JavaMementoPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getJavaMementoPattern_HasJavaResourceModelMemento(), this.getJavaResourceModelMemento(), null, "hasJavaResourceModelMemento", null, 1, -1, JavaMementoPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javaResourceModelMementoEClass, JavaResourceModelMemento.class, "JavaResourceModelMemento", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJavaResourceModelMemento_MementoNum(), theXMLTypePackage.getInt(), "mementoNum", "1", 0, 1, JavaResourceModelMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getJavaResourceModelMemento_ReferencesAnnJavaResourceModel(), this.getAnnJavaResourceModel(), null, "referencesAnnJavaResourceModel", null, 1, 1, JavaResourceModelMemento.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //DesignPatternsLayerPSMPackageImpl
