/**
 */
package DesignPatternsLayerPSM.impl;

import DesignPatternsLayerPSM.AnnJavaResourceModel;
import DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage;
import DesignPatternsLayerPSM.JavaBuilderPattern;
import DesignPatternsLayerPSM.JavaDirector;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Builder Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl#getHasJavaDirector <em>Has Java Director</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl#getAssociatesAnnJavaResourceModels <em>Associates Ann Java Resource Models</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaBuilderPatternImpl extends DesignPatternImpl implements JavaBuilderPattern {
	/**
	 * The cached value of the '{@link #getHasJavaDirector() <em>Has Java Director</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasJavaDirector()
	 * @generated
	 * @ordered
	 */
	protected JavaDirector hasJavaDirector;

	/**
	 * The cached value of the '{@link #getAssociatesAnnJavaResourceModels() <em>Associates Ann Java Resource Models</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatesAnnJavaResourceModels()
	 * @generated
	 * @ordered
	 */
	protected EList<AnnJavaResourceModel> associatesAnnJavaResourceModels;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaBuilderPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsLayerPSMPackage.Literals.JAVA_BUILDER_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JavaDirector getHasJavaDirector() {
		return hasJavaDirector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHasJavaDirector(JavaDirector newHasJavaDirector, NotificationChain msgs) {
		JavaDirector oldHasJavaDirector = hasJavaDirector;
		hasJavaDirector = newHasJavaDirector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR, oldHasJavaDirector, newHasJavaDirector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasJavaDirector(JavaDirector newHasJavaDirector) {
		if (newHasJavaDirector != hasJavaDirector) {
			NotificationChain msgs = null;
			if (hasJavaDirector != null)
				msgs = ((InternalEObject)hasJavaDirector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR, null, msgs);
			if (newHasJavaDirector != null)
				msgs = ((InternalEObject)newHasJavaDirector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR, null, msgs);
			msgs = basicSetHasJavaDirector(newHasJavaDirector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR, newHasJavaDirector, newHasJavaDirector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AnnJavaResourceModel> getAssociatesAnnJavaResourceModels() {
		if (associatesAnnJavaResourceModels == null) {
			associatesAnnJavaResourceModels = new EObjectResolvingEList<AnnJavaResourceModel>(AnnJavaResourceModel.class, this, DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS);
		}
		return associatesAnnJavaResourceModels;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR:
				return basicSetHasJavaDirector(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR:
				return getHasJavaDirector();
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS:
				return getAssociatesAnnJavaResourceModels();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR:
				setHasJavaDirector((JavaDirector)newValue);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS:
				getAssociatesAnnJavaResourceModels().clear();
				getAssociatesAnnJavaResourceModels().addAll((Collection<? extends AnnJavaResourceModel>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR:
				setHasJavaDirector((JavaDirector)null);
				return;
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS:
				getAssociatesAnnJavaResourceModels().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR:
				return hasJavaDirector != null;
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS:
				return associatesAnnJavaResourceModels != null && !associatesAnnJavaResourceModels.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaBuilderPatternImpl
