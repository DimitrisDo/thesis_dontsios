/**
 */
package DesignPatternsLayerPSM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Concrete Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaConcreteBuilder#getBuildsJavaRepresentation <em>Builds Java Representation</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaConcreteBuilder()
 * @model
 * @generated
 */
public interface JavaConcreteBuilder extends JavaBuilder {
	/**
	 * Returns the value of the '<em><b>Builds Java Representation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Builds Java Representation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Builds Java Representation</em>' containment reference.
	 * @see #setBuildsJavaRepresentation(JavaRepresentation)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaConcreteBuilder_BuildsJavaRepresentation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	JavaRepresentation getBuildsJavaRepresentation();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaConcreteBuilder#getBuildsJavaRepresentation <em>Builds Java Representation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Builds Java Representation</em>' containment reference.
	 * @see #getBuildsJavaRepresentation()
	 * @generated
	 */
	void setBuildsJavaRepresentation(JavaRepresentation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	JavaRepresentation buildsJavaRepresentation();

} // JavaConcreteBuilder
