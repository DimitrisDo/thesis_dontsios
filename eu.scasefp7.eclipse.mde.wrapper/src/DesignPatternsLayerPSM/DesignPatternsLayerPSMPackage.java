/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMFactory
 * @model kind="package"
 * @generated
 */
public interface DesignPatternsLayerPSMPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "DesignPatternsLayerPSM";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/plugin/eu.scasefp7.eclipse.mde.m2m/Metamodels/DesignPatternsLayerPSMMetamodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Eu.fp7.scase";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DesignPatternsLayerPSMPackage eINSTANCE = DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl.init();

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnotationModelImpl <em>Annotation Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnotationModelImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotationModel()
	 * @generated
	 */
	int ANNOTATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Has Annotated Element</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Has Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__HAS_ANNOTATION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__NAME = 2;

	/**
	 * The feature id for the '<em><b>Annotates RES Tful Service PSM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__ANNOTATES_RES_TFUL_SERVICE_PSM = 3;

	/**
	 * The feature id for the '<em><b>Annotation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL__ANNOTATION_TYPE = 4;

	
	/**
	 * The number of structural features of the '<em>Annotation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Annotation Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnotatedElementImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotatedElement()
	 * @generated
	 */
	int ANNOTATED_ELEMENT = 1;

	/**
	 * The number of structural features of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Annotated Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnotationImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 2;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnPSMComponentPropertyImpl <em>Ann PSM Component Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnPSMComponentPropertyImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnPSMComponentProperty()
	 * @generated
	 */
	int ANN_PSM_COMPONENT_PROPERTY = 3;

	/**
	 * The feature id for the '<em><b>Annotates PSM Component Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PSM_COMPONENT_PROPERTY__ANNOTATES_PSM_COMPONENT_PROPERTY = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann PSM Component Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PSM_COMPONENT_PROPERTY_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann PSM Component Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_PSM_COMPONENT_PROPERTY_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnJavaResourceModelImpl <em>Ann Java Resource Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnJavaResourceModelImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaResourceModel()
	 * @generated
	 */
	int ANN_JAVA_RESOURCE_MODEL = 4;

	/**
	 * The feature id for the '<em><b>Annotates Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_RESOURCE_MODEL__ANNOTATES_JAVA_RESOURCE_MODEL = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann Java Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_RESOURCE_MODEL_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann Java Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_RESOURCE_MODEL_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnJavaAlgoResourceModelImpl <em>Ann Java Algo Resource Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnJavaAlgoResourceModelImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaAlgoResourceModel()
	 * @generated
	 */
	int ANN_JAVA_ALGO_RESOURCE_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Annotates Java Algo Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann Java Algo Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_ALGO_RESOURCE_MODEL_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann Java Algo Resource Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_ALGO_RESOURCE_MODEL_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;
	
	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.DesignPatternImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getDesignPattern()
	 * @generated
	 */
	int DESIGN_PATTERN = 6;

	/**
	 * The number of structural features of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Design Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_OPERATION_COUNT = ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaBridgePatternImpl <em>Java Bridge Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaBridgePatternImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBridgePattern()
	 * @generated
	 */
	int JAVA_BRIDGE_PATTERN = 7;

	/**
	 * The feature id for the '<em><b>Associates Ann Java Algo Resource Model</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BRIDGE_PATTERN__ASSOCIATES_ANN_JAVA_ALGO_RESOURCE_MODEL = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH = DESIGN_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Java Bridge Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BRIDGE_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Java Bridge Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BRIDGE_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;
	
	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl <em>Java Builder Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBuilderPattern()
	 * @generated
	 */
	int JAVA_BUILDER_PATTERN = 8;

	/**
	 * The feature id for the '<em><b>Has Java Director</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Associates Ann Java Resource Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Java Builder Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Java Builder Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaDirectorImpl <em>Java Director</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaDirectorImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaDirector()
	 * @generated
	 */
	int JAVA_DIRECTOR = 9;

	/**
	 * The feature id for the '<em><b>Has Java Builder</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_DIRECTOR__HAS_JAVA_BUILDER = 0;

	/**
	 * The number of structural features of the '<em>Java Director</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_DIRECTOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Java Director</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_DIRECTOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaBuilderImpl <em>Java Builder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaBuilderImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBuilder()
	 * @generated
	 */
	int JAVA_BUILDER = 11;

	/**
	 * The feature id for the '<em><b>Associates Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL = 0;

	/**
	 * The number of structural features of the '<em>Java Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Builds Java Representation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER___BUILDS_JAVA_REPRESENTATION = 0;

	/**
	 * The number of operations of the '<em>Java Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_BUILDER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaConcreteBuilderImpl <em>Java Concrete Builder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaConcreteBuilderImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaConcreteBuilder()
	 * @generated
	 */
	int JAVA_CONCRETE_BUILDER = 10;

	/**
	 * The feature id for the '<em><b>Associates Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_CONCRETE_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL = JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL;

	/**
	 * The feature id for the '<em><b>Builds Java Representation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION = JAVA_BUILDER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Java Concrete Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_CONCRETE_BUILDER_FEATURE_COUNT = JAVA_BUILDER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Builds Java Representation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_CONCRETE_BUILDER___BUILDS_JAVA_REPRESENTATION = JAVA_BUILDER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Java Concrete Builder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_CONCRETE_BUILDER_OPERATION_COUNT = JAVA_BUILDER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl <em>Java Representation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaRepresentationImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaRepresentation()
	 * @generated
	 */
	int JAVA_REPRESENTATION = 12;

	/**
	 * The feature id for the '<em><b>Refers To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION__REFERS_TO = 0;

	/**
	 * The feature id for the '<em><b>Has</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION__HAS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION__NAME = 2;

	/**
	 * The feature id for the '<em><b>Resource Instance Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID = 3;

	/**
	 * The number of structural features of the '<em>Java Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Java Representation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_REPRESENTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.DesignPatternModelImpl <em>Design Pattern Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.DesignPatternModelImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getDesignPatternModel()
	 * @generated
	 */
	int DESIGN_PATTERN_MODEL = 13;

	/**
	 * The feature id for the '<em><b>Has Design Pattern</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN = 0;

	/**
	 * The number of structural features of the '<em>Design Pattern Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Design Pattern Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESIGN_PATTERN_MODEL_OPERATION_COUNT = 0;

	
	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaObserverPatternImpl <em>Java Observer Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaObserverPatternImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObserverPattern()
	 * @generated
	 */
	int JAVA_OBSERVER_PATTERN = 14;

	/**
	 * The feature id for the '<em><b>Has Java Observer</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Java Observer Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Java Observer Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaObserverImpl <em>Java Observer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaObserverImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObserver()
	 * @generated
	 */
	int JAVA_OBSERVER = 15;

	/**
	 * The feature id for the '<em><b>Observes</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER__OBSERVES = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER__NAME = 1;

	/**
	 * The feature id for the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL = 2;

	/**
	 * The number of structural features of the '<em>Java Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER___UPDATE = 0;

	/**
	 * The number of operations of the '<em>Java Observer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVER_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl <em>Java Observable Ann HTTP Activity Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObservableAnnHTTPActivityHandler()
	 * @generated
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER = 16;

	/**
	 * The feature id for the '<em><b>Is Observed By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY = ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL = ANNOTATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Extends Ann Java HTTP Activity Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER = ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Java Observable Ann HTTP Activity Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER_FEATURE_COUNT = ANNOTATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Java Observable Ann HTTP Activity Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER_OPERATION_COUNT = ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.AnnJavaHTTPActivityHandlerImpl <em>Ann Java HTTP Activity Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.AnnJavaHTTPActivityHandlerImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaHTTPActivityHandler()
	 * @generated
	 */
	int ANN_JAVA_HTTP_ACTIVITY_HANDLER = 17;

	/**
	 * The feature id for the '<em><b>Annotates HTTP Activity Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_HTTP_ACTIVITY_HANDLER__ANNOTATES_HTTP_ACTIVITY_HANDLER = ANNOTATED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Ann Java HTTP Activity Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_HTTP_ACTIVITY_HANDLER_FEATURE_COUNT = ANNOTATED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Ann Java HTTP Activity Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANN_JAVA_HTTP_ACTIVITY_HANDLER_OPERATION_COUNT = ANNOTATED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaMementoPatternImpl <em>Java Memento Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaMementoPatternImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaMementoPattern()
	 * @generated
	 */
	int JAVA_MEMENTO_PATTERN = 18;

	/**
	 * The feature id for the '<em><b>Has Java Resource Model Memento</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO = DESIGN_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Java Memento Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MEMENTO_PATTERN_FEATURE_COUNT = DESIGN_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Java Memento Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MEMENTO_PATTERN_OPERATION_COUNT = DESIGN_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl <em>Java Resource Model Memento</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl
	 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaResourceModelMemento()
	 * @generated
	 */
	int JAVA_RESOURCE_MODEL_MEMENTO = 19;

	/**
	 * The feature id for the '<em><b>Memento Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM = 0;

	/**
	 * The feature id for the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL = 1;

	/**
	 * The number of structural features of the '<em>Java Resource Model Memento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_RESOURCE_MODEL_MEMENTO_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Java Resource Model Memento</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_RESOURCE_MODEL_MEMENTO_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnotationModel <em>Annotation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation Model</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel
	 * @generated
	 */
	EClass getAnnotationModel();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.AnnotationModel#getHasAnnotatedElement <em>Has Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Annotated Element</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel#getHasAnnotatedElement()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EReference getAnnotationModel_HasAnnotatedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.AnnotationModel#getHasAnnotation <em>Has Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Annotation</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel#getHasAnnotation()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EReference getAnnotationModel_HasAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.AnnotationModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel#getName()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EAttribute getAnnotationModel_Name();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotatesRESTfulServicePSM <em>Annotates RES Tful Service PSM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates RES Tful Service PSM</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel#getAnnotatesRESTfulServicePSM()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EReference getAnnotationModel_AnnotatesRESTfulServicePSM();

	
	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotationType <em>Annotation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Annotation Type</em>'.
	 * @see DesignPatternsLayerPSM.AnnotationModel#getAnnotationType()
	 * @see #getAnnotationModel()
	 * @generated
	 */
	EAttribute getAnnotationModel_AnnotationType();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotated Element</em>'.
	 * @see DesignPatternsLayerPSM.AnnotatedElement
	 * @generated
	 */
	EClass getAnnotatedElement();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see DesignPatternsLayerPSM.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnPSMComponentProperty <em>Ann PSM Component Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann PSM Component Property</em>'.
	 * @see DesignPatternsLayerPSM.AnnPSMComponentProperty
	 * @generated
	 */
	EClass getAnnPSMComponentProperty();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.AnnPSMComponentProperty#getAnnotatesPSMComponentProperty <em>Annotates PSM Component Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates PSM Component Property</em>'.
	 * @see DesignPatternsLayerPSM.AnnPSMComponentProperty#getAnnotatesPSMComponentProperty()
	 * @see #getAnnPSMComponentProperty()
	 * @generated
	 */
	EReference getAnnPSMComponentProperty_AnnotatesPSMComponentProperty();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnJavaResourceModel <em>Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaResourceModel
	 * @generated
	 */
	EClass getAnnJavaResourceModel();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.AnnJavaResourceModel#getAnnotatesJavaResourceModel <em>Annotates Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaResourceModel#getAnnotatesJavaResourceModel()
	 * @see #getAnnJavaResourceModel()
	 * @generated
	 */
	EReference getAnnJavaResourceModel_AnnotatesJavaResourceModel();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel <em>Ann Java Algo Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Java Algo Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaAlgoResourceModel
	 * @generated
	 */
	EClass getAnnJavaAlgoResourceModel();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel#getAnnotatesJavaAlgoResourceModel <em>Annotates Java Algo Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates Java Algo Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaAlgoResourceModel#getAnnotatesJavaAlgoResourceModel()
	 * @see #getAnnJavaAlgoResourceModel()
	 * @generated
	 */
	EReference getAnnJavaAlgoResourceModel_AnnotatesJavaAlgoResourceModel();
	
	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.DesignPattern <em>Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern</em>'.
	 * @see DesignPatternsLayerPSM.DesignPattern
	 * @generated
	 */
	EClass getDesignPattern();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaBridgePattern <em>Java Bridge Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Bridge Pattern</em>'.
	 * @see DesignPatternsLayerPSM.JavaBridgePattern
	 * @generated
	 */
	EClass getJavaBridgePattern();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerPSM.JavaBridgePattern#getAssociatesAnnJavaAlgoResourceModel <em>Associates Ann Java Algo Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Associates Ann Java Algo Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.JavaBridgePattern#getAssociatesAnnJavaAlgoResourceModel()
	 * @see #getJavaBridgePattern()
	 * @generated
	 */
	EReference getJavaBridgePattern_AssociatesAnnJavaAlgoResourceModel();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>BMake Bridge Pattern For External Service</em>'.
	 * @see DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForExternalService()
	 * @see #getJavaBridgePattern()
	 * @generated
	 */
	EAttribute getJavaBridgePattern_BMakeBridgePatternForExternalService();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>BMake Bridge Pattern For Search</em>'.
	 * @see DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForSearch()
	 * @see #getJavaBridgePattern()
	 * @generated
	 */
	EAttribute getJavaBridgePattern_BMakeBridgePatternForSearch();
	
	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaBuilderPattern <em>Java Builder Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Builder Pattern</em>'.
	 * @see DesignPatternsLayerPSM.JavaBuilderPattern
	 * @generated
	 */
	EClass getJavaBuilderPattern();

	/**
	 * Returns the meta object for the containment reference '{@link DesignPatternsLayerPSM.JavaBuilderPattern#getHasJavaDirector <em>Has Java Director</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Has Java Director</em>'.
	 * @see DesignPatternsLayerPSM.JavaBuilderPattern#getHasJavaDirector()
	 * @see #getJavaBuilderPattern()
	 * @generated
	 */
	EReference getJavaBuilderPattern_HasJavaDirector();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerPSM.JavaBuilderPattern#getAssociatesAnnJavaResourceModels <em>Associates Ann Java Resource Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Associates Ann Java Resource Models</em>'.
	 * @see DesignPatternsLayerPSM.JavaBuilderPattern#getAssociatesAnnJavaResourceModels()
	 * @see #getJavaBuilderPattern()
	 * @generated
	 */
	EReference getJavaBuilderPattern_AssociatesAnnJavaResourceModels();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaDirector <em>Java Director</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Director</em>'.
	 * @see DesignPatternsLayerPSM.JavaDirector
	 * @generated
	 */
	EClass getJavaDirector();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.JavaDirector#getHasJavaBuilder <em>Has Java Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Java Builder</em>'.
	 * @see DesignPatternsLayerPSM.JavaDirector#getHasJavaBuilder()
	 * @see #getJavaDirector()
	 * @generated
	 */
	EReference getJavaDirector_HasJavaBuilder();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaConcreteBuilder <em>Java Concrete Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Concrete Builder</em>'.
	 * @see DesignPatternsLayerPSM.JavaConcreteBuilder
	 * @generated
	 */
	EClass getJavaConcreteBuilder();

	/**
	 * Returns the meta object for the containment reference '{@link DesignPatternsLayerPSM.JavaConcreteBuilder#getBuildsJavaRepresentation <em>Builds Java Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Builds Java Representation</em>'.
	 * @see DesignPatternsLayerPSM.JavaConcreteBuilder#getBuildsJavaRepresentation()
	 * @see #getJavaConcreteBuilder()
	 * @generated
	 */
	EReference getJavaConcreteBuilder_BuildsJavaRepresentation();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerPSM.JavaConcreteBuilder#buildsJavaRepresentation() <em>Builds Java Representation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Builds Java Representation</em>' operation.
	 * @see DesignPatternsLayerPSM.JavaConcreteBuilder#buildsJavaRepresentation()
	 * @generated
	 */
	EOperation getJavaConcreteBuilder__BuildsJavaRepresentation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaBuilder <em>Java Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Builder</em>'.
	 * @see DesignPatternsLayerPSM.JavaBuilder
	 * @generated
	 */
	EClass getJavaBuilder();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaBuilder#getAssociatesAnnJavaResourceModel <em>Associates Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associates Ann Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.JavaBuilder#getAssociatesAnnJavaResourceModel()
	 * @see #getJavaBuilder()
	 * @generated
	 */
	EReference getJavaBuilder_AssociatesAnnJavaResourceModel();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerPSM.JavaBuilder#buildsJavaRepresentation() <em>Builds Java Representation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Builds Java Representation</em>' operation.
	 * @see DesignPatternsLayerPSM.JavaBuilder#buildsJavaRepresentation()
	 * @generated
	 */
	EOperation getJavaBuilder__BuildsJavaRepresentation();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaRepresentation <em>Java Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Representation</em>'.
	 * @see DesignPatternsLayerPSM.JavaRepresentation
	 * @generated
	 */
	EClass getJavaRepresentation();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaRepresentation#getRefersTo <em>Refers To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Refers To</em>'.
	 * @see DesignPatternsLayerPSM.JavaRepresentation#getRefersTo()
	 * @see #getJavaRepresentation()
	 * @generated
	 */
	EReference getJavaRepresentation_RefersTo();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerPSM.JavaRepresentation#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has</em>'.
	 * @see DesignPatternsLayerPSM.JavaRepresentation#getHas()
	 * @see #getJavaRepresentation()
	 * @generated
	 */
	EReference getJavaRepresentation_Has();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaRepresentation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerPSM.JavaRepresentation#getName()
	 * @see #getJavaRepresentation()
	 * @generated
	 */
	EAttribute getJavaRepresentation_Name();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId <em>Resource Instance Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Instance Id</em>'.
	 * @see DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId()
	 * @see #getJavaRepresentation()
	 * @generated
	 */
	EAttribute getJavaRepresentation_ResourceInstanceId();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.DesignPatternModel <em>Design Pattern Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Design Pattern Model</em>'.
	 * @see DesignPatternsLayerPSM.DesignPatternModel
	 * @generated
	 */
	EClass getDesignPatternModel();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.DesignPatternModel#getHasDesignPattern <em>Has Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Design Pattern</em>'.
	 * @see DesignPatternsLayerPSM.DesignPatternModel#getHasDesignPattern()
	 * @see #getDesignPatternModel()
	 * @generated
	 */
	EReference getDesignPatternModel_HasDesignPattern();

	
	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaObserverPattern <em>Java Observer Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Observer Pattern</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserverPattern
	 * @generated
	 */
	EClass getJavaObserverPattern();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.JavaObserverPattern#getHasJavaObserver <em>Has Java Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Java Observer</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserverPattern#getHasJavaObserver()
	 * @see #getJavaObserverPattern()
	 * @generated
	 */
	EReference getJavaObserverPattern_HasJavaObserver();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaObserver <em>Java Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Observer</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserver
	 * @generated
	 */
	EClass getJavaObserver();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaObserver#getObserves <em>Observes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Observes</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserver#getObserves()
	 * @see #getJavaObserver()
	 * @generated
	 */
	EReference getJavaObserver_Observes();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaObserver#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserver#getName()
	 * @see #getJavaObserver()
	 * @generated
	 */
	EAttribute getJavaObserver_Name();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaObserver#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.JavaObserver#getReferencesAnnJavaResourceModel()
	 * @see #getJavaObserver()
	 * @generated
	 */
	EReference getJavaObserver_ReferencesAnnJavaResourceModel();

	/**
	 * Returns the meta object for the '{@link DesignPatternsLayerPSM.JavaObserver#update() <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see DesignPatternsLayerPSM.JavaObserver#update()
	 * @generated
	 */
	EOperation getJavaObserver__Update();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler <em>Java Observable Ann HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Observable Ann HTTP Activity Handler</em>'.
	 * @see DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler
	 * @generated
	 */
	EClass getJavaObservableAnnHTTPActivityHandler();

	/**
	 * Returns the meta object for the reference list '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getIsObservedBy <em>Is Observed By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Is Observed By</em>'.
	 * @see DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getIsObservedBy()
	 * @see #getJavaObservableAnnHTTPActivityHandler()
	 * @generated
	 */
	EReference getJavaObservableAnnHTTPActivityHandler_IsObservedBy();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getReferencesAnnJavaResourceModel()
	 * @see #getJavaObservableAnnHTTPActivityHandler()
	 * @generated
	 */
	EReference getJavaObservableAnnHTTPActivityHandler_ReferencesAnnJavaResourceModel();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getExtendsAnnJavaHTTPActivityHandler <em>Extends Ann Java HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Extends Ann Java HTTP Activity Handler</em>'.
	 * @see DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getExtendsAnnJavaHTTPActivityHandler()
	 * @see #getJavaObservableAnnHTTPActivityHandler()
	 * @generated
	 */
	EReference getJavaObservableAnnHTTPActivityHandler_ExtendsAnnJavaHTTPActivityHandler();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler <em>Ann Java HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ann Java HTTP Activity Handler</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler
	 * @generated
	 */
	EClass getAnnJavaHTTPActivityHandler();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler#getAnnotatesHTTPActivityHandler <em>Annotates HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Annotates HTTP Activity Handler</em>'.
	 * @see DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler#getAnnotatesHTTPActivityHandler()
	 * @see #getAnnJavaHTTPActivityHandler()
	 * @generated
	 */
	EReference getAnnJavaHTTPActivityHandler_AnnotatesHTTPActivityHandler();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaMementoPattern <em>Java Memento Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Memento Pattern</em>'.
	 * @see DesignPatternsLayerPSM.JavaMementoPattern
	 * @generated
	 */
	EClass getJavaMementoPattern();

	/**
	 * Returns the meta object for the containment reference list '{@link DesignPatternsLayerPSM.JavaMementoPattern#getHasJavaResourceModelMemento <em>Has Java Resource Model Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Java Resource Model Memento</em>'.
	 * @see DesignPatternsLayerPSM.JavaMementoPattern#getHasJavaResourceModelMemento()
	 * @see #getJavaMementoPattern()
	 * @generated
	 */
	EReference getJavaMementoPattern_HasJavaResourceModelMemento();

	/**
	 * Returns the meta object for class '{@link DesignPatternsLayerPSM.JavaResourceModelMemento <em>Java Resource Model Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Resource Model Memento</em>'.
	 * @see DesignPatternsLayerPSM.JavaResourceModelMemento
	 * @generated
	 */
	EClass getJavaResourceModelMemento();

	/**
	 * Returns the meta object for the attribute '{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getMementoNum <em>Memento Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Memento Num</em>'.
	 * @see DesignPatternsLayerPSM.JavaResourceModelMemento#getMementoNum()
	 * @see #getJavaResourceModelMemento()
	 * @generated
	 */
	EAttribute getJavaResourceModelMemento_MementoNum();

	/**
	 * Returns the meta object for the reference '{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>References Ann Java Resource Model</em>'.
	 * @see DesignPatternsLayerPSM.JavaResourceModelMemento#getReferencesAnnJavaResourceModel()
	 * @see #getJavaResourceModelMemento()
	 * @generated
	 */
	EReference getJavaResourceModelMemento_ReferencesAnnJavaResourceModel();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DesignPatternsLayerPSMFactory getDesignPatternsLayerPSMFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnotationModelImpl <em>Annotation Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnotationModelImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotationModel()
		 * @generated
		 */
		EClass ANNOTATION_MODEL = eINSTANCE.getAnnotationModel();

		/**
		 * The meta object literal for the '<em><b>Has Annotated Element</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_MODEL__HAS_ANNOTATED_ELEMENT = eINSTANCE.getAnnotationModel_HasAnnotatedElement();

		/**
		 * The meta object literal for the '<em><b>Has Annotation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_MODEL__HAS_ANNOTATION = eINSTANCE.getAnnotationModel_HasAnnotation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION_MODEL__NAME = eINSTANCE.getAnnotationModel_Name();

		/**
		 * The meta object literal for the '<em><b>Annotates RES Tful Service PSM</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATION_MODEL__ANNOTATES_RES_TFUL_SERVICE_PSM = eINSTANCE.getAnnotationModel_AnnotatesRESTfulServicePSM();

		
		/**
		 * The meta object literal for the '<em><b>Annotation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION_MODEL__ANNOTATION_TYPE = eINSTANCE.getAnnotationModel_AnnotationType();

		
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnotatedElementImpl <em>Annotated Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnotatedElementImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotatedElement()
		 * @generated
		 */
		EClass ANNOTATED_ELEMENT = eINSTANCE.getAnnotatedElement();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnotationImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnPSMComponentPropertyImpl <em>Ann PSM Component Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnPSMComponentPropertyImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnPSMComponentProperty()
		 * @generated
		 */
		EClass ANN_PSM_COMPONENT_PROPERTY = eINSTANCE.getAnnPSMComponentProperty();

		/**
		 * The meta object literal for the '<em><b>Annotates PSM Component Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_PSM_COMPONENT_PROPERTY__ANNOTATES_PSM_COMPONENT_PROPERTY = eINSTANCE.getAnnPSMComponentProperty_AnnotatesPSMComponentProperty();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnJavaResourceModelImpl <em>Ann Java Resource Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnJavaResourceModelImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaResourceModel()
		 * @generated
		 */
		EClass ANN_JAVA_RESOURCE_MODEL = eINSTANCE.getAnnJavaResourceModel();

		/**
		 * The meta object literal for the '<em><b>Annotates Java Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_JAVA_RESOURCE_MODEL__ANNOTATES_JAVA_RESOURCE_MODEL = eINSTANCE.getAnnJavaResourceModel_AnnotatesJavaResourceModel();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnJavaAlgoResourceModelImpl <em>Ann Java Algo Resource Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnJavaAlgoResourceModelImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaAlgoResourceModel()
		 * @generated
		 */
		EClass ANN_JAVA_ALGO_RESOURCE_MODEL = eINSTANCE.getAnnJavaAlgoResourceModel();

		/**
		 * The meta object literal for the '<em><b>Annotates Java Algo Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_JAVA_ALGO_RESOURCE_MODEL__ANNOTATES_JAVA_ALGO_RESOURCE_MODEL = eINSTANCE.getAnnJavaAlgoResourceModel_AnnotatesJavaAlgoResourceModel();
		
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.DesignPatternImpl <em>Design Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.DesignPatternImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getDesignPattern()
		 * @generated
		 */
		EClass DESIGN_PATTERN = eINSTANCE.getDesignPattern();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaBridgePatternImpl <em>Java Bridge Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaBridgePatternImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBridgePattern()
		 * @generated
		 */
		EClass JAVA_BRIDGE_PATTERN = eINSTANCE.getJavaBridgePattern();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Java Algo Resource Model</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_BRIDGE_PATTERN__ASSOCIATES_ANN_JAVA_ALGO_RESOURCE_MODEL = eINSTANCE.getJavaBridgePattern_AssociatesAnnJavaAlgoResourceModel();

		/**
		 * The meta object literal for the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_EXTERNAL_SERVICE = eINSTANCE.getJavaBridgePattern_BMakeBridgePatternForExternalService();

		/**
		 * The meta object literal for the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_BRIDGE_PATTERN__BMAKE_BRIDGE_PATTERN_FOR_SEARCH = eINSTANCE.getJavaBridgePattern_BMakeBridgePatternForSearch();
		
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl <em>Java Builder Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaBuilderPatternImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBuilderPattern()
		 * @generated
		 */
		EClass JAVA_BUILDER_PATTERN = eINSTANCE.getJavaBuilderPattern();

		/**
		 * The meta object literal for the '<em><b>Has Java Director</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_BUILDER_PATTERN__HAS_JAVA_DIRECTOR = eINSTANCE.getJavaBuilderPattern_HasJavaDirector();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Java Resource Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_BUILDER_PATTERN__ASSOCIATES_ANN_JAVA_RESOURCE_MODELS = eINSTANCE.getJavaBuilderPattern_AssociatesAnnJavaResourceModels();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaDirectorImpl <em>Java Director</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaDirectorImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaDirector()
		 * @generated
		 */
		EClass JAVA_DIRECTOR = eINSTANCE.getJavaDirector();

		/**
		 * The meta object literal for the '<em><b>Has Java Builder</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_DIRECTOR__HAS_JAVA_BUILDER = eINSTANCE.getJavaDirector_HasJavaBuilder();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaConcreteBuilderImpl <em>Java Concrete Builder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaConcreteBuilderImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaConcreteBuilder()
		 * @generated
		 */
		EClass JAVA_CONCRETE_BUILDER = eINSTANCE.getJavaConcreteBuilder();

		/**
		 * The meta object literal for the '<em><b>Builds Java Representation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_CONCRETE_BUILDER__BUILDS_JAVA_REPRESENTATION = eINSTANCE.getJavaConcreteBuilder_BuildsJavaRepresentation();

		/**
		 * The meta object literal for the '<em><b>Builds Java Representation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JAVA_CONCRETE_BUILDER___BUILDS_JAVA_REPRESENTATION = eINSTANCE.getJavaConcreteBuilder__BuildsJavaRepresentation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaBuilderImpl <em>Java Builder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaBuilderImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaBuilder()
		 * @generated
		 */
		EClass JAVA_BUILDER = eINSTANCE.getJavaBuilder();

		/**
		 * The meta object literal for the '<em><b>Associates Ann Java Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_BUILDER__ASSOCIATES_ANN_JAVA_RESOURCE_MODEL = eINSTANCE.getJavaBuilder_AssociatesAnnJavaResourceModel();

		/**
		 * The meta object literal for the '<em><b>Builds Java Representation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JAVA_BUILDER___BUILDS_JAVA_REPRESENTATION = eINSTANCE.getJavaBuilder__BuildsJavaRepresentation();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaRepresentationImpl <em>Java Representation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaRepresentationImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaRepresentation()
		 * @generated
		 */
		EClass JAVA_REPRESENTATION = eINSTANCE.getJavaRepresentation();

		/**
		 * The meta object literal for the '<em><b>Refers To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_REPRESENTATION__REFERS_TO = eINSTANCE.getJavaRepresentation_RefersTo();

		/**
		 * The meta object literal for the '<em><b>Has</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_REPRESENTATION__HAS = eINSTANCE.getJavaRepresentation_Has();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_REPRESENTATION__NAME = eINSTANCE.getJavaRepresentation_Name();

		/**
		 * The meta object literal for the '<em><b>Resource Instance Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_REPRESENTATION__RESOURCE_INSTANCE_ID = eINSTANCE.getJavaRepresentation_ResourceInstanceId();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.DesignPatternModelImpl <em>Design Pattern Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.DesignPatternModelImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getDesignPatternModel()
		 * @generated
		 */
		EClass DESIGN_PATTERN_MODEL = eINSTANCE.getDesignPatternModel();

		/**
		 * The meta object literal for the '<em><b>Has Design Pattern</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DESIGN_PATTERN_MODEL__HAS_DESIGN_PATTERN = eINSTANCE.getDesignPatternModel_HasDesignPattern();

		
		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaObserverPatternImpl <em>Java Observer Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaObserverPatternImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObserverPattern()
		 * @generated
		 */
		EClass JAVA_OBSERVER_PATTERN = eINSTANCE.getJavaObserverPattern();

		/**
		 * The meta object literal for the '<em><b>Has Java Observer</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVER_PATTERN__HAS_JAVA_OBSERVER = eINSTANCE.getJavaObserverPattern_HasJavaObserver();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaObserverImpl <em>Java Observer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaObserverImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObserver()
		 * @generated
		 */
		EClass JAVA_OBSERVER = eINSTANCE.getJavaObserver();

		/**
		 * The meta object literal for the '<em><b>Observes</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVER__OBSERVES = eINSTANCE.getJavaObserver_Observes();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_OBSERVER__NAME = eINSTANCE.getJavaObserver_Name();

		/**
		 * The meta object literal for the '<em><b>References Ann Java Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVER__REFERENCES_ANN_JAVA_RESOURCE_MODEL = eINSTANCE.getJavaObserver_ReferencesAnnJavaResourceModel();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation JAVA_OBSERVER___UPDATE = eINSTANCE.getJavaObserver__Update();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl <em>Java Observable Ann HTTP Activity Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaObservableAnnHTTPActivityHandlerImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaObservableAnnHTTPActivityHandler()
		 * @generated
		 */
		EClass JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER = eINSTANCE.getJavaObservableAnnHTTPActivityHandler();

		/**
		 * The meta object literal for the '<em><b>Is Observed By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__IS_OBSERVED_BY = eINSTANCE.getJavaObservableAnnHTTPActivityHandler_IsObservedBy();

		/**
		 * The meta object literal for the '<em><b>References Ann Java Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__REFERENCES_ANN_JAVA_RESOURCE_MODEL = eINSTANCE.getJavaObservableAnnHTTPActivityHandler_ReferencesAnnJavaResourceModel();

		/**
		 * The meta object literal for the '<em><b>Extends Ann Java HTTP Activity Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER__EXTENDS_ANN_JAVA_HTTP_ACTIVITY_HANDLER = eINSTANCE.getJavaObservableAnnHTTPActivityHandler_ExtendsAnnJavaHTTPActivityHandler();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.AnnJavaHTTPActivityHandlerImpl <em>Ann Java HTTP Activity Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.AnnJavaHTTPActivityHandlerImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getAnnJavaHTTPActivityHandler()
		 * @generated
		 */
		EClass ANN_JAVA_HTTP_ACTIVITY_HANDLER = eINSTANCE.getAnnJavaHTTPActivityHandler();

		/**
		 * The meta object literal for the '<em><b>Annotates HTTP Activity Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANN_JAVA_HTTP_ACTIVITY_HANDLER__ANNOTATES_HTTP_ACTIVITY_HANDLER = eINSTANCE.getAnnJavaHTTPActivityHandler_AnnotatesHTTPActivityHandler();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaMementoPatternImpl <em>Java Memento Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaMementoPatternImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaMementoPattern()
		 * @generated
		 */
		EClass JAVA_MEMENTO_PATTERN = eINSTANCE.getJavaMementoPattern();

		/**
		 * The meta object literal for the '<em><b>Has Java Resource Model Memento</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_MEMENTO_PATTERN__HAS_JAVA_RESOURCE_MODEL_MEMENTO = eINSTANCE.getJavaMementoPattern_HasJavaResourceModelMemento();

		/**
		 * The meta object literal for the '{@link DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl <em>Java Resource Model Memento</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see DesignPatternsLayerPSM.impl.JavaResourceModelMementoImpl
		 * @see DesignPatternsLayerPSM.impl.DesignPatternsLayerPSMPackageImpl#getJavaResourceModelMemento()
		 * @generated
		 */
		EClass JAVA_RESOURCE_MODEL_MEMENTO = eINSTANCE.getJavaResourceModelMemento();

		/**
		 * The meta object literal for the '<em><b>Memento Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_RESOURCE_MODEL_MEMENTO__MEMENTO_NUM = eINSTANCE.getJavaResourceModelMemento_MementoNum();

		/**
		 * The meta object literal for the '<em><b>References Ann Java Resource Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_RESOURCE_MODEL_MEMENTO__REFERENCES_ANN_JAVA_RESOURCE_MODEL = eINSTANCE.getJavaResourceModelMemento_ReferencesAnnJavaResourceModel();

	}

} //DesignPatternsLayerPSMPackage
