/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Memento Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaMementoPattern#getHasJavaResourceModelMemento <em>Has Java Resource Model Memento</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaMementoPattern()
 * @model
 * @generated
 */
public interface JavaMementoPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Has Java Resource Model Memento</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.JavaResourceModelMemento}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Java Resource Model Memento</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Java Resource Model Memento</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaMementoPattern_HasJavaResourceModelMemento()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<JavaResourceModelMemento> getHasJavaResourceModelMemento();

} // JavaMementoPattern
