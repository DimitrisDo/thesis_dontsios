/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Resource Model Memento</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getMementoNum <em>Memento Num</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaResourceModelMemento()
 * @model
 * @generated
 */
public interface JavaResourceModelMemento extends EObject {
	/**
	 * Returns the value of the '<em><b>Memento Num</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Memento Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Memento Num</em>' attribute.
	 * @see #setMementoNum(int)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaResourceModelMemento_MementoNum()
	 * @model default="1" unique="false" dataType="org.eclipse.emf.ecore.xml.type.Int" ordered="false"
	 * @generated
	 */
	int getMementoNum();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getMementoNum <em>Memento Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Memento Num</em>' attribute.
	 * @see #getMementoNum()
	 * @generated
	 */
	void setMementoNum(int value);

	/**
	 * Returns the value of the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Java Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #setReferencesAnnJavaResourceModel(AnnJavaResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaResourceModelMemento_ReferencesAnnJavaResourceModel()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaResourceModel getReferencesAnnJavaResourceModel();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaResourceModelMemento#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 */
	void setReferencesAnnJavaResourceModel(AnnJavaResourceModel value);

} // JavaResourceModelMemento
