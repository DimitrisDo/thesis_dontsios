/**
 */
package DesignPatternsLayerPSM;

import RESTfulServicePSM.JavaAlgoResourceModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ann Java Algo Resource Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel#getAnnotatesJavaAlgoResourceModel <em>Annotates Java Algo Resource Model</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnJavaAlgoResourceModel()
 * @model
 * @generated
 */
public interface AnnJavaAlgoResourceModel extends AnnotatedElement {
	/**
	 * Returns the value of the '<em><b>Annotates Java Algo Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotates Java Algo Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotates Java Algo Resource Model</em>' reference.
	 * @see #setAnnotatesJavaAlgoResourceModel(JavaAlgoResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnJavaAlgoResourceModel_AnnotatesJavaAlgoResourceModel()
	 * @model required="true"
	 * @generated
	 */
	JavaAlgoResourceModel getAnnotatesJavaAlgoResourceModel();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel#getAnnotatesJavaAlgoResourceModel <em>Annotates Java Algo Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotates Java Algo Resource Model</em>' reference.
	 * @see #getAnnotatesJavaAlgoResourceModel()
	 * @generated
	 */
	void setAnnotatesJavaAlgoResourceModel(JavaAlgoResourceModel value);

} // AnnJavaAlgoResourceModel
