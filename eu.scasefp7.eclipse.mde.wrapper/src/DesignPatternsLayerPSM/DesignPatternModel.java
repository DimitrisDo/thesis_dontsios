/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Design Pattern Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.DesignPatternModel#getHasDesignPattern <em>Has Design Pattern</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getDesignPatternModel()
 * @model
 * @generated
 */
public interface DesignPatternModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Design Pattern</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.DesignPattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Design Pattern</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Design Pattern</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getDesignPatternModel_HasDesignPattern()
	 * @model containment="true"
	 * @generated
	 */
	EList<DesignPattern> getHasDesignPattern();

} // DesignPatternModel
