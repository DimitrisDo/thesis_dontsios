/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Observer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaObserver#getObserves <em>Observes</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaObserver#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaObserver#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserver()
 * @model
 * @generated
 */
public interface JavaObserver extends EObject {
	/**
	 * Returns the value of the '<em><b>Observes</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Observes</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Observes</em>' reference.
	 * @see #setObserves(JavaObservableAnnHTTPActivityHandler)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserver_Observes()
	 * @model required="true"
	 * @generated
	 */
	JavaObservableAnnHTTPActivityHandler getObserves();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaObserver#getObserves <em>Observes</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Observes</em>' reference.
	 * @see #getObserves()
	 * @generated
	 */
	void setObserves(JavaObservableAnnHTTPActivityHandler value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserver_Name()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaObserver#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Java Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #setReferencesAnnJavaResourceModel(AnnJavaResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserver_ReferencesAnnJavaResourceModel()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaResourceModel getReferencesAnnJavaResourceModel();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaObserver#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 */
	void setReferencesAnnJavaResourceModel(AnnJavaResourceModel value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void update();

} // JavaObserver
