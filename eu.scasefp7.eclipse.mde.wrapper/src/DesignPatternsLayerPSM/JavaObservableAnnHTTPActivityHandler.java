/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Observable Ann HTTP Activity Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getIsObservedBy <em>Is Observed By</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getExtendsAnnJavaHTTPActivityHandler <em>Extends Ann Java HTTP Activity Handler</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObservableAnnHTTPActivityHandler()
 * @model
 * @generated
 */
public interface JavaObservableAnnHTTPActivityHandler extends Annotation {
	/**
	 * Returns the value of the '<em><b>Is Observed By</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.JavaObserver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Observed By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Observed By</em>' reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObservableAnnHTTPActivityHandler_IsObservedBy()
	 * @model
	 * @generated
	 */
	EList<JavaObserver> getIsObservedBy();

	/**
	 * Returns the value of the '<em><b>References Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>References Ann Java Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #setReferencesAnnJavaResourceModel(AnnJavaResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObservableAnnHTTPActivityHandler_ReferencesAnnJavaResourceModel()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaResourceModel getReferencesAnnJavaResourceModel();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getReferencesAnnJavaResourceModel <em>References Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>References Ann Java Resource Model</em>' reference.
	 * @see #getReferencesAnnJavaResourceModel()
	 * @generated
	 */
	void setReferencesAnnJavaResourceModel(AnnJavaResourceModel value);

	/**
	 * Returns the value of the '<em><b>Extends Ann Java HTTP Activity Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extends Ann Java HTTP Activity Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends Ann Java HTTP Activity Handler</em>' reference.
	 * @see #setExtendsAnnJavaHTTPActivityHandler(AnnJavaHTTPActivityHandler)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObservableAnnHTTPActivityHandler_ExtendsAnnJavaHTTPActivityHandler()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaHTTPActivityHandler getExtendsAnnJavaHTTPActivityHandler();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler#getExtendsAnnJavaHTTPActivityHandler <em>Extends Ann Java HTTP Activity Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extends Ann Java HTTP Activity Handler</em>' reference.
	 * @see #getExtendsAnnJavaHTTPActivityHandler()
	 * @generated
	 */
	void setExtendsAnnJavaHTTPActivityHandler(AnnJavaHTTPActivityHandler value);

} // JavaObservableAnnHTTPActivityHandler
