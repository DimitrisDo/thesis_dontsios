/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Representation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaRepresentation#getRefersTo <em>Refers To</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaRepresentation#getHas <em>Has</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaRepresentation#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId <em>Resource Instance Id</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaRepresentation()
 * @model
 * @generated
 */
public interface JavaRepresentation extends EObject {
	/**
	 * Returns the value of the '<em><b>Refers To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refers To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refers To</em>' reference.
	 * @see #setRefersTo(AnnJavaResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaRepresentation_RefersTo()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaResourceModel getRefersTo();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaRepresentation#getRefersTo <em>Refers To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refers To</em>' reference.
	 * @see #getRefersTo()
	 * @generated
	 */
	void setRefersTo(AnnJavaResourceModel value);

	/**
	 * Returns the value of the '<em><b>Has</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.AnnPSMComponentProperty}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaRepresentation_Has()
	 * @model
	 * @generated
	 */
	EList<AnnPSMComponentProperty> getHas();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaRepresentation_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaRepresentation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Resource Instance Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Instance Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Instance Id</em>' attribute.
	 * @see #isSetResourceInstanceId()
	 * @see #unsetResourceInstanceId()
	 * @see #setResourceInstanceId(String)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaRepresentation_ResourceInstanceId()
	 * @model unsettable="true" id="true"
	 * @generated
	 */
	String getResourceInstanceId();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId <em>Resource Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Instance Id</em>' attribute.
	 * @see #isSetResourceInstanceId()
	 * @see #unsetResourceInstanceId()
	 * @see #getResourceInstanceId()
	 * @generated
	 */
	void setResourceInstanceId(String value);

	/**
	 * Unsets the value of the '{@link DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId <em>Resource Instance Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResourceInstanceId()
	 * @see #getResourceInstanceId()
	 * @see #setResourceInstanceId(String)
	 * @generated
	 */
	void unsetResourceInstanceId();

	/**
	 * Returns whether the value of the '{@link DesignPatternsLayerPSM.JavaRepresentation#getResourceInstanceId <em>Resource Instance Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Resource Instance Id</em>' attribute is set.
	 * @see #unsetResourceInstanceId()
	 * @see #getResourceInstanceId()
	 * @see #setResourceInstanceId(String)
	 * @generated
	 */
	boolean isSetResourceInstanceId();

} // JavaRepresentation
