/**
 */
package DesignPatternsLayerPSM;

import RESTfulServicePSM.ServicePSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.AnnotationModel#getHasAnnotatedElement <em>Has Annotated Element</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.AnnotationModel#getHasAnnotation <em>Has Annotation</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.AnnotationModel#getName <em>Name</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotatesRESTfulServicePSM <em>Annotates RES Tful Service PSM</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotationType <em>Annotation Type</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel()
 * @model
 * @generated
 */
public interface AnnotationModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Annotated Element</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.AnnotatedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Annotated Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Annotated Element</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel_HasAnnotatedElement()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<AnnotatedElement> getHasAnnotatedElement();

	/**
	 * Returns the value of the '<em><b>Has Annotation</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Annotation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Annotation</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel_HasAnnotation()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Annotation> getHasAnnotation();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.AnnotationModel#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Annotates RES Tful Service PSM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotates RES Tful Service PSM</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotates RES Tful Service PSM</em>' reference.
	 * @see #setAnnotatesRESTfulServicePSM(ServicePSM)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel_AnnotatesRESTfulServicePSM()
	 * @model required="true"
	 * @generated
	 */
	ServicePSM getAnnotatesRESTfulServicePSM();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotatesRESTfulServicePSM <em>Annotates RES Tful Service PSM</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotates RES Tful Service PSM</em>' reference.
	 * @see #getAnnotatesRESTfulServicePSM()
	 * @generated
	 */
	void setAnnotatesRESTfulServicePSM(ServicePSM value);

	/**
	 * Returns the value of the '<em><b>Annotation Type</b></em>' attribute.
	 * The default value is <code>"DesignPatternsLayer"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotation Type</em>' attribute.
	 * @see #setAnnotationType(String)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getAnnotationModel_AnnotationType()
	 * @model default="DesignPatternsLayer" ordered="false"
	 * @generated
	 */
	String getAnnotationType();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.AnnotationModel#getAnnotationType <em>Annotation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Annotation Type</em>' attribute.
	 * @see #getAnnotationType()
	 * @generated
	 */
	void setAnnotationType(String value);

} // AnnotationModel
