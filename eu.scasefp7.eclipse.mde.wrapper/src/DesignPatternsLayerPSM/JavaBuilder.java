/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Builder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaBuilder#getAssociatesAnnJavaResourceModel <em>Associates Ann Java Resource Model</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBuilder()
 * @model
 * @generated
 */
public interface JavaBuilder extends EObject {
	/**
	 * Returns the value of the '<em><b>Associates Ann Java Resource Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Java Resource Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Java Resource Model</em>' reference.
	 * @see #setAssociatesAnnJavaResourceModel(AnnJavaResourceModel)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBuilder_AssociatesAnnJavaResourceModel()
	 * @model required="true"
	 * @generated
	 */
	AnnJavaResourceModel getAssociatesAnnJavaResourceModel();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaBuilder#getAssociatesAnnJavaResourceModel <em>Associates Ann Java Resource Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associates Ann Java Resource Model</em>' reference.
	 * @see #getAssociatesAnnJavaResourceModel()
	 * @generated
	 */
	void setAssociatesAnnJavaResourceModel(AnnJavaResourceModel value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	JavaRepresentation buildsJavaRepresentation();

} // JavaBuilder
