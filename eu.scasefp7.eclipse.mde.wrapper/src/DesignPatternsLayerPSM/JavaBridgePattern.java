/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Bridge Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaBridgePattern#getAssociatesAnnJavaAlgoResourceModel <em>Associates Ann Java Algo Resource Model</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}</li>
 *   <li>{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBridgePattern()
 * @model
 * @generated
 */
public interface JavaBridgePattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Associates Ann Java Algo Resource Model</b></em>' reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associates Ann Java Algo Resource Model</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associates Ann Java Algo Resource Model</em>' reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBridgePattern_AssociatesAnnJavaAlgoResourceModel()
	 * @model required="true"
	 * @generated
	 */
	EList<AnnJavaAlgoResourceModel> getAssociatesAnnJavaAlgoResourceModel();

	/**
	 * Returns the value of the '<em><b>BMake Bridge Pattern For External Service</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BMake Bridge Pattern For External Service</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BMake Bridge Pattern For External Service</em>' attribute.
	 * @see #setBMakeBridgePatternForExternalService(boolean)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBridgePattern_BMakeBridgePatternForExternalService()
	 * @model default="false" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBMakeBridgePatternForExternalService();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForExternalService <em>BMake Bridge Pattern For External Service</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BMake Bridge Pattern For External Service</em>' attribute.
	 * @see #isBMakeBridgePatternForExternalService()
	 * @generated
	 */
	void setBMakeBridgePatternForExternalService(boolean value);

	/**
	 * Returns the value of the '<em><b>BMake Bridge Pattern For Search</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>BMake Bridge Pattern For Search</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>BMake Bridge Pattern For Search</em>' attribute.
	 * @see #setBMakeBridgePatternForSearch(boolean)
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaBridgePattern_BMakeBridgePatternForSearch()
	 * @model default="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean"
	 * @generated
	 */
	boolean isBMakeBridgePatternForSearch();

	/**
	 * Sets the value of the '{@link DesignPatternsLayerPSM.JavaBridgePattern#isBMakeBridgePatternForSearch <em>BMake Bridge Pattern For Search</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>BMake Bridge Pattern For Search</em>' attribute.
	 * @see #isBMakeBridgePatternForSearch()
	 * @generated
	 */
	void setBMakeBridgePatternForSearch(boolean value);

} // JavaBridgePattern
