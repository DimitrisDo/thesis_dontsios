/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Director</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaDirector#getHasJavaBuilder <em>Has Java Builder</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaDirector()
 * @model
 * @generated
 */
public interface JavaDirector extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Java Builder</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.JavaBuilder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Java Builder</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Java Builder</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaDirector_HasJavaBuilder()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<JavaBuilder> getHasJavaBuilder();

} // JavaDirector
