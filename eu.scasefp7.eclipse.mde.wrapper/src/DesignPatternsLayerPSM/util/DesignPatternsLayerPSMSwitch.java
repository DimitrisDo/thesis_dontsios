/**
 */
package DesignPatternsLayerPSM.util;

import DesignPatternsLayerPSM.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage
 * @generated
 */
public class DesignPatternsLayerPSMSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DesignPatternsLayerPSMPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerPSMSwitch() {
		if (modelPackage == null) {
			modelPackage = DesignPatternsLayerPSMPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DesignPatternsLayerPSMPackage.ANNOTATION_MODEL: {
				AnnotationModel annotationModel = (AnnotationModel)theEObject;
				T result = caseAnnotationModel(annotationModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANNOTATED_ELEMENT: {
				AnnotatedElement annotatedElement = (AnnotatedElement)theEObject;
				T result = caseAnnotatedElement(annotatedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANN_PSM_COMPONENT_PROPERTY: {
				AnnPSMComponentProperty annPSMComponentProperty = (AnnPSMComponentProperty)theEObject;
				T result = caseAnnPSMComponentProperty(annPSMComponentProperty);
				if (result == null) result = caseAnnotatedElement(annPSMComponentProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANN_JAVA_RESOURCE_MODEL: {
				AnnJavaResourceModel annJavaResourceModel = (AnnJavaResourceModel)theEObject;
				T result = caseAnnJavaResourceModel(annJavaResourceModel);
				if (result == null) result = caseAnnotatedElement(annJavaResourceModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANN_JAVA_ALGO_RESOURCE_MODEL: {
				AnnJavaAlgoResourceModel annJavaAlgoResourceModel = (AnnJavaAlgoResourceModel)theEObject;
				T result = caseAnnJavaAlgoResourceModel(annJavaAlgoResourceModel);
				if (result == null) result = caseAnnotatedElement(annJavaAlgoResourceModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.DESIGN_PATTERN: {
				DesignPattern designPattern = (DesignPattern)theEObject;
				T result = caseDesignPattern(designPattern);
				if (result == null) result = caseAnnotation(designPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_BRIDGE_PATTERN: {
				JavaBridgePattern javaBridgePattern = (JavaBridgePattern)theEObject;
				T result = caseJavaBridgePattern(javaBridgePattern);
				if (result == null) result = caseDesignPattern(javaBridgePattern);
				if (result == null) result = caseAnnotation(javaBridgePattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER_PATTERN: {
				JavaBuilderPattern javaBuilderPattern = (JavaBuilderPattern)theEObject;
				T result = caseJavaBuilderPattern(javaBuilderPattern);
				if (result == null) result = caseDesignPattern(javaBuilderPattern);
				if (result == null) result = caseAnnotation(javaBuilderPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_DIRECTOR: {
				JavaDirector javaDirector = (JavaDirector)theEObject;
				T result = caseJavaDirector(javaDirector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_CONCRETE_BUILDER: {
				JavaConcreteBuilder javaConcreteBuilder = (JavaConcreteBuilder)theEObject;
				T result = caseJavaConcreteBuilder(javaConcreteBuilder);
				if (result == null) result = caseJavaBuilder(javaConcreteBuilder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_BUILDER: {
				JavaBuilder javaBuilder = (JavaBuilder)theEObject;
				T result = caseJavaBuilder(javaBuilder);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_REPRESENTATION: {
				JavaRepresentation javaRepresentation = (JavaRepresentation)theEObject;
				T result = caseJavaRepresentation(javaRepresentation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.DESIGN_PATTERN_MODEL: {
				DesignPatternModel designPatternModel = (DesignPatternModel)theEObject;
				T result = caseDesignPatternModel(designPatternModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER_PATTERN: {
				JavaObserverPattern javaObserverPattern = (JavaObserverPattern)theEObject;
				T result = caseJavaObserverPattern(javaObserverPattern);
				if (result == null) result = caseDesignPattern(javaObserverPattern);
				if (result == null) result = caseAnnotation(javaObserverPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVER: {
				JavaObserver javaObserver = (JavaObserver)theEObject;
				T result = caseJavaObserver(javaObserver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_OBSERVABLE_ANN_HTTP_ACTIVITY_HANDLER: {
				JavaObservableAnnHTTPActivityHandler javaObservableAnnHTTPActivityHandler = (JavaObservableAnnHTTPActivityHandler)theEObject;
				T result = caseJavaObservableAnnHTTPActivityHandler(javaObservableAnnHTTPActivityHandler);
				if (result == null) result = caseAnnotation(javaObservableAnnHTTPActivityHandler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.ANN_JAVA_HTTP_ACTIVITY_HANDLER: {
				AnnJavaHTTPActivityHandler annJavaHTTPActivityHandler = (AnnJavaHTTPActivityHandler)theEObject;
				T result = caseAnnJavaHTTPActivityHandler(annJavaHTTPActivityHandler);
				if (result == null) result = caseAnnotatedElement(annJavaHTTPActivityHandler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_MEMENTO_PATTERN: {
				JavaMementoPattern javaMementoPattern = (JavaMementoPattern)theEObject;
				T result = caseJavaMementoPattern(javaMementoPattern);
				if (result == null) result = caseDesignPattern(javaMementoPattern);
				if (result == null) result = caseAnnotation(javaMementoPattern);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DesignPatternsLayerPSMPackage.JAVA_RESOURCE_MODEL_MEMENTO: {
				JavaResourceModelMemento javaResourceModelMemento = (JavaResourceModelMemento)theEObject;
				T result = caseJavaResourceModelMemento(javaResourceModelMemento);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotationModel(AnnotationModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotated Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotatedElement(AnnotatedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann PSM Component Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann PSM Component Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnPSMComponentProperty(AnnPSMComponentProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Java Resource Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Java Resource Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnJavaResourceModel(AnnJavaResourceModel object) {
		return null;
	}
	
	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Java Algo Resource Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Java Algo Resource Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnJavaAlgoResourceModel(AnnJavaAlgoResourceModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPattern(DesignPattern object) {
		return null;
	}
	
	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Bridge Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Bridge Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaBridgePattern(JavaBridgePattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Builder Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Builder Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaBuilderPattern(JavaBuilderPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Director</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Director</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaDirector(JavaDirector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Concrete Builder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Concrete Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaConcreteBuilder(JavaConcreteBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Builder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Builder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaBuilder(JavaBuilder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Representation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Representation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaRepresentation(JavaRepresentation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Design Pattern Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Design Pattern Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDesignPatternModel(DesignPatternModel object) {
		return null;
	}
	

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Observer Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Observer Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaObserverPattern(JavaObserverPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Observer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Observer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaObserver(JavaObserver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Observable Ann HTTP Activity Handler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Observable Ann HTTP Activity Handler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaObservableAnnHTTPActivityHandler(JavaObservableAnnHTTPActivityHandler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ann Java HTTP Activity Handler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ann Java HTTP Activity Handler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnJavaHTTPActivityHandler(AnnJavaHTTPActivityHandler object) {
		return null;
	}
	

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Memento Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Memento Pattern</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaMementoPattern(JavaMementoPattern object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Java Resource Model Memento</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Java Resource Model Memento</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavaResourceModelMemento(JavaResourceModelMemento object) {
		return null;
	}
	

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DesignPatternsLayerPSMSwitch
