/**
 */
package DesignPatternsLayerPSM.util;

import DesignPatternsLayerPSM.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage
 * @generated
 */
public class DesignPatternsLayerPSMAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DesignPatternsLayerPSMPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsLayerPSMAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DesignPatternsLayerPSMPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DesignPatternsLayerPSMSwitch<Adapter> modelSwitch =
		new DesignPatternsLayerPSMSwitch<Adapter>() {
			@Override
			public Adapter caseAnnotationModel(AnnotationModel object) {
				return createAnnotationModelAdapter();
			}
			@Override
			public Adapter caseAnnotatedElement(AnnotatedElement object) {
				return createAnnotatedElementAdapter();
			}
			@Override
			public Adapter caseAnnotation(Annotation object) {
				return createAnnotationAdapter();
			}
			@Override
			public Adapter caseAnnPSMComponentProperty(AnnPSMComponentProperty object) {
				return createAnnPSMComponentPropertyAdapter();
			}
			@Override
			public Adapter caseAnnJavaResourceModel(AnnJavaResourceModel object) {
				return createAnnJavaResourceModelAdapter();
			}
			@Override
			public Adapter caseAnnJavaAlgoResourceModel(AnnJavaAlgoResourceModel object) {
				return createAnnJavaAlgoResourceModelAdapter();
			}
			@Override
			public Adapter caseJavaBridgePattern(JavaBridgePattern object) {
				return createJavaBridgePatternAdapter();
			}
			@Override
			public Adapter caseDesignPattern(DesignPattern object) {
				return createDesignPatternAdapter();
			}
			@Override
			public Adapter caseJavaBuilderPattern(JavaBuilderPattern object) {
				return createJavaBuilderPatternAdapter();
			}
			@Override
			public Adapter caseJavaDirector(JavaDirector object) {
				return createJavaDirectorAdapter();
			}
			@Override
			public Adapter caseJavaConcreteBuilder(JavaConcreteBuilder object) {
				return createJavaConcreteBuilderAdapter();
			}
			@Override
			public Adapter caseJavaBuilder(JavaBuilder object) {
				return createJavaBuilderAdapter();
			}
			@Override
			public Adapter caseJavaRepresentation(JavaRepresentation object) {
				return createJavaRepresentationAdapter();
			}
			@Override
			public Adapter caseDesignPatternModel(DesignPatternModel object) {
				return createDesignPatternModelAdapter();
			}
			@Override
			public Adapter caseJavaObserverPattern(JavaObserverPattern object) {
				return createJavaObserverPatternAdapter();
			}
			@Override
			public Adapter caseJavaObserver(JavaObserver object) {
				return createJavaObserverAdapter();
			}
			@Override
			public Adapter caseJavaObservableAnnHTTPActivityHandler(JavaObservableAnnHTTPActivityHandler object) {
				return createJavaObservableAnnHTTPActivityHandlerAdapter();
			}
			@Override
			public Adapter caseAnnJavaHTTPActivityHandler(AnnJavaHTTPActivityHandler object) {
				return createAnnJavaHTTPActivityHandlerAdapter();
			}
			@Override
			public Adapter caseJavaMementoPattern(JavaMementoPattern object) {
				return createJavaMementoPatternAdapter();
			}
			@Override
			public Adapter caseJavaResourceModelMemento(JavaResourceModelMemento object) {
				return createJavaResourceModelMementoAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnotationModel <em>Annotation Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnotationModel
	 * @generated
	 */
	public Adapter createAnnotationModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnotatedElement <em>Annotated Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnotatedElement
	 * @generated
	 */
	public Adapter createAnnotatedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.Annotation
	 * @generated
	 */
	public Adapter createAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnPSMComponentProperty <em>Ann PSM Component Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnPSMComponentProperty
	 * @generated
	 */
	public Adapter createAnnPSMComponentPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnJavaResourceModel <em>Ann Java Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnJavaResourceModel
	 * @generated
	 */
	public Adapter createAnnJavaResourceModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnJavaAlgoResourceModel <em>Ann Java Algo Resource Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnJavaAlgoResourceModel
	 * @generated
	 */
	public Adapter createAnnJavaAlgoResourceModelAdapter() {
		return null;
	}
	
	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.DesignPattern <em>Design Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.DesignPattern
	 * @generated
	 */
	public Adapter createDesignPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaBridgePattern <em>Java Bridge Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaBridgePattern
	 * @generated
	 */
	public Adapter createJavaBridgePatternAdapter() {
		return null;
	}
	
	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaBuilderPattern <em>Java Builder Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaBuilderPattern
	 * @generated
	 */
	public Adapter createJavaBuilderPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaDirector <em>Java Director</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaDirector
	 * @generated
	 */
	public Adapter createJavaDirectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaConcreteBuilder <em>Java Concrete Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaConcreteBuilder
	 * @generated
	 */
	public Adapter createJavaConcreteBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaBuilder <em>Java Builder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaBuilder
	 * @generated
	 */
	public Adapter createJavaBuilderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaRepresentation <em>Java Representation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaRepresentation
	 * @generated
	 */
	public Adapter createJavaRepresentationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.DesignPatternModel <em>Design Pattern Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.DesignPatternModel
	 * @generated
	 */
	public Adapter createDesignPatternModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaObserverPattern <em>Java Observer Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaObserverPattern
	 * @generated
	 */
	public Adapter createJavaObserverPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaObserver <em>Java Observer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaObserver
	 * @generated
	 */
	public Adapter createJavaObserverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler <em>Java Observable Ann HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaObservableAnnHTTPActivityHandler
	 * @generated
	 */
	public Adapter createJavaObservableAnnHTTPActivityHandlerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler <em>Ann Java HTTP Activity Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.AnnJavaHTTPActivityHandler
	 * @generated
	 */
	public Adapter createAnnJavaHTTPActivityHandlerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaMementoPattern <em>Java Memento Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaMementoPattern
	 * @generated
	 */
	public Adapter createJavaMementoPatternAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link DesignPatternsLayerPSM.JavaResourceModelMemento <em>Java Resource Model Memento</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see DesignPatternsLayerPSM.JavaResourceModelMemento
	 * @generated
	 */
	public Adapter createJavaResourceModelMementoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DesignPatternsLayerPSMAdapterFactory
