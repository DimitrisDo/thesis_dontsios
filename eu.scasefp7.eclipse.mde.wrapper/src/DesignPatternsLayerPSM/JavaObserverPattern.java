/**
 */
package DesignPatternsLayerPSM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Observer Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link DesignPatternsLayerPSM.JavaObserverPattern#getHasJavaObserver <em>Has Java Observer</em>}</li>
 * </ul>
 *
 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserverPattern()
 * @model
 * @generated
 */
public interface JavaObserverPattern extends DesignPattern {
	/**
	 * Returns the value of the '<em><b>Has Java Observer</b></em>' containment reference list.
	 * The list contents are of type {@link DesignPatternsLayerPSM.JavaObserver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Java Observer</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Java Observer</em>' containment reference list.
	 * @see DesignPatternsLayerPSM.DesignPatternsLayerPSMPackage#getJavaObserverPattern_HasJavaObserver()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<JavaObserver> getHasJavaObserver();

} // JavaObserverPattern
